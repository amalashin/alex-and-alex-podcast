#!/usr/bin/env python
# -*- coding: utf-8 -*- #

from os import environ, EX_NOINPUT, EX_CONFIG
from os.path import isfile
from sys import exit
from boto3 import client
from botocore.exceptions import NoCredentialsError

AWS_REGION = 'eu-central-1'
AWS_S3_BUCKET = 'alex-and-alex-podcast'

FEED_FILENAME = 'podcast.xml'   # xml feed filename


def main():
    print('Starting publish task...')

    if not isfile(FEED_FILENAME):
        print(f'ERROR: Feed file {FEED_FILENAME} doesn`t exist')
        exit(EX_NOINPUT)

    if 'CI_COMMIT_TAG' not in environ or not environ['CI_COMMIT_TAG']:
        print('ERROR: There no commit tag in env')
        exit(EX_CONFIG)

    if 'AWS_ACCESS_KEY' not in environ or 'AWS_SECRET_KEY' not in environ:
        print('ERROR: AWS credentials are not set in the environment')
        exit(EX_CONFIG)

    print(f'-> Found env settings:')
    print(f'-> ... Commit tag = {environ["CI_COMMIT_TAG"]}')
    print(f'-> ... AWS Access Key = {environ["AWS_ACCESS_KEY"]}')
    print(f'-> ... AWS Secret Key = {environ["AWS_SECRET_KEY"][:10]}...')

    s3 = client('s3', aws_access_key_id=environ['AWS_ACCESS_KEY'], aws_secret_access_key=environ['AWS_SECRET_KEY'], region_name=AWS_REGION)
    print('-> AWS S3 Client configured')
    print(f'-> Uploading {FEED_FILENAME}...', end='')

    try:
        s3.upload_file(FEED_FILENAME, AWS_S3_BUCKET, f'{FEED_FILENAME}', ExtraArgs={'ACL': 'public-read', 'StorageClass': 'STANDARD', 'ContentType': 'application/xml'})
        print('done!')
    except FileNotFoundError:
        print('file not found')
    except NoCredentialsError:
        print('err: no credentials')
        exit(EX_CONFIG)
    print('-> XML was published successfully')


if __name__ == '__main__':
    main()
