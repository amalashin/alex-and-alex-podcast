Title: Эпизод восемьдесят пятый
Date: 2021-10-24 12:00
Tags: vmworld2021, vmware, vrealize, wone, access, nsx, vsphere, security, intel
Summary: Закончился VMworld, отгремели громкие анонсы, работа продолжается. Статьи, конференции - мы собрались с Лёхой поговорить о том, что у нас впереди, а также обсудить всё новое, что появилось, а ещё кое-что старое (загрузочные USB), что уйдёт из функционала продуктов VMware. Присоединяйтесь!

# Новости от ЛёхиР (alex8s)
- Доступна [библиотека презентаций с VMworld 2021](https://www.vmware.com/vmworld/en/video-library/search.html#year=2021)
- Кнопка [vRealize Operations Manger в WS1 Access](https://debay.blog/2020/09/14/add-vrealize-operation-manager-in-the-ws1-app-catalog/)
- Разбор обновлённого [WS1 Access Connector 21.08](https://techzone.vmware.com/blog/its-time-upgrade-latest-version-vmware-workspace-one-access-connector)
- Подборка книг, чтобы [прокачать знания по сетям и NSX](https://nicovibert.com/2021/07/09/learning-networking-list-of-resources/)
- Подготовка к [миграции на Windows 11](https://blogs.vmware.com/euc/2021/10/ease-your-upgrade-to-microsoft-windows-11-with-vmware-workspace-one.html)

# Новости от ЛёхиМ
- vSphere 7u3 - нововведения и отказ от SD/USB, портал [Core](https://core.vmware.com)
- [Подход Cloud-Smart в эру Мульти-облака](https://news.vmware.com/releases/vmworld-2021-cross-cloud-services)
- [Модернизация приложений на любом облаке](https://news.vmware.com/releases/vmworld-2021-modern-apps)
- Истории успеха: 
  - [Toast](https://www.youtube.com/watch?v=6Dx3ZYvVGeI)
  - [Space Ape Games](https://www.youtube.com/watch?v=kovlBW2o294)
  - [FedEx](https://www.youtube.com/watch?v=8sEx1qWp7wE)
- [Эволюция кибербезопасности: как адаптироваться к киберугрозам завтрашнего дня](https://blogs.vmware.com/customer-experience-and-success/2021/07/the-evolution-of-cybersecurity-how-to-adapt-to-the-cyberthreats-of-tomorrow.html)
- Мероприятие: [VMware на Intel ON](https://reg.oneventseries.intel.com/flow/intel/innovation2021/reg/login)

# Дополнительно
- [КРИ-П](https://www.youtube.com/c/КРИПы) - Критические Работы по Инфраструктуре - в Пятницу
