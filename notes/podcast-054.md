Title: Эпизод пятьдесят четвёртый
Date: 2020-07-14 12:00
Tags: tkg, tanzu, k8s, vrealize, datrium, blue medora, tmc, spring, serverless, blockchain, ios, kerberos
Summary: Внезапно про блокчейн от VMware и Spring - свежие новости, прямо из облака!

# Новости облаков и автоматизации
- Свежий релиз [Tanzu Kubernetes Grid Integrated Edition](https://docs.pivotal.io/tkgi/1-8/release-notes.html)
- vRealize Cloud Management [получил первое место в отчёте IDC](https://www.vmware.com/learn/571311_REG.html?src=wb_5ee28a53c08e5&cid=7012H000001YmbbQAC)
- Компания VMware [заявила о намерениях приобретения Datrium](https://www.datrium.com/blog/datrium-acquired-by-vmware/)
- [Blue Medora True Visibility Suite](https://bluemedora.com/products/vmware-vrealize-true-visibility/) теперь часть VMware
- В Tanzu Mission Control [появилась опция защиты данных](https://tanzu.vmware.com/content/blog/vmware-tanzu-mission-control-adds-data-protection-for-kubernetes)
- [Конференция SpringOne](https://springone.io)
- Почему [Serverless - это важно](https://blogs.vmware.com/services-education-insights/2020/06/why-you-should-consider-adding-serverless-compute-capability-to-existing-applications.html)

# Новости EUC
- Будущее бизнеса [с blockchain технологиями](https://octo.vmware.com/the-future-of-business/)
    - [Масштабируемая архитектура распределённого доверия](http://www.cs.unc.edu/~reiter/papers/2019/DSN.pdf)
- 5 интересных [особенностей в vSphere 7](https://www.virtuallyghetto.com/2020/06/five-of-my-favorite-enhancements-in-vsphere-7.html)
- iOS 14 [изменения в конфигурации профилей](https://emm.how/t/ios-14-changes-in-configuration-profiles/1285)
- iOS 10-14 [название bundle ID приложений в iOS](https://emm.how/t/ios-12-list-of-default-apps-and-bundle-id-s/790)
- [Инициация пользователя в iOS 13 / macOS 10.15 Catalina](https://simplemdm.com/apple-user-enrollment/)
- Настройка [Kerberos Integrated Delegation для почты вместе с SEG](https://mobile-jon.com/2020/07/03/kerberos-drives-a-seamless-email-experience/)

# Дополнительно
- [КРИ-П](https://www.youtube.com/channel/UClg6v5K2QlMOdBf2ffGEJKw) - Критические Работы по Инфраструктуре - в Пятницу
- [CREEP](https://www.youtube.com/watch?v=qjdY1g0eL1M&list=PL0aZOZ-cWW_V4-5VFDBG0kBrOeSWdx0Sw) - Commit Every Experiment in Production
