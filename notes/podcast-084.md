Title: Эпизод восьмидесят четвёртый
Date: 2021-09-27 12:00
Tags: springone, azure, tanzu, vmware, cloud directo, nvidia, nsx, wone
Summary: Осенним утром мы с Лёхой собрались, чтобы поговорить о новостях по следам конференции SpringOne, а также отметить долгожданный выход новой версии портала Workspace ONE Access 21.08. Всё это и другие новости приходите к нам послушать, заварив чайку или кофий.

# Новости от ЛёхиМ 
- SpringOne 2021 - [обзор](https://tanzu.vmware.com/content/blog/springone-2021-day-1-recap-and-highlights) и [основные моменты](https://tanzu.vmware.com/content/blog/springone-2021-day-2-recap-and-highlights)
  - [Azure Spring Cloud Enterprise Tier](https://tanzu.vmware.com/content/blog/introducing-azure-spring-cloud-enterprise-tier)
  - [Tanzu Application Platform](https://tanzu.vmware.com/content/blog/announcing-vmware-tanzu-application-platform)
  - EBook [State of Spring 2021](https://tanzu.vmware.com/content/ebooks/the-state-of-spring-2021)
- Миграция NSX для vCloud Director 1.3 - [технический обзор](https://blogs.vmware.com/cloudprovider/2021/08/vmware-nsx-migration-for-vmware-cloud-director-1-3-technical-overview.html)
- VMware Cloud Disaster Recovery - [что новго?](https://blogs.vmware.com/virtualblocks/2021/09/02/whats-new-30-minute-rpo/)
- NVIDIA AI Enterprise - [платформа AI от NVIDIA, эксклюзивно для vSphere](https://www.nvidia.com/en-us/data-center/products/ai-enterprise-suite/?ncid=partn-652761-p14#cid=hpc09_partn_en-us)


# Новости от ЛёхиР (alex8s) 
- Курсы по [Cloud Assembly и интеграции NSX-T с vRA бесплатно от LiveFire](https://www.livefire.solutions/e-learning-content/)
- Вышел [Workspace ONE Access 21.08](https://docs.vmware.com/en/VMware-Workspace-ONE-Access/21.08/rn/VMware-Workspace-ONE-Access-2108-Release-Notes-On-Premises.html)
- В новом Workspace ONE Access 21.08 [наличествует коннектор с поддержкой VDI](https://mobile-jon.com/2021/09/13/workspace-one-access-connector-2108-you-had-me-at-virtual-apps/)
- Инструмент [для отрисовки топологии Workspace ONE](https://topologytool.techzone.vmware.com)
- Обновление токена [OAuth для доступа к REST API Workspace ONE UEM](https://www.brookspeppin.com/2021/09/07/how-to-renew-oauth-tokens-in-workspace-one-uem/#more-3402)


# Дополнительно
- [КРИ-П](https://www.youtube.com/c/КРИПы) - Критические Работы по Инфраструктуре - в Пятницу (теперь и видеоверсия нас).
- Весенние вебинары: [анонсы](https://www.youtube.com/playlist?list=PLSHgmTQvHHVFg6C-d6_ncX8O0buyY0Jce)
- Летняя академия: 
