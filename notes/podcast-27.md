Title: Эпизод двадцать седьмой
Date: 2019-12-02 10:00
Tags: wone, uem, horizon, mdm, pks, harbor, pacific, vra, skyline
Summary: 1 Декабря - первый день зимы, и время выхода нового эпизода нашего подкаста!

# Новости EUC
- Что Workspace One UEM собирает о пользовательских устройствах. [Privacy](https://www.vmware.com/help/privacy.html)
- Подробный [список Privacy данных](https://www.vmware.com/help/privacy/uem-privacy-disclosure.html)
- Подробная [таблица Privacy для разных платформ](https://docs.vmware.com/en/VMware-Workspace-ONE-UEM/1910/UEM_Managing_Devices/GUID-AWT-DATA-COLLECT-MATRIX.html)
- Конфигурация [Horizon + UAG](https://techzone.vmware.com/configuring-horizon-edge-service-vmware-unified-access-gateway-vmware-horizon-operational-tutorial)
- Что такое [Modern Management](https://blog.eucse.com/are-you-ready-for-modern-management/) и современное управление устройствами пользователей

# Новости облаков и автоматизации
- Вышел в GA Enterprise PKS PKS 1.6: [заметки о релизе](https://docs.vmware.com/en/VMware-Enterprise-PKS/1.6/rn/VMware-Enterprise-PKS-16-Release-Notes.html) и [ссылки на загрузку](https://my.vmware.com/en/web/vmware/info/slug/infrastructure_operations_management/vmware_enterprise_pks/1_6)
- Harbor 1.9.3 - что новго и [заметки о релизе](https://docs.vmware.com/en/VMware-Enterprise-PKS/1.0/rn/VMware-Harbor-Release-Notes.html#harbor-pks-release-notes-v1.9.3)
- Роадмап и новые фичи [Harbor 1.10](https://github.com/goharbor/harbor/releases/tag/v1.10.0-rc1)
- [Подписка на бета-версию Project Pacific](https://blogs.vmware.com/vsphere/2019/11/interested-in-the-project-pacific-beta.html) (читай vSphere 7)
- Ноябрьский релиз vRealize Automstion 8 - [интеграция с OpenShift и группы безопасности NSX](https://blogs.vmware.com/management/2019/11/ca-supports-os.html)
- Продвинутая поддержка [Skyline и интеграция с Dell Support Assist](https://blogs.vmware.com/kb/2019/11/announcing-updates-to-vmware-skyline-november-2019.html), [список ответов на вопросы](https://kb.vmware.com/s/article/55928) и [собираемые данные](https://kb.vmware.com/s/article/71071)
