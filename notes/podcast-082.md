Title: Эпизод восьмидесят второй
Date: 2021-08-06 12:00
Tags: vrealize, ai/ml, vmware, cvaas, security, tanzu, modern app, uem, certificates
Summary: Только что прошли несколько наших сессий Летней Академии. Мы с Лёхой делились с вами, кто подключался, интересными подробностями по управлению устройствами, построению платформ MLOps, брали интервью у экспертов. В качестве послевкусия мы собрались обсудить сегодняшние новости по этим темам. Присоединяйтесь!

# Новости от ЛёхиМ 
- vRealize 8.5 - [что нового](https://blogs.vmware.com/management/2021/07/whats-new-in-vrealize-v8-5.html)
- Дашборды vRealize Operations - [просто и понятно](https://blogs.vmware.com/management/2021/06/vrealize-operations-dashboards-made-easy-part-1-2-introduction-to-dashboards.html)
- Автоматическая валидация как сервис (CVaaS) - [каталог VMware](https://youtu.be/NsCUCX9wI6Q)
- [Чек-лист по безопасности](https://www.vmware.com/content/dam/digitalmarketing/vmware/en/pdf/docs/vmw-security-checklist-vrealize-cloud-management.pdf) для управления при помощи vRealize Cloud
- Tanzu Standard - [восьмиминутный обзор](https://youtu.be/EDUKwKn3g8I)
- [Технические руководства от партнёров](https://core.vmware.com/blog/technical-guides-vmware-and-partners-infrastructure-aimachine-learning) по развёртыванию нагрузок AI/ML на платформе VMware
- [Мультитенантность для современных приложений](https://blogs.vmware.com/vov/2021/07/02/vmware-it-modern-application-platform-addresses-automation-and-multitenancy/) - как VMware IT достигло желаемого результата
- [Пошаговое руководство](https://docs.bitnami.com/tutorials/assign-pod-nodes-helm-affinity-rules/) по назначению подов на ноды для helm-чартов (от Bitnami)

# Новости от ЛёхиР (alex8s) 
- "Федеративное обучение" - работа с [данными для машинного обучения](https://octo.vmware.com/federated-machine-learning-overcoming-data-silos-and-strengthening-privacy/)
- Наименьшие привелегии и [DevSecOps идеология от VMware CTO](https://octo.vmware.com/modern-least-privilege-and-devsecops/)
- Работа с VMware Workspace ONE UEM [профилями по принципу CI/CD](https://mobile-jon.com/2021/07/26/vmware-forklift-supersize-your-change-management-strategy-in-workspace-one/) 
- Выбор сертификата в зависимости от домена: [сертификаты в браузерах на Windows](https://digitalworkspace.one/2021/07/21/certificate-picker-for-sso-on-windows-browsers/)
- Выбор [номера протокола для МЭ на Windows](https://digitalworkspace.one/2021/07/18/kb-windows-firewall-custom-port/)


# Дополнительно
- [КРИ-П](https://www.youtube.com/c/КРИПы) - Критические Работы по Инфраструктуре - в Пятницу (теперь и видеоверсия нас).
- Весенние вебинары: [анонсы](https://www.youtube.com/playlist?list=PLSHgmTQvHHVFg6C-d6_ncX8O0buyY0Jce)
- Летняя академия: 
