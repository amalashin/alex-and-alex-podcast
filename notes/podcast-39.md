Title: Эпизод тридцать девятый
Date: 2020-03-06 12:00
Tags: flings, afe, android, wone, uem, certificates, ios, ai/ml, bitfusion, meetup
Summary: Новый, свежий, пятнично-предпразничный эпизод уже ждёт вас в облаке 💐! Присоединяйтесь!

# Новости EUC
- [Утилита синхронизации стеков приложений между геораспределёнными площадками](https://flings.vmware.com/app-volumes-entitlement-sync)
- Подробности о Android for Enterprise: [доп фишки разделения рабочего профиля для BYOD сценария](https://blogs.vmware.com/euc/2020/02/android-work-profile.html)
- [Что нового с Android for Enterprise](https://developer.android.com/preview/work) в Android 11
- [Предсказания на 10 лет в ИТ от CIO VMware](https://octo.vmware.com/cto-view-11-predictions-for-2030/)
- [Видео про интеграцию Workspace One UEM с Certificate Authority](https://techzone.vmware.com/vmware?share=demo1971)
- [Подробная статья по интеграции с Microsoft Certificate Authority](https://digital-work.space/display/AIRWATCH/AirWatch+integration+ADCS+via+DCOM)
- [Обзор безопасности платформы iOS](https://mobile-jon.com/2018/07/18/ios-security-overview/)

# Новости облаков и автоматизации
- Как использовать метки в vSphere: [Best Practices for vSphere 6.7 Tagging](https://blogs.vmware.com/vsphere/2020/02/best-practices-for-vsphere-6-7-tagging.html)
- Возвращаемся к основам: [как работает трансляция памяти ВМ](https://blogs.vmware.com/vsphere/2020/03/how-is-virtual-memory-translated-to-physical-memory.html)
- [Новые уязвимости процессоров Intel и как это влияет на платформу виртуализации](https://blogs.vmware.com/vsphere/2020/01/vsphere-intel-cpu-l1d-cacheout-vector-register-sampling.html)
- [Разговор про ML и виртуализацию](https://blogs.vmware.com/ml-ai/2019/11/25/network-attached-full-or-partial-gpus-to-any-kubernetes-cluster-using-bitfusion-flexdirect/) - как, когда, зачем и почему?

# Дополнительно
**Митап VMware: Мир разработчика и что ему нужно от ИТ (19 Марта 2020г). [Пообщаемся?!](https://vmware-rus.timepad.ru/event/1277968/)**
