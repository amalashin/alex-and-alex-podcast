Title: Эпизод пятнадцатый
Date: 2019-09-09 10:00
Tags: horizon, zero-trust, nsx, vmworld, wone, vra, vio, openstack
Summary: Лёха&Лёха, а так же гости - Серёга и Серёга в студии, слушаем, что получилось :)

# Новости EUC
- Безопасность в среде Horizon и модель Zero-Trust: код видео с VMworld 2019 US - ADV1761BU, документ [Horizon Security](https://docs.vmware.com/en/VMware-Horizon-7/7.9/horizon-security.pdf), NSX-T и Horizon - код видео ADV1313BU, [статья по соединениям в Horizon](https://techzone.vmware.com/blog/understanding-horizon-connections), [**список ссылок на записи сессий VMworld 2019 US**](https://github.com/lamw/vmworld2019-session-urls/blob/master/vmworld-us-playback-urls.md)
- Нагрузка от запроса сертификатов в Workspace One профилях, статья про [разбор логики и разных сценариев доставки профилей с сертификатами на устройства](https://digital-work.space/display/AIRWATCH/Certificate+batch)

# Новости облаков и автоматизации
- [vRealize Automation 8.0](https://blogs.vmware.com/management/2019/08/vrealize-automation-8-whats-new-overview.html): обсуждаем с Сергеем Калугиным основные изменения в архитектуре сервиса, новые понятия, установка, интеграции, расширения, оркестрация, миграция и тд.
- Вышел VIO 6.0 - [OpenStack от VMware на базе релиза Stein](https://docs.vmware.com/en/VMware-Integrated-OpenStack/6.0/rn/VMware-Integrated-OpenStack-60-Release-Notes.html)

# Дополнительно
- Сергей Лукьянов про Летнюю Академию VMware и [весенние вебинары]( https://onlinexperiences.com/Server.nxp?LASCmd=AI:4;F:APIUTILS!51004&PageID=7001FDBF-55BC-45EE-BB4B-DED132BF16BE), ответ на загадку
- Новый вопрос
