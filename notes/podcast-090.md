Title: Эпизод девяностый
Date: 2022-02-01 12:00
Tags: horizon, wsone, pci-dss, security, euc, future, nvidia, vmware, mwc
Summary: Сегодня Лёха изображал ясновидящего - мы с ним обсудили 11 предсказаний в EUC на 2022 год. Кроме этого многое происходит в мире ИБ: отчёты, статьи, сертификации, новая версия инструмента для журналирования. Всё это на нашем новом крипкасте. Завариваем чаёк/кофий и погружаемся!

# Новости от ЛёхиМ 
- Вышел [Network Insight 6.5](https://blogs.vmware.com/management/2022/01/announcing-vmware-vrealize-network-insight-6-5-and-cloud.html)
- Каталог вебинаров VMware, [повышение безопасности цепочки поставок ПО](https://tanzu.vmware.com/content/webinars/jan-25-secure-your-software-supply-chain-with-vmware-application-catalog)
- VMware IT: [миграция нагрузок](https://blogs.vmware.com/vov/2022/01/12/how-vmware-it-makes-workload-migrations-easy-2/)
- Безопасность: [отчёт VMware за 2021 год](https://blogs.vmware.com/vov/2022/01/12/security-transformation-highlights-from-the-vmware-it-performance-annual-report-2021-part-five/)
- [MWC 2022](https://www.mwcbarcelona.com)
- [NVIDIA GTC](https://www.nvidia.com/gtc/)

# Новости от ЛёхиР (alex8s) 
- Взгляд в будущее: [11 предсказаний о EUC в 2022](https://blogs.vmware.com/euc/2022/01/vmwares-euc-cto-office-makes-11-predictions-for-euc-in-2022.html)
- Уязвимость [Log4j, заметка для администраторов Horizon](https://blogs.vmware.com/euc/2022/01/guidance-to-vmware-horizon-customers-regarding-log4j.html)
- Horizon VDI: [мониторинг на стороне клиента VDI с ControlUp](https://blogs.vmware.com/euc/2022/01/get-started-with-controlup-remote-dx-and-vmware-horizon.html)
- Тонкости [записи экрана в Horizon](http://blog.vmpress.org/2022/01/vmware-horizon-recording-faq.html)
- Сертификация [PCI-DSS для услуг Workspace ONE](https://blogs.vmware.com/euc/2022/01/workspace-one-access-and-hub-services-workspace-one-intelligence-workspace-one-assist-and-vmware-remotehelp-achieve-pci-dss-compliance.html)

# Дополнительно
- [КРИ-П](https://www.youtube.com/c/КРИПы) - Критические Работы по Инфраструктуре - в Пятницу (теперь и видеоверсия нас).
