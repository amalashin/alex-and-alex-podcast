Title: Эпизод пятьдесят третий
Date: 2020-07-03 12:00
Tags: appvolumes, ios, windows, software, lastline, fusion, vra, carbon black, vov
Summary: Лето, солнце и жара, и подкасты до утра! Прямо из облака!

# Новости EUC
- Группы хранилищ и [работа с шаблонами в AppVolumes](https://docs.vmware.com/en/VMware-App-Volumes/4/com.vmware.appvolumes.admin.doc/GUID-174CA732-BFC6-4930-BADB-656E79C19369.html)
- Как забирать [логи с iOS с помощью OS Windows](https://www.utest.com/articles/how-to-capture-ios-console-logs-on-windows-pc)
- Управляемая [отсрочка установки ПО на Windows](https://brookspeppin.com/2020/06/23/app-install-deferrals-psadt-with-workspace-one-tech-preview/)
- Соображения о [развёртывании ПО разных видов на Windows](https://www.vmware.com/content/dam/digitalmarketing/vmware/en/pdf/techpaper/vmw-windows-application-delivery-design-considerations.pdf)


# Новости облаков и автоматизации
- Закрылась сделка [по приобретению Lastline](https://www.lastline.com)
- VMware Fusion 11.x не поддерживается на macOS Big Sur (но скоро будет)
- По мнению Gartner (и по опросам пользователей) решения по мониторингу VMware являются лучшим выбором и получили награду [Gartner Peer Insights Customers' Choice 2020](https://blogs.vmware.com/management/2020/05/vmware-named-gartner-peer-insights-customers-choice.html)
- Облачный vRealize Automation - [June Launch](https://cloud.vmware.com/community/2020/06/24/vrealize-automation-cloud-06-20-launch-update/)
- Пять новых технических курсов по Carbon Black (можно объединить в единый трэк):
  - [VMware Carbon Black EDR Administrator]( https://mylearn.vmware.com/mgrReg/courses.cfm?ui=www_edu&a=one&id_subject=92499&rcode=vaultfieldnews)
  - [VMware Carbon Black EDR Advanced Administrator](https://mylearn.vmware.com/mgrReg/courses.cfm?ui=www_edu&a=one&id_subject=92512&rcode=vaultfieldnews)
  - [VMware Carbon Black EDR Advanced Analyst](https://mylearn.vmware.com/mgrReg/courses.cfm?ui=www_edu&a=one&id_subject=92515&rcode=vaultfieldnews)
  - [VMware Carbon Black App Control Administrator](https://mylearn.vmware.com/mgrReg/courses.cfm?ui=www_edu&a=one&id_subject=92493&rcode=vaultfieldnews)
  - [VMware Carbon Black App Control Advanced Administrator](https://mylearn.vmware.com/mgrReg/courses.cfm?ui=www_edu&a=one&id_subject=92496&rcode=vaultfieldnews)
- Вёбинар VMware on VMware - как компания мигрировала крупнейшие нагрузки в новый ЦОД [регистрация](https://vmware.zoom.us/webinar/register/WN_q8jiFUWITmS-v9CvdRAEhw)

# Дополнительно
- [КРИ-П](https://www.youtube.com/channel/UClg6v5K2QlMOdBf2ffGEJKw) - Критические Работы по Инфраструктуре - в Пятницу
