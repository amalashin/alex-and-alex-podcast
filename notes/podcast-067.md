Title: Эпизод шестьдесят седьмой
Date: 2020-12-09 10:00
Tags: techzone, azure, workspace one, tanzu, sql, postgres, raspberrypi, security, vsphere, vsan
Summary: Первые дни зимы на дворе, время заварить тёплый чай/кофе, уютно устроиться и узнать что-то интересное - мы с Лёхой подготовили для Вас подборку новостей и разбор свежих новинок в мире VMware и около. Покрипкастим вместе!

# Новости управления рабочими местами
- Инструкция по настройке [Freestyle Orchestrator](https://techzone.vmware.com/getting-started-freestyle-orchestrator)
- На Techzone появились архитектурные документы на все [компоненты Workspace One](https://techzone.vmware.com/reference-architecture)
- Инициация устройств с помощью [Azure Active Directory Domain Join, синхронизация учётных записей с WS1 UEM](https://mobile-jon.com/2020/12/01/extending-a-full-workspace-one-cloud-to-azure-ad-join/)
- Нужно построить [свой Proxy для связи между Azure Active Directory и Workspace One Access](http://blog.tbwfdu.com/2020/11/rollcall.html)


# Новости облаков и автоматизации
- [Супер скрипт разворачивания лабы по Tanzu](https://www.virtuallyghetto.com/2020/10/automated-vsphere-with-tanzu-lab-deployment-script.html) от William Lam
- VMware Tanzu SQL - [база данных для Kubernetes на базе Postgres](https://tanzu.vmware.com/content/blog/vmware-tanzu-sql-now-ga-kubernetes-postgres)
- Запуск устаревшего кода .net в контейнере, в облаке? изи - [как это сделать](https://tanzu.vmware.com/content/blog/run-your-legacy-net-in-the-cloud-with-tanzu)
- [Raspberry Pi, как арбитр для vSAN? Пожалуйста!](https://www.storagereview.com/review/how-to-raspberry-pi-as-a-vsan-witness)
- Вебинар по безопасности [Передовые методы автоматизации для защиты от угроз](https://www.carbonblack.com/resources/intrinsic-security-best-practices-for-using-automation-to-simplify-and-improve-threat-protection/?lp=1&utm_source=vmware_internal&utm_medium=my_field_newsletter&utm_campaign=intrinsic&utm_term=vmware-additional-description&utm_content=webinar)
- Познакомиться, [как работает диагностика vSphere и vSAN в новой версии](https://docs.vmware.com/en/VMware-Skyline-Health-Diagnostics/index.html)
- Microsoft [превращает Teams в комбайн](https://www.zdnet.com/article/microsoft-makes-power-platform-tools-dataverse-data-platform-available-for-teams/#ftag=RSSbaffb68)

# Дополнительно
- [КРИ-П](https://www.youtube.com/c/КРИПы) - Критические Работы по Инфраструктуре - в Пятницу
