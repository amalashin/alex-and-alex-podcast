Title: Эпизод тридцать шестой
Date: 2020-02-14 16:00
Tags: licensing, partners, vmc, testdrive, cloud, kb, wone, uem, ens, appvolumes, 
Summary: Садитесь поудобнее в наушниках и ловите свежий подкаст от Лёхи и Лёхи - он уже ждёт вас в облаке!

# Новости облаков и автоматизации
- Изменение системы лицензирования VMware, [теперь по ядрам](https://www.vmware.com/company/news/updates/cpu-pricing-model-update-feb-2020.html)
- Партнёрское мероприятие - новая программа, [смотрим онлайн](https://www.vmware.com/uk/partner-connect-live.html)
- Миграция в облако - взгляд VMware и преимущества VMC ([вебинар](https://www.vmware.com/learn/452984_REG.html))
- Новая схема лицензирования облачной услуги - [закажи сейчас, плати помесячно](https://cloud.vmware.com/community/2020/01/30/commit-now-pay-monthly/)
- [TestDrive](https://portal.vmtestdrive.com/) получил апгрейд до последних версий продуктов, изучаем новые возможности
- Почему стоит использовать гибридные облака - [мнения 5 ИТ-Лидеров в отрасли](https://www.vmware.com/radius/hybrid-cloud-benefits/)
- [Необычная уязвимость Jenkins](https://nvd.nist.gov/vuln/detail/CVE-2020-2100) - будем внимательны!
- [Solutions Lab](https://blogs.vmware.com/apps/vslab) - как можно применять решения VMware для решения разных задач
- Интересные статьи БЗ:
    - Самая популярная статья за последние 2 месяца: [Ссылки на vSphere Client (2089791)](https://kb.vmware.com/s/article/2089791)
    - По прежнему в топах: [Поддерживаемые ОС для vCenter (2091273)](https://kb.vmware.com/s/article/2091273)
    - Новая статья: [Подключение через VNC к ВМ с несколькими 4k-мониторами ведёт к краху сервиса (76793)](https://kb.vmware.com/s/article/76793)
    - Удалённая консоль ВМ: [Изменение порта клиентского доступа, теперь 443 (76672)](https://kb.vmware.com/s/article/76672)
    - Ухаживаем за vCenter: [Обслуживание дискового пространства VCSA (76563)](https://kb.vmware.com/s/article/76563)

# Новости EUC
- Вышла новая версия Workspace ONE UEM 20.01 (SaaS). [Новшества.](https://docs.vmware.com/en/VMware-Workspace-ONE-UEM/2001/rn/VMware-Workspace-ONE-UEM-Release-Notes-2001.html)
- [Видео по новшествам WOne UEM 20.01](https://youtu.be/7dwS7uBsYiQ).
- Особенности подключения ENSv2 к CNS, [зачем заводить тикет](https://digital-work.space/pages/viewpage.action?pageId=13533236)
- Подключение [нескольких vGPU в виртуальной машине](http://www.yellow-bricks.com/2020/02/01/adding-multiple-vgpus-to-a-virtual-machine/)
- Сравнение скорости [vSGA против vGPU](https://blogs.vmware.com/performance/2020/01/vmware-vsga-for-content-rich-vdi.html)
- Новый [курс обучения AppVolumes 4.0 (BETA)](https://mylearn.vmware.com/mgrReg/courses.cfm?ui=www_edu&a=one&id_subject=92669) вслед за релизом инструмента
- [Демонстрация работы](https://techzone.vmware.com/vmware?share=demo1785) AppVolumes 4.0

# Дополнительно
Борьба с шумом и отвлекающими факторами в Open Space, звуковыми и визуальными: [как шум негативно влияет на здоровье](https://www.ncbi.nlm.nih.gov/pubmed/24083264), [снижает продуктивность](https://www.techradar.com/news/audio/how-your-noisy-open-plan-office-is-making-you-66-less-productive-1148580) и даже может [вызвать серьёзные проблемы](https://www.scientificamerican.com/article/ask-the-brains-background-noise/), [повысить усталость](https://www.wsj.com/articles/the-biggest-office-interruptions-are-1378852919) и [ухудшить память](https://www.theatlantic.com/magazine/archive/2014/04/the-optimal-office/358640/); [решаем вопросы звукоизоляции](http://cambridgesound.com/wp-content/uploads/2015/09/CSM-WP-The-Workplace-Speech-Privacy-Crisis_Web.pdf), [применяем для этого спец-средства](https://www.audiomania.ru/content/art-5978.html) и [закрываемся от посторонних](https://www.inc.com/geoffrey-james/more-proof-as-if-needed-it-that-open-plan-offices-are-absolute-dumbest-idea-ever.html).
