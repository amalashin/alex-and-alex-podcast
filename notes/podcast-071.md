Title: Эпизод семьдесят первый
Date: 2021-02-05 10:00
Tags: vsan, ci/cd, wone, euc, trends, monitoring, vr, networking
Summary: Первый день февраля - первый день последнего месяца зимы, а также первый день нового финансового года в VMware. Это повод собраться с Лёхой и обсудить, что интересного накопилось на сегодня из новостей и что нового ждать от компании. Приходите и Вы к нам на огонёк.

# Новости от ЛёхиР (alex8s)
- Возвращение VSAN на Horizon. Шаг назад - два вперёд;
- Что такое [CI/CD конвеер с точки зрения VMware](https://octo.vmware.com/enterprise-ci-cd-holistic-view/);
- Работа с [Workspace ONE MacOS Hub CLI](https://mobile-jon.com/2021/01/25/an-introduction-to-the-workspace-one-macos-hub-cli/);
- Настройка [Workspace One Conditional Access для Android](https://darrylmiles.blog/2019/03/16/setting-up-workspace-one-single-sign-on-sso-and-conditional-access/) - (ещё один) потерянный мануал.

# Новости от ЛёхиМ
- EUC набирает обороты - [новый протокол для доступа к рабочим местам у AWS](https://aws.amazon.com/blogs/desktop-and-application-streaming/reimagining-end-user-computing/)
- Обзор Microsoft по [самым используемым приложениям в магазине за 2020г](https://www.microsoft.com/security/blog/?p=92631)
- Свежие мысли о технологиях от Sanjay Poonen, теперь о [финансовом секторе](https://www.forbes.com/sites/vmware/2021/01/19/faster-to-the-future-of-banking/)
- Коллеги по аудиоцеху [обсудили тёмные закоулки ИТ при помощи True Visibility Suite](https://packetpushers.net/podcast/tech-bytes-vmwares-vrealize-true-visibility-suite-illuminates-dark-corners-of-your-it-stack-sponsored/)
- [Новый релиз Skyline](https://docs.vmware.com/en/VMware-Skyline-Advisor/services/rn/VMware-Skyline-Advisor-Release-Notes.html), включает самые часто запрашиваемые функции
- [Обновлённый каталог VMware Marketplace](https://cloud.vmware.com/community/2021/01/21/new-vmware-marketplace-simplified-search-solution-cloning/) - новые решения, проще поиск и дополнительные фишки
- Виртуальная реальнось от VMware меняет подходы к рабочим местам
- [Семь брошюр на русском по сетевым технологиям](https://alex-and-alex-podcast.s3.eu-central-1.amazonaws.com/networking.zip)

# Дополнительно
- [КРИ-П](https://www.youtube.com/c/КРИПы) - Критические Работы по Инфраструктуре - в Пятницу
