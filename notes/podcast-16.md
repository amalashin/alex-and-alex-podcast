Title: Эпизод шестнадцатый
Date: 2019-09-16 10:00
Tags: k8s, pks, vmworld, dem, wone, vra
Summary: Лёха&Лёха снова в эфире и сегодня у нас особенный гость - человек "оттуда", который делится своими впечатлениями про VMworld 2019 US, мыслями об автоматизации, k8s и процессах в большой компании.

# Новости облаков и автоматизации
- Kubernetes для начинающих: [Академия от VMware](https://kubernetes.academy), [Книга Kubernetes on vSphere for Dummies](https://k8s.vmware.com/kubernetes-on-vsphere-for-dummies/) и лабораторная работа [VMware Essentials PKS Getting Started](https://labs.hol.vmware.com/HOL/catalogs/lab/6639)
- [Презентации с VMworld 2019 US](https://github.com/lamw/vmworld2019-session-urls/blob/master/vmworld-us-playback-urls.md)

# Новости EUC
- Переименование User Environment Manager в Dynamic Environment Manager (DEM)
- [Новинки Workspace One в поддержке Android](https://blogs.vmware.com/euc/2019/09/workspace-one-uem-for-android.html)

# Дополнительно
- Взгляд заказчика (а как у них?): разговариваем с Александром Пыльневым про автоматизацию, кубер, VMworld 2019 US, и новый vRealize Automation
- Ответ на вопрос, победитель и новая загадка!
