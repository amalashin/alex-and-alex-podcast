Title: Эпизод двадцатый
Date: 2019-10-15 10:00
Tags: carbonblack, openstack, oracle, blockchain, ai/ml, nsx, horizon, interview
Summary: Пока мы ждём аппрува подкаста в Spotify и Я.Музыке, новый эпизод, как обычно, доступен прямо из облака!

# Новости облаков и автоматизации
- [VMware #1 в Cloud Systems and Service Management Software по мнению компании IDC](https://www.vmware.com/learn/330390_IDC_REG.html) (и [уже семь лет удерживает позиции](https://blogs.vmware.com/management/2019/10/vmware-idc-cloud-system-management-software-7-years-in-a-row.html))
- [Carbon Black стал частью VMware](https://www.vmware.com/company/news/releases/vmw-newsfeed.VMware-Completes-Acquisition-of-Carbon-Black.1926966.html) - сделка завершена
- SUSE перестаёт распространять OpenStack, [чтобы сосредоточиться на контейнерах и доставке приложений](https://www.suse.com/c/suse-doubles-down-on-application-delivery-to-meet-customer-needs/)
- [Oracle изменил политику поддержки своих продуктов в среде виртуализации VMware](https://blogs.vmware.com/apps/2019/10/oracle-on-vmware-support-policy-changes-oct-9-2019-metalink-note-249212-1.html)
- [Блокчейн от VMware](https://research.vmware.com/projects/vmware-blockchain) и исследовательские проекты (VMware Research)
- [Указ Президента РФ о развитии искуственного интеллекта в Российской Федерации](http://publication.pravo.gov.ru/Document/View/0001201910110003?index=1&rangeSize=1) (и пару слов о VMware)
- [Бесплатный курс](https://vmwarelearningzone.vmware.com/oltpublish/site/coursePlayer.do?dispatch=show&courseSessionId=5c0e080a-e426-11e9-b1f7-0cc47a3505aa) для подготовки к экзамену VMware Professional NSX-T Data Center 2.4
- vForum Online - глубокое погружение в технологии VMware, демонстрации, лабы и тд. [Присоединяйтесь 16 октября](http://bit.ly/35r843F)!
- Вебинар ["Что нового в Kubernetes 1.16"](https://www.cncf.io/webinars/whats-new-in-kubernetes-1-16/) (пройдёт 22 октября)

# Новости EUC
- Horizon Cloud в Azure [стал поддерживать Windows Virtual Desktop](https://blogs.vmware.com/euc/2019/09/windows-virtual-desktop-horizon-azure.html)
- [Портал по feature-запросам](https://kb.vmware.com/s/article/2960048?lang=en_US) <== загадка тут!
- Гость из Dell Technologies - разговор про EUC с Александром Тарасовым
