Title: Эпизод пятьдесят второй
Date: 2020-06-23 12:00
Tags: harbor, cncf, developers, tanzu, skyline, vrealize, vmworld2020, wone, hub, uag, horizon, amd, nvidia
Summary: Лето, солнце, технологии ИТ - всё смешалось! Мы с Лёхой отделяем ИТ от лета, и самые горячие новости - от солнца. 

# Новости облаков и автоматизации
- Репозиторий Harbor 2.0 достиг уровня "Graduated" в CNCF и стал 11-м проектом в этой категории
- Новый ресурс - [Developer TV](https://tanzu.vmware.com/developer/tv/)
- Бесплатный [воркшоп по Skyline](https://calendly.com/skyline-covid19-taskforce/skyline-workshop) - сотрудники поддержки VMware помогут установить, настроить и объяснят как пользоваться
- Вышла поваренная книга [vRealize Network Insight](https://networkinsightcookbook.com/vrealize-network-insight-cookbook/)
- Стартовала [регистрация на VMworld 2020](https://www.vmworld.com/en/index.html)

# Новости EUC
- Workspace One Access и Hub Services в облаке прошли [сертификацию по ISO 27001, ISO 27017 и ISO 27018](https://cloud.vmware.com/trust-center)
- Глубокое погружение в [UAG от Mobile John](https://mobile-jon.com/2020/06/17/deep-dive-into-building-ws1-uem-components-on-the-uag/)
- Настройка [SSO в macOS](https://blogs.vmware.com/euc/2020/06/managing-identity-preferences-to-streamline-single-sign-on-for-macos-revisited.html)
- Настройка учётных записей [администратора Instant Clones в Horizon API](https://www.retouw.nl/horizon/horizonrestapi-handling-instant-clone-administrator-accounts/)
- AMD vs NVIDIA [сравнение технологий vGPU видеокарт](https://searchvirtualdesktop.techtarget.com/opinion/Comparing-AMD-vs-Nvidia-for-virtual-desktop-GPU-cards)
- Проброс встроенной видеокарты в [Intel NUC внутрь виртуальной машины](https://www.virtuallyghetto.com/2020/06/passthrough-of-integrated-gpu-igpu-for-standard-intel-nuc.html)

# Дополнительно
- [КРИ-П](https://www.youtube.com/channel/UClg6v5K2QlMOdBf2ffGEJKw) - Критические Работы по Инфраструктуре - в Пятницу
