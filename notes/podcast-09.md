Title: Эпизод девятый
Date: 2019-07-24 12:00
Tags: wone, airwatch, apns, android, ai/ml, pso, bitfusion, interview
Summary: Эпизод выдался интересный - нас было четверо! А в конце вас ждёт интервью с победителем первого приза.

# Новости EUC
- [Новая версия VMware Workspace One UEM / Airwatch 1907](https://docs.vmware.com/en/VMware-Workspace-ONE-UEM/1907/rn/VMware-Workspace-ONE-UEM-Release-Notes-1907.html), [как проверить работу APNS](https://digital-work.space/display/AIRWATCH/APNs+troubleshooting+in+SQL), изменение логики синхронизации с LDAP и [Android 9.0 for work and personal passcodes](https://docs.vmware.com/en/VMware-Workspace-ONE-UEM/1907/Android_Platform/GUID-AWT-PROFILE-PASSCODE.html)
- Биты и байты: два способа управления ОС Android ([матрица подддержки](https://androidenterprisepartners.withgoogle.com/devices/)), драйверы от производителей ([POEM Service](https://digital-work.space/display/AIRWATCH/Matrix#Matrix-platforms)), [миграция на Google стандарт с помощью нового инструмента](https://docs.vmware.com/en/VMware-Workspace-ONE-UEM/1907/Android_Platform/GUID-48D742FE-79FA-4134-9BB2-E80622099A07.html)

# Новости облаков и автоматизации
- ML/AI в продуктах VMware, [приобретение компании Bitfusion](https://blogs.vmware.com/vsphere/2019/07/vmware-to-acquire-bitfusion.html)
- Об истории и том, что VMware - это не только виртуализация

# Дополнительно
- Александр Старостин рассказывает про подразделения VMware, что от них ждать и куда потратить PSO-кредиты
- Рубрика Мириана - загадка!
- Интервью с победителем (Евгений)
