Title: Эпизод пятьдесят пятый
Date: 2020-07-24 12:00
Tags: horizon, android11, intune, wone, uag, datrium, kubernetes, agile, tanzu, tkg, spring
Summary: До конца года остаётся меньше половины. И, безусловно, лето - это самое лушее время изучить новые технологии VMware. Прямо из облака ☁️!

# Новости EUC
- Проверка на источник подключения [Origin Check в Horizon](https://digital-work.space/display/HORIZON/Horizon+Origin+Checking)
- [Бета-версия Android 11](https://madereal.blog/2020/06/10/android-11-beta-public-available/)
- Сравнение [Microsoft Intune vs VMware Workspace One](https://mobile-jon.com/2020/07/13/evaluating-intune-against-workspace-one-uem/)
- Утилита-[инсталлятор Unified Access Gateway](https://flings.vmware.com/unified-access-gateway-deployment-utility)


# Новости облаков и автоматизации
- [Datrium стал частью VMware](https://blogs.vmware.com/virtualblocks/2020/07/17/vmware-acquires-datrium-for-draas/)
- Новые возможности [VMC on AWS](https://cloud.vmware.com/community/2020/07/02/next-gen-host/)
- YAML пишем, кубернетес в уме:
    - [Настройка Workload'ов](https://tanzu.vmware.com/developer/tv/tanzu-tuesdays/0011/)
    - [Создание приложений на Spring с Kubenetes и Cassandra](https://tanzu.vmware.com/developer/blog/save-your-stack-build-cloud-native-apps-with-spring-kubernetes-and-cassandra/)
    - [Продуктивность Agile-команд во время удалённой работы](https://tanzu.vmware.com/content/practitioners/keeping-agile-teams-productive-when-working-remotely)
- [Масштабирование кластера Kubernetes в Tanzu](https://tanzu.vmware.com/content/blog/simply-scaling-a-tanzu-kubernetes-cluster-with-the-tkg-service-for-vsphere) (TKG для vSphere)
- Интересные статьи БЗ:
    - [отличия редакций Tanzu Kubernetes Grid (TKG и TKG+)](https://kb.vmware.com/s/article/78173)
    - [что делать, если забыли пароль админа Harbor](https://kb.vmware.com/s/article/76289)
    - [невозможно проапгрейдить PKS (TKGI) 1.7](https://kb.vmware.com/s/article/79002)
    - [удаление кластера в PKS (TKGI) не завершается](https://kb.vmware.com/s/article/78981)
    - [ошибка установки учётных данных при работе с AWS](https://kb.vmware.com/s/article/79089)
- Обучающие материалы:
    - расширение курсов VMware на [Kube Academy](https://kube.academy/courses)
    - Tanzu Service Mesh - [что это такое (с демонстрацией)](https://youtu.be/EquVhIkS1oc)
    - [Kubernetes день второй](https://tanzu.vmware.com/content/webinars/jun-25-kubernetes-for-day-2-fedops)
    - [делаем разработчику на  kubernetes хорошо](https://tanzu.vmware.com/content/webinars/jun-30-making-the-kubernetes-developer-experience-better)
    - Tanzu Observability - [как понять свое приложение Spring Boot](https://tanzu.vmware.com/content/webinars/jun-24-tanzu-observability-tips-for-understanding-your-spring-boot-applications)

# Дополнительно
- [КРИ-П](https://www.youtube.com/channel/UClg6v5K2QlMOdBf2ffGEJKw) - Критические Работы по Инфраструктуре - в Пятницу
