Title: Эпизод тридцатый
Date: 2019-12-27 10:00
Tags: vra, nsx, vov, devops, horizon, uag, dem
Summary: Пока вокруг праздничная суматоха - предновогодний выпуск ждёт вас прямо в облаке!

# Новости облаков и автоматизации
- Вышел [vRealize Automation 8.0.1](https://docs.vmware.com/en/vRealize-Automation/8.0.1/rn/vRealize-Automation-801-releasenotes.html) и [NSX-T 2.5.1](https://docs.vmware.com/en/VMware-NSX-T-Data-Center/2.5.1/rn/VMware-NSX-T-Data-Center-251-Release-Notes.html) - минорные, но важные релизы.
- Бэкап и восстановление vRealize Suite 2019 в целом и vRA в частности. [Как это сделать правильно](https://docs.vmware.com/en/vRealize-Suite/2019/vrealize-suite-2019-backup-and-restore-netbackup%20.pdf).
- [vSphere CLI](https://code.vmware.com/web/tool/6.7/vsphere-cli) EOL (End of Life)
- [VMware Security Advisory 2019-0023](https://www.vmware.com/security/advisories/VMSA-2019-0023.html) - необходимо обновиться!
- Биты и байты: vRealize Automation 8 - [изучаем внутреннее устройство и взаимодействие между сервисами](https://gitlab.com/vra8/bp/-/wikis/Взаимодействие-между-сервисами)
- VMware on VMware - [безопасность в компании](https://blogs.vmware.com/vov/2019/12/10/intrinsic-security-how-vmware-it-cost-effectively-reimagined-what-it-means-to-be-safe-and-is-making-everything-even-more-secure/) и [переход к DevOps](https://blogs.vmware.com/vov/2019/12/10/look-under-the-hood-of-vmwares-ongoing-devops-transformation/).

# Новости EUC
- Вышел Horizon 7.11 - [заметки к релизу](https://techzone.vmware.com/blog/whats-new-vmware-horizon-711-cart-53-and-dynamic-environment-management-910)
- Unified Access Gateway 3.8 - [видеоролик](https://youtu.be/5lbtCbwlfXA)
- AirLift 2.1 - [новые дэшборды](https://youtu.be/7nOGEQUPIDs)
- Dynamic Environment Manager (DEM) - [поддержка Windows 10](https://kb.vmware.com/s/article/57386)
- [Управление Win10 с DEM](https://techzone.vmware.com/blog/managing-windows-10-vmware-dynamic-environment-manager)
- [Управление File Type Associations с помощью DEM](https://www.ivandemes.com/managing-file-type-associations-fta-natively-using-dynamic-environment-manager/)
