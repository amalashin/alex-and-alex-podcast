Title: Эпизод семнадцатый
Date: 2019-09-23 12:00
Tags: android, afe, horizon, uag, airwatch, wone, oracle, vrni, nsx, vra
Summary: Лёха&Лёха в облаке, подключайтесь! Несмотря на то, что выпуск в основном про EUC, поговорили о разном - слушайте, загружайте, комментируйте!

# Новости EUC
- Вышел новый Android 10 - [особенности в enterprise]( https://www.brianmadden.com/opinion/Android-10-released-how-is-Android-Enterprise) и [миграция AfE](https://bayton.org/docs/enterprise-mobility/android/considerations-when-migrating-from-device-administrator-to-android-enterprise)
- Обновление платформы: [Horizon 7.10](https://docs.vmware.com/en/VMware-Horizon-7/7.10/rn/horizon-710-view-release-notes.html), [UAG 3.7](https://docs.vmware.com/en/Unified-Access-Gateway/3.7/rn/Unified-Access-Gateway-37-Release-Notes.html), [ThinApp 5.2.6](https://docs.vmware.com/en/VMware-ThinApp/5.2.6/rn/VMware-ThinApp-526-Release-Notes.html) и подробный [обзор платформы Enterprise](https://techzone.vmware.com/resource/managing-user-experience-vmware-horizon-7-enterprise-edition#sec5-sub3)
- Биты и байты: Windows 10 Conditional Access, [заведение Windows 10 в AirWatch и подключение к Workspace One Access](https://digital-work.space/display/AIRWATCH/Windows+Conditional+Access+-+WinSSO); [проверка успешности подключения и корректности работы с порталом](https://digital-work.space/display/AIRWATCH/Packet+Sniffing+during+Windows+10+Enrollment)

# Новости облаков и автоматизации
- [Oracle Cloud](https://www.vmware.com/company/news/releases/vmw-newsfeed.Oracle-and-VMware-Partner-to-Support-Customers-Hybrid-Cloud-Strategies.1916340.html) на VMware и поддержка продуктов Oracle. [Анонс](https://www.oracle.com/cloud/VMware/)
- Новая версия [vRealize Network Insight 5.0](https://docs.vmware.com/en/VMware-vRealize-Network-Insight/5.0/rn/vrealize-network-insight-50-release-notes.html)
- [Новая версия NSX-T 2.50](https://docs.vmware.com/en/VMware-NSX-T-Data-Center/2.5/rn/VMware-NSX-T-Data-Center-250-Release-Notes.html)
- Прекращение поддежки продуктов: [vRealize Automation Desktop](https://kb.vmware.com/s/article/74713), [ESXi и vCenter 5.0/5.1](https://kb.vmware.com/s/article/2145103), [vSphere 6.0](https://kb.vmware.com/s/article/66977)
