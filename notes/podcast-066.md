Title: Эпизод шестьдесят шестой
Date: 2020-11-24 12:00
Tags: photonos, realtime, blockchain, nsx, avi, sd-wan, wone, uem, windows, android
Summary: За эти дни мы приготовили с Лёхой целый снежный ком новостей для Вас! Новые курсы обучения, релизы и технические статьи, собираем новинки недели. Подключаемся к КРИПкасту, смотрим и комментим в real time! Либо слушаем в режиме подкаста.

# Новости облаков и автоматизации
- Бета [Photon OS 4.0](https://blogs.vmware.com/vsphere/2020/11/photon-os-4-0-beta-release-is-now-available.html)
- Релиз [VMware Blockchain]https://octo.vmware.com/vmware-blockchain-launch/?cid=70134000001SkJT)
- Тренинг [NSX Advanced Load Balancer Architecture](https://info.avinetworks.com/workshop/nov-18-2020)
- [VMware SD-WAN and Microsoft: Coming together](https://www.brighttalk.com/webcast/13111/448289/vmware-sd-wan-and-microsoft-coming-together-with-azure-o365-and-more)

# Новости EUC
- Выход VMware [Workspace One UEM 20.10](https://docs.vmware.com/en/VMware-Workspace-ONE-UEM/2010/rn/VMware-Workspace-ONE-UEM-Release-Notes-2010.html) 
    - Freestyle Orchestrator (Бета)
    - Публикация приложений в тест-зону Google (Android)
    - Время последней перезагрузки (Android)
    - Запретить удаление ПО (iOS 14)
    - HTTP-Proxy для отсылки APNs данных (iOS)
    - Работа с Shared iPads for Business (iOS)
    - Запретить рандомизацию MAC-адреса (iOS 14, macOS Big Sur)
    - Конец поддержки WinPhone
    - Скрипты и сенсоры для Win и macOS (Бета)
- Защита Windows 10 от двух новых эксплоитов [с помощью Workspace One](https://techzone.vmware.com/blog/prevent-bad-neighbor-vulnerability-affects-windows-10-systems-using-workspace-one)
- Умножаем Windows на /dev/null - все [способы обнуления системы](https://brookspeppin.com/2020/11/10/reset-wipe/)
- Внутренние приложения на Android COPE - [конфликт интересов в системе](https://arsenb.wordpress.com/2020/10/05/android-11-cope-internal-apps-and-where-it-all-ends/)

# Дополнительно
- [КРИ-П](https://www.youtube.com/c/КРИПы) - Критические Работы по Инфраструктуре - в Пятницу (теперь и видеоверсия нас).
