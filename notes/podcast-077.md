Title: Эпизод семьдесят седьмой
Date: 2021-04-22 12:00
Tags: verify, vmware, wone, sccm, workspace, dell, nvidia, cloud, mesh7, vra, vrealize
Summary: Весенним тёплым днём мы с Лёхой собрались на цифровой завалинке, обсудим послевкусие от обучения Zero-Trust, а также впечатления от крупнейшей конференции NVIDIA. Заваривайте чаёк/кофий и заходите послушать.

# Новости от ЛёхиР (alex8s)
- Новая версия [VMware Verify, внутри Workspace One Hub](https://docs.vmware.com/en/VMware-Workspace-ONE-Access/services/ws1_access_authentication_cloud/GUID-7FFD0938-D0ED-4948-B8B4-AAB2A85D5B70.html)
- Лучшие практики, [построение политик Workspace One Access](https://theidentityguy.ca/2021/02/25/workspace-one-access-best-practices-in-policy-management/)
- Поведенческий анализ и [очки риска при заходе в Workspace One Access](https://theidentityguy.ca/2021/03/29/workspace-one-access-login-risk-score/)
- Жизнь систем управления в одной коммуналке, [отключение Microsoft SCCM Coexistence Mode](https://jamesachambers.com/disabling-sccm-mdm-coexistence-mode-unofficial-imperfect-workaround/)

# Новости от ЛёхиМ
- VMware отделяется от Dell
- GTC 21
- VMware Cloud - [что за новая штука](https://www.vmware.com/company/news/releases/vmw-newsfeed.VMware-Cloud-Accelerates-App-Modernization-through-Modular-Multi-Cloud-Services.8269da95-5c87-41d0-8102-39142868e4bc.html)?
- [VMware Cloud Universal](https://www.vmware.com/products/cloud-universal.html)
- Mesh7 - теперь уже часть VMware
- [Продление окончания поддержки vSphere/vSAN 6.5](https://blogs.vmware.com/vsphere/2021/03/announcing-limited-extension-of-vmware-vsphere-6-5-general-support-period.html)
- Что нового в [vRealize Automation Cloud 03.21](https://cloud.vmware.com/community/2021/04/07/vrealize-automation-cloud-release-full-easter-eggs/)

# Дополнительно
- [КРИ-П](https://www.youtube.com/c/КРИПы) - Критические Работы по Инфраструктуре - в Пятницу
- Весенние вебинары: [анонсы](https://www.youtube.com/playlist?list=PLSHgmTQvHHVFg6C-d6_ncX8O0buyY0Jce)
