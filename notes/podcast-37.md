Title: Эпизод тридцать седьмой
Date: 2020-02-21 16:00
Tags: certificates, uag, horizon, blast, wone, iot, pivotal, tanzu, digital, iaac, vov, skyline
Summary: Предпраздничная подборка новостей уже ждёт вас в облаке, присоединяемся!

# Новости EUC
- [Автоматизация выпуска сертификатов LetsEncrypt для Windows систем](https://arsenb.wordpress.com/2020/02/10/simplifying-letsencrypt-on-windows-cryptle-automation/)
- [Автоматизация выпуска сертификатов LetsEncrypt для UAG](https://digitalworkspace.blog/2020/01/03/automating-lets-encrypt-cerificates-lifecycle-for-horizon-and-unified-access-gateway/)
- [Доступ к физическому ПК через Horizon 7.7 Blast](http://blog.vmpress.org/2020/02/horizon.html)
- [Безопасность долговременных пользовательских сессий](https://octo.vmware.com/long-lived-sessions-keep-applying-security-policies/)
- [Введение и обзор](https://youtu.be/LGQRUe2vKWs) портала Workspace One Access
- [Форум по Workspace One](https://communities.vmware.com/community/vmtn/workspace) переехал на общий VMware Communities портал (наконец-то!)
- [Занимательный ролик](https://youtu.be/eZDlJgJf55o) о развёртывании контейнеров на docker и интеграции их в Minecraft

# Новости облаков и автоматизации
- [VMware IoT - всё](https://www.yahoo.com/news/m/461bb388-b54d-3af4-8662-f6c10329cfab/vmware-lays-off-executives-in.html). [Да здравствует IoT!](https://blogs.vmware.com/services-education-insights/2020/02/introduction-to-a-modern-iot-application.html) (введение в Modern IoT Application)
- Платформы для современных приложений: [какое из трёх слов?](https://blogs.vmware.com/services-education-insights/2020/02/platforms-for-modernizing-applications-which-3-words.html)
- От решений Pivotal к VMware Tanzu, [что необходимо знать](https://content.pivotal.io/webinars/from-pivotal-to-vmware-tanzu-what-you-need-to-know)
- Виртуальное мероприятие VMware - Цифровой бизнес. Создание, запуск и обслуживание любых приложений в любом облаке. От облаков до железа, что нужно знать уже сегодня! [Регистрируемся](https://www.vmware.com/uk/app-modernization.html?src=em_5e4454e6653a0&cid=7012H0000021ThW)
- Инфраструктура, как код. [Важна ли?](http://virtualpadawon.net/2020/02/05/infrastructure-as-code-is-it-important/)
- Подкаст [VMware CIO Exchange](https://podcasts.apple.com/us/podcast/cio-exchange-podcast/id1498290907). Изучаем опыт лидеров отрасли.
- VMware на VMware - [ИТ-отчёт за 2019 год](https://www.vmware.com/content/dam/digitalmarketing/vmware/en/pdf/company/vmw-it-performance-annual-report-2019.pdf).
- Skyline "День второй". Поставили и что?? [Регистрация на вёбинар](https://vmwarelearningzone.vmware.com/oltpublish/site/openlearn.do?dispatch=previewLesson&id=411b5d07-4cbc-11ea-9f48-0cc47adeb5f8&playlistId=8c8c15e5-4970-11ea-9f48-0cc47adeb5f8), чтобы узнать, как использовать!
