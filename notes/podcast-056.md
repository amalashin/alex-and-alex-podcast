Title: Эпизод пятьдесят шестой
Date: 2020-08-10 12:00
Tags: spring, government, vrealize, pathfinder, cloud, vmc, aws, carbon black, skyline, nsx, uem, wone
Summary: Свежие новости томным августовским днём! ⛱ Прямо из облака! ☁️

# Новости облаков и автоматизации
- Напомним про [конференцию SpringOne](https://springone.io)
- ИТ и VMware:
    - [в гос-секторе](https://youtu.be/fmRKeyN4EL8)
    - [в здравоохранении](https://youtu.be/g0lNMivZiuA)
    - [в обучении](https://youtu.be/TNqcfxiqsyQ)
- [vRealize Automation Cloud, июльские обновления](https://cloud.vmware.com/community/2020/07/22/vrealize-07-20-launch-update/) или роадмап для версии on-prem
- Обновления (VMware Pathfinder)[https://pathfinder.vmware.com]:
    - [VMware Cloud on AWS](https://pathfinder.vmware.com/path/start_your_hybrid_cloud_journey_with_vmware_cloud_on_aws)
    - [Миграция в облако VMC on AWS](https://pathfinder.vmware.com/path/cloud_migration_with_vmware_cloud_on_aws)
    - [Carbon Black и охота на уязвимости](https://pathfinder.vmware.com/path/threathunting)
    - [VMware Skyline](https://pathfinder.vmware.com/path/take_pressure_off_it_with_vmware_skyline)
- [VMware Skyline теперь поддерживает NSX](https://docs.vmware.com/en/VMware-Skyline-Advisor/services/rn/VMware-Skyline-Advisor-Release-Notes.html)
- [VEBA и vRealize Automation](https://blogs.vmware.com/services-education-insights/2020/07/deploying-the-vmware-event-broker-appliance-veba-using-cloud-assembly.html)

# Новости EUC
- Интересные места, в которые стоит посмотреть во время [начальной настройки Workspace One UEM](https://mobile-jon.com/2020/07/29/5-ways-to-elevate-your-workspace-one-uem-deployment/)
- Как обойти грабли и [настроить VMware Tunnel](https://mobile-jon.com/2020/07/21/workspace-one-uem-and-building-a-global-strategy-for-vmware-tunnel/)
- Альтернатива [GPO от Workspace One UEM](https://www.evengooder.com/2020/06/ws1-modern-management-for-gpo-settings.html)
- Размышления о защите VDI в соответствии с требованиями регуляторов

# Дополнительно
- [Кастомизация терминала на macOS с установкой ZSH](https://brandon.azbill.dev/how-to-customize-your-zsh-terminal/)
- [КРИ-П](https://www.youtube.com/channel/UClg6v5K2QlMOdBf2ffGEJKw) - Критические Работы по Инфраструктуре - в Пятницу
- [CREEP](https://www.youtube.com/watch?v=qjdY1g0eL1M&list=PL0aZOZ-cWW_V4-5VFDBG0kBrOeSWdx0Sw) - Commit Every Experiment in Production
