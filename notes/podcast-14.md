Title: Эпизод четырнадцатый
Date: 2019-09-02 12:00
Tags: vmworld, pacific, k8s, pks, vcf, vsan, datascience, ai/ml, horizon, airwatch
Summary: В день знаний мы рассказываем про основные анонсы VMworld 2019 US!

# Новости облаков и автоматизации
- Главный анонс VMworld 2019 US - Платформа Tanzu и [подход VMware к современным приложеним](https://blogs.vmware.com/cloudnative/2019/08/26/vmware-completes-approach-to-modern-applications/). Сюда входят: [Проект Pacific](https://blogs.vmware.com/cloudnative/2019/08/26/project-pacific-is-kubernetes-to-the-core/) (Run) и [технический обзор](https://blogs.vmware.com/vsphere/2019/08/project-pacific-technical-overview.html), [проект Olympus](https://blogs.vmware.com/cloudnative/2019/08/26/vmware-tanzu-mission-control/) (Manage) и [Pivotal (Build)](https://pivotal.io)
- Как развернуть VMware Enterprise PKS на платформе VMware vCloud Foundation: [блог](https://blogs.vmware.com/cloudnative/2019/08/05/deploying-vmware-enterprise-pks-on-vmware-cloud-foundation/), [видео](https://www.youtube.com/watch?v=Q4gok753fNY), [документация](https://docs.vmware.com/en/VMware-Validated-Design/5.1/sddc-deployment-of-vmware-enterprise-pks-with-vmware-nsx-t-workload-domains/GUID-0E446865-377E-417E-B18E-C8B63CE9A2A2.html)
- Облачные новости: [гибридная платформа следующего поколения](https://www.vmware.com/company/news/releases/vmw-newsfeed.VMware-Delivers-a-Hybrid-Cloud-Platform-Powering-Next-Generation-Hybrid-IT.1906447.html), обновления продуктов vSphere 6.7u3 и vSAN 6.7u3, а так же [партнёрство с компанией NVIDIA для оптимизации нагрузок DataScience](https://www.vmware.com/company/news/releases/vmw-newsfeed.NVIDIA-and-VMware-to-Accelerate-Machine-Learning-Data-Science-and-AI-Workloads-on-VMware-Cloud-on-AWS-Accelerated-by-NVIDIA-GPUs.1906445.html). Анонс [vRealize Automation 8.0 с Kubernetes](https://blogs.vmware.com/management/2019/08/vrealize-automation-8-whats-new-overview.html) и [vRealize Operations 8.0 с AI/ML внутри!](https://blogs.vmware.com/management/2019/08/announcing-vmware-vrealize-operations-cloud-ai-ml-based-monitoring-as-a-service.html)

# Новости EUC
- [VMworld EUC Keynote](https://blogs.vmware.com/euc/2019/08/digital-workspace-vmworld-2019.html?src=so_5d64a7b2ebc11&cid=70134000001CTmC) и ссылки на [видео сессий VMworld](https://github.com/lamw/vmworld2019-session-urls/blob/master/vmworld-us-playback-urls.md)
- [Что нового в Horizon 7](https://s3-us-west-1.amazonaws.com/vmworld-usa-2019/ADV1337BU.mp4) и [демо управления macOS](https://s3-us-west-1.amazonaws.com/vmworld-usa-2019/UEM2099BU.mp4)

# Дополнительно
Мириан рассказывает про самые крупные инсталляции AirWatch и новая загадка
