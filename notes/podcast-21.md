Title: Эпизод двадцать первый
Date: 2019-10-22 10:00
Tags: wone, uem, horizon, vra, pks, k8s, pks
Summary: Новый эпизод! Прямо из облака!

# Новости EUC
- Импортозамещение Microsoft Active Directory: cинхронизация OpenLDAP и Active Directory учётных записей с WOne Access - возможно ли?
- [Работа каскада vIDM/Access с двумя доменами Active Directory](https://blogs.vmware.com/horizontech/2017/05/using-vmware-identity-manager-transform-users-active-directory-domains.html)
- Публикация ПО сразу в нужный портал Access по уникальному URL
- Уникальный URL запуска на базе UUID: `https://<hostname>/SAAS/API/1.0/GET/apps/launch/app/<resourceUUID>`
- [Подготовка образа с Windows 10 под Horizon 7](https://techzone.vmware.com/creating-optimized-windows-image-vmware-horizon-virtual-desktop#1150974)

# Новости облаков и автоматизации
- [vRealize 8 - GA!](https://blogs.vmware.com/management/2019/10/vrealize-suite-2019-vcloud-suite-2019-ga.html)
- [Книжица](https://www.vmware.com/content/dam/digitalmarketing/vmware/en/pdf/products/vrealize/vmware-cloud-management-with-actionable-insights-ebook.pdf) и [Брошюра](https://www.vmware.com/content/dam/digitalmarketing/vmware/en/pdf/products/vrealize/vmware-self-driving-operations-powered-by-ai-ml-brochure.pdf) о Self-Driving Operations
- [Лабораторная работа PKS для Ninja](https://labs.hol.vmware.com/HOL/catalogs/lab/6633/)
- [Bacula Backup для Kubernetes](https://www.baculasystems.com/kubernetes-backup-restore/)
- Amazon [анонсировал](https://aws.amazon.com/blogs/aws/amazon-eks-windows-container-support-now-generally-available/) доступность контейнеров на Windows в EKS
- [Rancher на Enterprise PKS](https://www.sovsystems.com/blog/rancher-ha-on-enterprise-pks) - зачем???
- [Kubernetes 1.16](https://blogs.vmware.com/cloudnative/2019/09/18/kubernetes-1-16-crds-storage-networking/) и комплиментарный релиз [Essentials PKS 1.16](https://my.vmware.com/web/vmware/info/slug/infrastructure_operations_management/vmware_essential_pks/1_16)
- [Самосборный Kubernetes vs VMware PKS](https://springoneplatform.io/2019/sessions/the-reality-of-diy-kubernetes-vs-pks) (видео и презентация)
