Title: Эпизод второй
Subtitle: Осваиваемся и привыкаем к звучанию своего голоса
Date: 2019-06-06 12:00
Tags: pks, mangle, pulse, iot, wone, uem, macos
Summary: Во втором выпуске мы поменяли микрофон (оказалось хуже) и учли некоторые пожелания.

# Новости облаков и автоматизации
- [PKS Ninja Community](https://github.com/CNA-Tech/PKS-Ninja)
- [Проект Mangle](https://vmware.github.io/mangle) и Chaos Engineering
- Интернет вещей VMware (Pulse IoT Center) и [Feature Walkthrough](https://featurewalkthrough.vmware.com/t/vmware-pulse-iot-center)

# Новости EUC
- [WhatsApp Malware и безопасность устройств](https://blogs.vmware.com/euc/2019/05/whatsapp-malware.html)
- Honeywell стал [реселлером VMware Workspace One UEM](https://blogs.vmware.com/euc/2019/05/workspaceone-honeywell.html)
- [Интеграция macOS с WOne и Active Directory](https://digital-work.space/display/AIRWATCH/macOS+integration+with+AD+using+NOMAD)

# Дополнительно
Загадка
