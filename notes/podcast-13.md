Title: Эпизод тринадцатый
Date: 2019-08-26 12:00
Tags: wone, uem, airwatch, certificates, pivotal, carbonblack, pks, testdrive
Summary: Лето скоро заканчивается, но мы не грустим - ведь новый эпизод подкаста опять в облаке!

# Новости EUC
- Выход VMware Workspace One UEM 1908: [release notes](https://docs.vmware.com/en/VMware-Workspace-ONE-UEM/1908/rn/VMware-Workspace-ONE-UEM-Release-Notes-1908.html) и [обзорное видео](https://www.youtube.com/watch?v=zmFgocI27zM)
- Биты и байты - [Интгерация AirWatch с сервером сертификатов](https://digital-work.space/display/AIRWATCH/AirWatch+Integration+with+Microsoft+CA), [DCOM Direct vs DCOM on-behalf-of](https://digital-work.space/display/AIRWATCH/AirWatch+integration+ADCS+via+DCOM), [SCEP Revocation в MobileSSO via OCSP Responder](https://digital-work.space/display/IDM/MobileSSO+for+iOS)

# Новости облаков и автоматизации
- VMware приобретает ряд компаний: [Pivotal](https://content.pivotal.io/blog/pivotal-vmware-transforming-how-more-of-the-world-builds-software), [Carbon Black](https://www.carbonblack.com/2019/08/22/the-next-chapter-in-our-story-vmware-carbon-black/), [Intristic](https://www.cnbc.com/2019/08/20/vmware-acquires-intrinsic-serverless-security-start-up.html), [Veriflow](https://blogs.vmware.com/management/2019/08/vmware-to-advance-network-monitoring-with-acquisition-of-veriflow)
- [VMware TestDrive](https://vmtestdrive.com) теперь позволяет протестировать [Enterprise PKS](https://kb.vmtestdrive.com/hc/en-us/articles/360006297834-VMware-Enterprise-PKS-Quick-Start) в живой среде

# Дополнительно
- Ответ на загадку и новый вопрос
- Поздравляем Мириана с днём рождения!
