Title: Эпизод тридцать третий
Date: 2020-01-22 00:00
Tags: appvolumes, wone, chef, macos, hub, devops, interview
Summary: А у нас опять гость! Разговариваем про современные приложения и Kubernetes.

# Новости EUC
- Как Алексей провёл новогодние праздники? Снимал видосы и адищно монтировал!
- [Настройка MS Project с VMware App Volumes 2.x](https://techzone.vmware.com/resource/best-practices-delivering-ms-project-vmware-app-volumes-2x-operational-tutorial)
- Порталы [Workspace ONE Feature Request Portals](https://techzone.vmware.com/blog/announcing-new-workspace-one-feature-request-portals)
- Быстрые [видео по Workspace One](https://videos.vmtestdrive.com/) (до середины 2018г)
- [Управление macOS через Workspace One UEM по команде от Chef](https://blog.eriknicolasgomez.com/2019/10/31/CMDM-Part-2-managing-workspace-one-profiles-with-chef-using-the-new-v1910-hubcli-agent/)
- [hubcli wrapper](https://github.com/uber/cpe-chef-cookbooks/tree/master/cpe_workspaceone)

# Новости облаков и автоматизации
- Интервью с Bjorn Brundert - ведущим экспертом по современным приложениям и DevOps в VMware EMEA.
