Title: Эпизод восьмой
Date: 2019-07-19 12:00
Tags: pmem, pivotal, k8s, vmworld, horizon, uem, appvolumes, airwatch, wone
Summary: На улице дождик, а мы записываем новый эпизод.

# Новости облаков и автоматизации
- Intel Optane и его [поддержка в vSphere](https://storagehub.vmware.com/t/vsphere-storage/vsphere-6-7-core-storage-1/pmem-persistant-memory-nvdimm-support-in-vsphere/), а так же [про PMEM](https://vspherecentral.vmware.com/t/hardware-acceleration/persistent-memory-pmem/)
- [Pivotal Application Services теперь поддерживает Kubernetes](https://blogs.vmware.com/cloudnative/2019/07/16/pas-on-kubernetes-improves-the-developer-experience/)
- [VMworld 2019 EU](https://www.vmworld.com/en/europe/index.html), [скорее регистрирутесь](https://reg.rainfocus.com/flow/vmware/vmworld19eu/reg), там будет [много интересного](https://www.vmworld.com/en/europe/agenda.html)

# Новости EUC
- [Обновление клиентской части Horizon](https://www.vmgu.ru/news/vmware-horizon-79-clients-51) и переход Android на x64
- [Обновление UEM и AppVolumes](https://techzone.vmware.com/blog/whats-new-vmware-user-environment-manager-98-and-app-volumes-217), [видео по новым функциям](https://youtu.be/kJZcF-uE8F0) и [справочник](https://code.vmware.com/apis/561/app-volumes-app-volumes-api#/)
- [Deep Dive по AirWatch / Workspace One UEM 1904](https://youtu.be/NWK1yXDlSBA)
