Title: Эпизод восемнадцатый
Date: 2019-09-30 12:00
Tags: k8s, vra, terraform, devops, tanzu, aws, wone, sso, ios, macos, horizon, uem, android
Summary: Пока Лёха спешит на самолёт, а офис VMware переезжает, новый эпизод уже готов!

# Новости облаков и автоматизации
- [Управление namespace'ами Kubernetes из vRealize Automation Cloud](https://blogs.vmware.com/management/2019/09/kubernetes-namespace-management-in-cloud-assembly.html)
- [Редактор свойств объектов блюпринта](https://blogs.vmware.com/management/2019/09/blueprint-object-properties-editor-in-vrealize-automation-cloud.html) (Blueprint Objects Properties Editor)
- [Провайдер terraform для Cloud Assembly](https://github.com/vmware/terraform-provider-vra) (ранний доступ)
- Автоматизация сети, группы безопасности в блюпринтах: [часть 1](https://blogs.vmware.com/management/2019/04/network-automation-cloud-assembly-and-nsx-part-1.html), [часть 2](https://blogs.vmware.com/management/2019/05/network-automation-with-cloud-assembly-and-nsx-part-2.html), [часть 3](https://blogs.vmware.com/management/2019/08/network-automation-with-cloud-assembly-and-nsx-part-3.html), [часть 4](https://blogs.vmware.com/management/2019/08/network-automation-with-cloud-assembly-and-nsx-part-4.html)
- Автоматизация сети, [поддержка IPv6](https://blogs.vmware.com/management/2019/09/cloud-assembly-support-for-ipv6.html)
- [Политики сервисов при deployment'е](http://blogs.vmware.com/management/2019/09/service-broker-policy-criteria.html)
- [Документация API для vRA Cloud](https://www.mgmt.cloud.vmware.com/automation-ui/api-docs/)
- [Вебинар по DevOps](https://onlinexperiences.com/scripts/Server.nxp?LASCmd=AI:4;F:QS!10100&ShowUUID=19BF584A-FC23-4D57-A70E-781738FE957C)
- Kubernetes 1.16 и [интеграция PKS с Tanzu Mission Control](https://www.pivotaltracker.com/story/show/168647036)
- [Баг в Harbor 1.7.0-1.7.5](https://www.vmware.com/security/advisories/VMSA-2019-0015.html) (повышение привелегий)
- Программа [AWS QuickStart](http://prosereng.us.newsweaver.com/l65hmpygtp/b9emue7ya9h)

# Новости EUC
- Выход обновления [Workspace One UEM 1909 (пока в облаке)](https://docs.vmware.com/en/VMware-Workspace-ONE-UEM/1909/rn/VMware-Workspace-ONE-UEM-Release-Notes-1909.html)
- [SSO Extension Profile](https://docs.vmware.com/en/VMware-Workspace-ONE-UEM/1909/iOS_Platform/GUID-3F402050-ED31-4914-9E94-D99CF215332C.html) для iOS 13 и macOS 13
- Новые функции в мобильных приложениях Workspace One, [сводный обзор самых интересных](https://blogs.vmware.com/euc/2019/09/secure-productivity-apps.html)Новый [интерфейс Horizon Console](https://techzone.vmware.com/blog/vmware-horizon-console-overview)
- [Работа со специализированнами Android Rugged устройствами](https://techzone.vmware.com/blog/deployment-and-troubleshooting-tips-android-enterprise-rugged-platform-using-vmware-workspace) с использованием VMware Workspace ONE UEM
- Подробный [разбор функционала Android 10](https://arstechnica.com/gadgets/2019/09/android-10-the-ars-technica-review/)
- [Варианты инициации устройств Apple](https://techzone.vmware.com/blog/hitchhikers-guide-apples-user-enrollment)

# Обсуждения
- [iOS безопасность через скрытность](https://arsenb.wordpress.com/2019/09/02/on-apple-security-by-obscurity-and-ws1-trust-network/)
- [Иск Apple](https://www.vice.com/en_us/article/d3a8jq/apple-corellium-lawsuit) за попытку разобраться в безопасности iOS
- Подробный [разбор уязвимостей iOS](https://googleprojectzero.blogspot.com/2019/08/a-very-deep-dive-into-ios-exploit.html) от команды аналитиков Google
- А новой загадки в этот раз не будет, а почему - узнайте в подкасте
