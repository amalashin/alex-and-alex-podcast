Title: Эпизод восьмидесятый
Date: 2021-06-11 12:00
Tags: vmware, pathfinder, cloud, vrealize, controlup, horizon, devops, appvolumes
Summary: У нас 80-ый Крипкаст, который мы с Лёхой посвятили очень традиционным темам: облакам и VDI. В облачных технологиях новые анонсы, а в теме VDI балом правит автоматизация. Вооружаемся шоколадом и разбираемся - как создать из "золотого" образа Windows конфетку!

# Новости от ЛёхиМ 
- Апгрейд ЦОД VMware - простой в 6 часов 12 июня с 7 утра по Московскому времени
- [Pathfinder: как перейти на NSX-T с NSX-v](https://pathfinder.vmware.com/activity/nsx_assessment)
- [Панель статуса облачных сервисов VMware](https://status.vmware-services.io)
- [Карта глобальных сервисов VMware](https://www.vmware.com/content/dam/digitalmarketing/vmware/en/pdf/products/vmware-cloud-mgmt-saas-services-map.pdf) - что можно получить из облака
- [Облачная система управления vRealize Cloud Management оттестирована и сертифицирована для работы с Azure VMware Solution](https://blogs.vmware.com/management/2021/05/vrealize-cloud-management-tested-and-certified-with-azure-vmware-solution.html)

# Новости от ЛёхиР (alex8s) 
- Новый [инструмент для отслеживания физических рабочих мест ControlUp Edge-DX](https://www.controlup.com/products/edge-dx/)
- Инструмент для [нагрузочного тестирования VDI - ControlUp Scoutbees](https://www.controlup.com/products/scoutbees/)
- Автоматизация в VMware Horizon:
    - Автоматизация создания "золотого" образа [с помощью Packer и Chocolatey](https://roderikdeblock.com/automate-the-building-of-your-vmware-golden-image-with-packer-and-chocolatey/)
    - ПОЛНАЯ автоматизация "золотого" образа [с помощью Azure DevOps, Packer и Chocolatey](https://roderikdeblock.com/fully-automated-image-build-and-deployment-to-vmware-horizon-using-azure-devops-packer-and-chocolatey/)
    - Автоматизация во время [захвата приложения с AppVolumes](https://roderikdeblock.com/automate-the-complete-capturing-process-using-app-volumes-tools/)

# Дополнительно
- [КРИ-П](https://www.youtube.com/c/КРИПы) - Критические Работы по Инфраструктуре - в Пятницу (теперь и видеоверсия нас).
- Весенние вебинары: [анонсы](https://www.youtube.com/playlist?list=PLSHgmTQvHHVFg6C-d6_ncX8O0buyY0Jce)
