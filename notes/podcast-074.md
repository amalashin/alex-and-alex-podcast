Title: Эпизод семьдесят четвёртый
Date: 2021-03-03 12:00
Tags: hcx, cloud director, vmware, vrealize, hol, customer connect, support, appvolumes, boxer, secure email
Summary: Конец февраля выдался относительно тихим,однако и тут есть ряд интересных анонсов на обсуждение. Разбираемся, какие новости намела нам на порог февральская метель.

# Новости от ЛёхиМ 
- [Выход HCX 4.0](https://blogs.vmware.com/networkvirtualization/2021/02/introducing-hcx4-0.html/), для тех, кто мигрирует! 
- Лабораторная работа [VMware Cloud Director на платформе Dell Technologies](https://labs.hol.vmware.com/HOL/catalogs/lab/9387)
- Новая [лабораторная по управлению с vRealize Cloud](https://labs.hol.vmware.com/HOL/catalogs/lab/9389)
- [VMware Customer Connect](https://blogs.vmware.com/services-education-insights/2021/02/announcing-vmware-customer-connect.html)
- [Реструктуризация поддержки VMware](https://www.vmware.com/content/dam/digitalmarketing/vmware/en/pdf/customer-success/vmware-success-360-datasheet.pdf) и [брошюра](https://www.vmware.com/content/dam/digitalmarketing/vmware/en/pdf/customer-success/vwmare-success-360-ebook.pdf)

# Новости от ЛёхиР (alex8s) 
- Стеки приложений AppVolumes, их [миграция с версии 2.18 на 4.х](https://flings.vmware.com/app-volumes-migration-utility#summary)
- Работа с [корпоративными документами в Boxer](https://mobile-jon.com/2021/02/23/workspace-one-boxer-unifies-mobile-content-management/)
- Аутентификация пользователей почты через шлюз [Secure EMail Gateway с помощью Kerberos в отказоустойчивом режиме](https://mobile-jon.com/2021/02/19/adding-resiliency-to-the-workspace-one-secure-email-gateway-with-kerberos-constrained-delegation/)


# Дополнительно
- [КРИ-П](https://www.youtube.com/c/КРИПы) - Критические Работы по Инфраструктуре - в Пятницу (теперь и видеоверсия нас).
