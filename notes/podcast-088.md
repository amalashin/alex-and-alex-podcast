Title: Эпизод восемьдесят восьмой
Date: 2021-12-07 12:00
Tags: cloudchomp, hci, uem, leader, vmware, kubernetes, aws, horizon, wone, scim, azure
Summary: На дворе валит густой снег, а мы с Лёхой собрались обсудить новости, которых навалило тоже предостаточно. Заваривайте чаю или кофе и к нам!

# Новости от ЛёхиМ 
- Давно не закупались - VMware хочет приобрести [CloudChomp](https://www.cloudchomp.com)
- Подъехала аналитика:
    - HCI - [VMware снова лидер 2021](https://blogs.vmware.com/virtualblocks/2021/11/24/vmware-fifth-time-leader-gartner-magic-quadrant-hyperconverged-infrastructure-software/)
    - UEM - [И тут VMware побеждает в Q4](https://blogs.vmware.com/euc/2021/11/vmware-named-a-leader-in-the-forrester-wave-unified-endpoint-management-q4-2021.html)
- [Как использовать платформу Kubernetes после установки](https://tanzu.vmware.com/content/webinars/oct-28-so-you-built-a-kubernetes-platform-now-what-achieving-platform-economics-with-kubernetes)
- Новости облаков: [VMware на re:Invent 2021](https://blogs.vmware.com/cloud/2021/12/01/vmware-cloud-on-aws-going-big-reinvent2021/)

# Новости от ЛёхиР (alex8s) 
- Вышел [Horizon 8.4 2111 ESB](https://docs.vmware.com/en/VMware-Horizon/8%202111/rn/vmware-horizon-8-2111-release-notes/index.html)
    - Microsoft Internet Explorer больше не поддерживается в Horizon Console
    - Пользователи без аутентификации
    - URL Content Redirection в IE/Firefox/Chrome
    - RTAV для WAN
    - Multi-NICs для Instant Clone машин
    - Мульти-образ и выборочные обновления для Instant Clone машин
    - Horizon Recording Server для RDSH
    - install_viewagent.sh --force установка Horizon Agent на неподдерживаемых ОС
    - View Agent Direct-Connection Plug-In для виртуальных машин с Linux OS
- Как [развернуть Horizon 8.4 - статья от Карла](https://www.carlstalhood.com/vmware-horizon/vmware-horizon-8/)
- Изменения в [API в Horizon 8.4 2111 для создания пулов и ферм RDS](https://www.retouw.nl/2021/11/30/horizon-8-2111-ga-whats-new-in-the-rest-apis/)
- Проверка [работоспособности Workspace ONE UEM](https://mobile-jon.com/2021/11/29/mobile-jons-guide-to-workspace-one-health-checks/)
- Протокол [SCIM, его реализация в Azure AD](https://docs.microsoft.com/en-us/azure/active-directory/app-provisioning/use-scim-to-provision-users-and-groups)
- Передача пользователей [из Azure AD в Workspace ONE Access с помощью SCIM Proxy](https://github.com/tbwfdu/rollcall)

# Дополнительно
- [КРИ-П](https://www.youtube.com/c/КРИПы) - Критические Работы по Инфраструктуре - в Пятницу (теперь и видеоверсия нас).
