Title: Эпизод двадцать третий
Date: 2019-11-06 10:00
Tags: wone, uem, android, ios, macos, vra, vrops, ai/ml, bitfusion, vmworld
Summary: В преддверии европейского VMworld 2019 обсуждаем горячие новости, а также взгляд на информационную безопасность с точки зрения преподавателя. Для последнего у нас в эфире специальный гость, отлично знающий тему изнутри!

# Новости EUC
- Выход [VMware Workspace ONE UEM 1910](https://docs.vmware.com/en/VMware-Workspace-ONE-UEM/1910/rn/VMware-Workspace-ONE-UEM-Release-Notes-1910.html) (облачный)
- (Android) [Remove a Work Profile](https://docs.vmware.com/en/VMware-Workspace-ONE-UEM/1910/Android_Platform/GUID-AWT-DEVICESUSERS-ANDROID-AFW.html)
- (iOS) [Доп атрибуты](https://docs.vmware.com/en/VMware-Workspace-ONE-UEM/1910/Application_Management/GUID-AWT-FLEX-DPLYMNT-ADD.html) для указания пользовательского домена для приложений
- (Windows) [Выбор разрядности сенсоров](https://docs.vmware.com/en/VMware-Workspace-ONE-UEM/1910/Windows_Desktop_Device_Management/GUID-568383AA-1D46-4109-9DC8-E3BDA2A38EE1.html) x32/x64
- (Windows) [Smart Groups](https://docs.vmware.com/en/VMware-Workspace-ONE-UEM/1910/UEM_ConsoleBasics/GUID-AWT-CREATESMARTGROUP.html) на базе OEM производителя
- (macOS) [Политики per-app VPN](https://docs.vmware.com/en/VMware-Workspace-ONE-UEM/1910/Tunnel_Linux/GUID-AWT-CREATE-NETTRAFFICRULES.html)
- (macOS) [Статья по извлечению BundleID](https://docs.vmware.com/en/VMware-Workspace-ONE-UEM/1910/Tunnel_Linux/GUID-AWT-EXTRACTMACOSBUNDLEID.html#GUID-AWT-EXTRACTMACOSBUNDLEID) для per-app VPN
- [Видеообзор Workspace ONE Assist 5.3](https://www.youtube.com/watch?v=9BG_njq_Nhc)

# Новости облаков и автоматизации
- vRealize Automation - [октябрьская редакция](https://cloud.vmware.com/vrealize-automation-cloud/features)
- [Вебинар Cloud Automation как сервис](https://onlinexperiences.com/scripts/Server.nxp?LASCmd=AI:4;F:QS!10100&ShowUUID=290BF56E-4F08-4890-81A8-5957C2D647E2)
- [Кампания по апгрейду vRealize Operations со скидкой](https://www.vmware.com/promotions/2018-vrealize-operations-upgrade.html) расширена до января 2020г
- [Срочная необходимость обновить сертификат vRealize Operations](https://kb.vmware.com/s/article/71018) (v6.x и далее) - до 19 ноября 2019 года!
- Снова про [Bitfusion](https://kb.vmware.com/s/article/71018) (виртуализация доступа к устройствам ускорения для AI/ML): [презентация технологии](https://videos.vmworld.com/global/2019/videoplayer/27609) на VMworld 2019 US ([pdf-файл](https://cms.vmworldonline.com/event_data/12/session_notes/BCA2626BU.pdf)), [видео-знакомство с продуктом](https://youtu.be/4fc0OwKWCoI), [обсуждение с продуктовыми менеджерами компании](https://www.youtube.com/watch?v=hqxJZU_RgzI)

# Дополнительно
- [Метод строгих математических доказательств в сфере ИБ](https://habr.com/ru/company/muk/blog/272789/) (по модели Белла — Лападулы)
- Обсуждение загадки (**есть победитель!**)
- Интервью с Кузьмой Пашковым - ведущим преподавателем направлений «Информационная безопасность», «Виртуализация компании VMware» и «Системы резервного копирования и архивирования корпорации ЕМС»!
