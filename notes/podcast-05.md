Title: Эпизод пятый
Date: 2019-06-26 12:00
Tags: airwatch, win10, vrops, k8s
Summary: Лёха вернулся из отпуска, Мириан придумал сумашедшее задание, а в студии у нас снова гости. Приятного прослушивания!

# Новости EUC
- WWDC, новые ОС от Apple и [AirWatch User Enrollment](https://digital-work.space/display/AIRWATCH/AirWatch+Matrix)
- [Режим киоска в Windows 10](https://digital-work.space/display/AIRWATCH/Windows+AppLocker+Management)

# Новости облаков и автоматизации
- Вспомним [vRealize Operations](https://www.vmware.com/learn/50702_REG.html)! Обновлённый отчёт [Forrester про экономическое влияние vROps на жизнь ИТ предприятия](https://www.vmware.com/content/dam/learn/en/amer/fy20/pdf/50702_20Q1_The-Forrester-Total-Economic-Impact-VMware-vRealize-Intelligent-Operations_April2019.pdf)
- [Kubernetes на Raspberry Pi](https://blogs.vmware.com/code/2019/06/14/kubernetes-on-a-raspberry-pi-cluster/) и [бесплатные книги O'Reilly про Kubernetes](https://k8s.vmware.com/kubernetes-for-executives/) от VMware

# Дополнительно
- Про курочек
- Рубрика Мириана
- Слово гостям: **Андрей** и **Анися** про работу в VMware
- Загадка
