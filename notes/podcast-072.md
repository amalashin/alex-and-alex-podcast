Title: Эпизод семьдесят второй
Date: 2021-02-09 12:00
Tags: esxi, security, uem, vrealize, vov, dr, conferences, android, boxer, horizon, controlup
Summary: Взлом ESXi и активация записи экрана в момент просадки производительности CPU - сегодня много интересных подробностей, которые нечасто встречаются в общих обзорах. Также не обошлось без новинок - всё это мы собрались с Лёхой обсудить, добро пожаловать к нам на уютную посиделку.  

# Новости от ЛёхиМ 
- Б - Безопасность. Своевременно [обновляйте](https://www.vmware.com/security/advisories.html) свои сервера
- VMware который год подряд [лидер в области UEM](https://blogs.vmware.com/euc/2021/01/vmware-leader-idc-marketscape-for-uem.html)
- Анонс vRealize 8.3
- VMware on VMware: 
  - [лучшая среда для разработчиков](https://vmugcollective.com/vmware-it-enables-app-developers-and-ops-teams-to-deliver-a-delightful-experience-to-customers-partners-and-colleagues/)
  - [DR для критичных приложений](https://blogs.vmware.com/vov/2021/01/28/discover-how-vmware-it-migrates-mission-critical-apps-to-improve-disaster-recovery/)
- [Конференции VMware в этом году](https://www.vmware.com/company/news/updates/2021/vmware-conference-dates.html)

# Новости от ЛёхиР (alex8s) 
- [Изменения в управлении Android11](https://bayton.org/docs/enterprise-mobility/android/android-11-cope-changes/)
- Инициация: [Android устройства без Google Services](https://arsenb.wordpress.com/2021/01/26/enroll-a-fully-managed-non-gms-android-device-using-adb/)
- Поддержка [планшетов Wacom в Horizon](https://blogs.vmware.com/euc/2021/02/smooth-signature-support-in-vmware-horizon.html?utm_source=feedly&utm_medium=rss&utm_campaign=smooth-signature-support-in-vmware-horizon)
- Разбираемся в новой фиче - [делегирование почтовых ящиков в Boxer](https://mobile-jon.com/2021/02/02/introducing-multiple-managed-accounts-and-delegation-with-workspace-one-boxer/)
- Управление [записью экрана в Horizon с помощью ControlUp](https://www.retouw.nl/2020/11/11/controlup-loves-horizon-session-recording/)

# Дополнительно
- [КРИ-П](https://www.youtube.com/c/КРИПы) - Критические Работы по Инфраструктуре - в Пятницу (теперь и видеоверсия нас).
