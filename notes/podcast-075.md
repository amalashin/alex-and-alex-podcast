Title: Эпизод семьдесят пятый
Date: 2021-03-18 12:00
Tags: cloud, multi dc, horizon, mirage, flex, ovh, redhat, vsphere 7, bitfusion, sap hana, vsan
Summary: В 75ом эпизоде Лёха выходит из облака. По пути мы обсуждаем свежие новости, а также вкусовые качества пива, сваренного на VSAN. Присоединяйтесь к нам на дегустацию!

# Новости от ЛёхиР (alex8s)
- Стратегия [выхода из облака](https://octo.vmware.com/its-time-to-develop-a-cloud-exit-strategy/)
- Растянутая на несколько ЦОД, [мультисайт архитектура Horizon 7/8](https://infohub.delltechnologies.com/p/vdi-data-protection-part-2-a-deep-dive-into-vmware-horizon-7-multi-site-scenarios/)
- Вторичные пользователи в [Android 9+ для посменной работы](https://blogs.vmware.com/euc/2021/03/android-enterprise-native-check-in-check-out-now-supported-in-workspace-one-uem-android-video-series-episode-17.html?utm_source=feedly&utm_medium=rss&utm_campaign=android-enterprise-native-check-in-check-out-now-supported-in-workspace-one-uem-android-video-series-episode-17)
- Звонок из прошлого, Horizon Mirage и FLEX: [управление корпоративным рабочим местом в BYOD сценарии](https://brookspeppin.com/2021/02/19/ws1-managedvm/)

# Новости от ЛёхиМ
- Сгорел ЦОД OVH в Страсбурге, а RedHat увеличил минимальную стоимость подписки
- Релиз [vSphere 7.0 U2](https://blogs.vmware.com/vsphere/2021/03/announcing-vsphere-7-update-2-release.html) - [it's all about AI](https://www.vmware.com/company/news/releases/vmw-newsfeed.VMware-Evolves-Developer-and-AI-Ready-Infrastructure-to-Advance-Digital-Business.4ab5538a-c5f6-4ebd-92a2-1dd0beaf972c.html)
- Релиз Bitfusion 3.0 - [что же нового?](https://docs.vmware.com/en/VMware-vSphere-Bitfusion/3.0/rn/vsphere-bitfusion-30-release-notes.html)
- Сумасшедший референс [SAP HANA на vSAN](https://lenovosuccess.com/casestudy/krombacher-brauerei) и [вебинар по теме](https://vmware.zoom.us/webinar/register/WN_nt5e_S6XRjeymAvUyAF1JQ)
- Истории успеха: [НЛМК](https://www.vmware.com/content/dam/digitalmarketing/vmware/en/pdf/customers/vmware-20q4-nlmk-en.pdf)
- Напомним про [Future:NET 2021](https://future-net.exceedlms.com/student/authentication/register?utm_source=em_6012eb343c1fa&utm_campaign=7012H000001Yrma)

# Дополнительно
- [КРИ-П](https://www.youtube.com/c/КРИПы) - Критические Работы по Инфраструктуре - в Пятницу
- Весенние вебинары: [анонсы](https://www.youtube.com/playlist?list=PLSHgmTQvHHVFg6C-d6_ncX8O0buyY0Jce)