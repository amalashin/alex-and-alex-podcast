Title: Эпизод первый (пилот)
Subtitle: Проба пера - пилотная серия
Date: 2019-05-28 12:00
Tags: wone, euc, sddc, bitnami, redhat, vvd, pks, workspace
Summary: Пилотный выпуск нашего подкаста, посвящённого технологиям компании VMware и другим интересным штукам облачного мира ИТ.

# Новости EUC
- Большая тема на сегодня - это выход WOne 1905!
- [Поддержка двух симок](https://code.vmware.com/samples/4930)
- [Mobile Flows](https://youtu.be/fvqGBJQnWdE) и [как оно работает](https://youtu.be/smrBijbxxrc)
- [Статья Graeme Gordon](https://techzone.vmware.com/blog/understanding-horizon-connections) про подключение для админов
- [Новый курс](https://www.microinform.ru/vmwareAirWatch/WS1-DM.htm) Digital Workspace

# Новости облаков и автоматизации
- [VMware анонсировали покупку компании Bitnami](https://cloud.vmware.com/community/2019/05/15/vmware-to-acquire-bitnami)!
- Анонс двусторонней поддержки поддержки [RedHat OpenShift](https://blog.openshift.com/red-hat-openshift-and-vmware-better-together) и [VMware SDDC](https://octo.vmware.com/vmware-red-hat-bring-red-hat-openshift-vmware-sddc/)
- [Референсный дизайн](https://communities.vmware.com/thread/595478) VVD по PKS
- [Мастер установки PKS](https://blogs.vmware.com/cloudnative/2019/05/16/vmware-enterprise-pks-installation-wizard/)
