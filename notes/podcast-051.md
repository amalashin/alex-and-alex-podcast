Title: Эпизод пятьдесят первый
Date: 2020-06-17 12:00
Tags: uag, wone, tools, appconfig, truesso, horizon, vsphere7, ai/ml, openstack, vmworld2020, google, cloud
Summary: Лето набирает обороты, а новые технологии врываются в нашу жизнь, новости VMware - прямо из облака!

# Новости EUC
### Решение проблем в UAG и Workspace One UEM 
- Исследование [проблем с DNS на Unified Access Gateway](https://kb.vmware.com/s/article/50120424)
- Кластер [высокой доступности на Unified Access Gateway](https://www.virtualizationhowto.com/2020/03/ha-high-availability-to-remote-workers-with-vmware-uag-ha/)
- Настройка двойной аутентификации с применением сервера [RADIUS на Unified Access Gateway](https://www.virtualizationhowto.com/2020/03/enable-two-factor-authentication-for-vmware-horizon-uag/)
- Настройка [MS Outlook для iOS/Android с помощью MDM](https://docs.microsoft.com/en-us/Exchange/clients/outlook-for-ios-and-android/account-setup?redirectedfrom=MSDN&view=exchserver-2019)
    - Пары ключ-значение AppConfig [для настройки MS OutLook](https://kb.vmware.com/s/article/50120818)
- Обновление APNs:
    - Обновление [сертификата APNs](https://kb.vmware.com/s/article/2960965)
    - Обновление [сертификата APNs для приложений](https://kb.vmware.com/s/article/50121242)
    - [Решение проблем с APNs в СУБД SQL](https://digital-work.space/display/AIRWATCH/APNs+troubleshooting+in+SQL)
### Разные новости по EUC
- Инструмент быстрой [настройки TrueSSO в Horizon](https://flings.vmware.com/true-sso-configuration-utility)
- Игра из [лабораторной работы по Horizon](https://my.vmware.com/web/vmware/evalcenter?p=horizon-hol-bog-20)
- [Подключаемое оборудование в vSphere 7](https://blogs.vmware.com/vsphere/2020/03/vsphere-7-assignable-hardware.html) позволяет строить [кластеры DRS с видеокартами NVIDIA](https://vlenzker.net/2020/04/better-together-vsphere-7-and-horizon-7-12-with-nvidia-vgpus-in-high-end-vdi-environments/)

# Новости облаков и автоматизации
- Недавно говорили про  IoT и AI/ML - а тут вышел [новый флинг про ML](https://flings.vmware.com/vmware-machine-learning-platform)!
- Bitfusion [теперь интегрирован в vSphere](https://blogs.vmware.com/vsphere/2020/06/vsphere-bitfusion-elastic-infrastructure-ai-ml.html) (станет доступен в конце июля) 
- Разговор про микросервисы и современные приложения
- Появился на свет [VMware Integrated OpenStack 7.0](https://docs.vmware.com/en/VMware-Integrated-OpenStack/7.0/rn/VMware-Integrated-OpenStack-70-Release-Notes.html)
- Программа ["Готовы к будущему"](https://www.vmware.com/solutions/remote-work-technology.html)
- Первая в жизни VMware [реклама на TV](https://www.youtube.com/watch?v=Bb3tLZwKLvU) (запустится на каналах CNN, CNBC, MSNBC и Bloomberg в середине июля)
- 23 июня открывается [регистрация на VMworld 2020](https://www.vmworld.com/en/index.html)
- Вебинар про [Google Cloud VMware Engine](https://onlinexperiences.com/scripts/Server.nxp?LASCmd=AI:4;F:QS!10100&ShowUUID=B74FE748-B664-47D0-9E56-1525C51BB0D5&AffiliateData=CVC)
- Ещё раз обратим внимание на проблему [сертификата STS в vSphere](https://blogs.vmware.com/vsphere/2020/05/signing-certificate-is-not-valid-security-token-service-certificate-issue-in-vsphere.html)

# Дополнительно
- [КРИ-П](https://www.youtube.com/channel/UClg6v5K2QlMOdBf2ffGEJKw) - Критические Работы по Инфраструктуре - в Пятницу
