Title: Эпизод восемьдесят девятый
Date: 2022-01-14 12:00
Tags: blast, macos, workspace, security, vmware, nsx, innovations, future, cloud
Summary: Новогодние праздники закончились, причём у зарубежных ребят из VMware намного раньше, чем у нас. Поэтому мы с Лёхой встретились, чтобы запустить новый год крипкастов и обсудить, что нового-интересного случилось с начала января. Заваривайте чаёк-кофий и давайте к нам!

# Новости от ЛёхиР (alex8s)
- Оптимизация протокола [Blast в Horizon](http://blog.vmpress.org/2022/01/horizon-blast.html)
- Работа с ошибками, [логи в Horizon 8](https://searchvirtualdesktop.techtarget.com/tip/Navigating-VMware-logs-for-troubleshooting)
- Что значит [быть vExpert-ом?](https://mobile-jon.com/2021/12/13/what-the-vexpert-program-means-to-mobile-jon/)
- Управление, [процессы в macOS с помощью Workspace ONE UEM](https://mobile-jon.com/2021/12/20/no-api-to-block-that-pesky-macos-service-workspace-one-to-the-rescue/)
    - Официальная [документация по управлению процессами в macOS](https://docs.vmware.com/en/VMware-Workspace-ONE-UEM/services/macOS_Platform/GUID-AppsProcessRestrictionsformacOS.html)
- Переход [с Basic Auth на OAuth2 с Microsoft Exchange Online](https://www.evengooder.com/2021/12/basic-auth-deprecation-and-workspace-one.html)

# Новости от ЛёхиМ
- Разговор о безопасности
- [Вышел NSX-T 3.2](https://blogs.vmware.com/networkvirtualization/2021/12/nsx-t-3-2-innovations.html/)
- Новые пути изучения продуктов VMware: [VMware Tanzu](https://pathfinder.vmware.com/path/tanzu-overview?utm_source=fieldnews&utm_medium=social&utm_campaign=tanzu-spif), [Anywhere Workspace](https://pathfinder.vmware.com/v3/path/anywhere-workspace-overview?utm_source=field-news&utm_medium=social&utm_campaign=anywhere-spif)
- Виртуальная реальность
- [Смотрим в будущее вместе с VMware](https://news.vmware.com/leadership/vmware-2022-predictions)
- [Книга: VMware Cloud for Dummies](https://www.vmware.com/content/microsites/learn/en/1183345_REG.html)

# Дополнительно
- [КРИ-П](https://www.youtube.com/c/КРИПы) - Критические Работы по Инфраструктуре - в Пятницу
