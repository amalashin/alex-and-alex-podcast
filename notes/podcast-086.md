Title: Эпизод восемьдесят шестой
Date: 2021-11-20 12:00
Tags: spinoff, vmware, multicloud, vsphere, NVIDIA, education, wsone, uem, monterey
Summary: Пока у Лёхи апгрейдится операционная система на макбуке до новой версии, мы собрались с ним обсудить, что нового теперь по части управления у VMware в отношении макбуков, чему нынче стоит поучиться, о конференции NVIDIA, а также многое другое. Заваривайте чаёк, ставьте систему на установку свежих патчей и давайте к нам на новый крипкаст!

# Новости от ЛёхиМ 
- VMware стала самостоятельной компанией
- [Миссия компании VMware](https://news.vmware.com/company/vmware-story-trusted-foundation-accelerate-innovation?utm_source=vmwsource&utm_medium=referral&utm_campaign=company-22Q3-corp-ww) мульти-облачном мире - предоставление надёжной основы для продвижения инноваций
- Релиз vSphere 7u3a с интеграцией платформ VMware и NVIDIA - [технический обзор](https://core.vmware.com/blog/accelerating-workloads-vsphere-7-tanzu-technical-preview-kubernetes-clusters-gpus)
- Конференция [NVIDIA GTC](https://www.nvidia.com/gtc/?ncid=ref-spo-773357#cid=gtcnov21_ref-spo_en-us)
- Новые [обучалки по продуктам VMware](https://blogs.vmware.com/learning/2021/10/20/new-courses-for-september-and-october/)

# Новости от ЛёхиР (alex8s) 
- Набор из [7 интересных функций WS1 UEM](https://mobile-jon.com/2021/10/25/top-seven-features-in-workspace-one-you-may-not-know-about/)
- Анализ благополучной работы пользователей - [DEEM](https://blogs.vmware.com/euc/2021/10/what-is-digital-employee-experience-dex.html)
- Новая система [macOS Monterey в связке с WS1 UEM](https://blogs.vmware.com/euc/2021/11/macos-monterey-what-workspace-one-customers-need-to-know.html)
- Защита локального Horizon с помощью [облачных услуг по безопасности от VMware, вышежших за последние пару лет](https://www.evengooder.com/2021/10/Securing-Horizon-From-The-Cloud.html)

# Дополнительно
- [КРИ-П](https://www.youtube.com/c/КРИПы) - Критические Работы по Инфраструктуре - в Пятницу (теперь и видеоверсия нас).
