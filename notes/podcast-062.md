Title: Эпизод шестьдесят второй
Date: 2020-09-28 10:00
Tags: vmware, vmworld2020, vmworldsecemea, vcloud, networking, digital workspace, security, innovations
Summary: Всё о VMWORLD 2020 - аудио версия видео стрима, для тех кому удобнее слушать 🔊!

# VMWORLD 2020
Главное и ближайшее событие для нас с Лёхой - VMworld 2020. И вот мы туда зарегились, посмотрели в каталог докладов - а их там видимо-не видимо! Что же выбрать интересного посмотреть? - мы решили собраться онлайн и обсудить, какие темы приглянулись, ожидания-надежды, слухи и домыслы, вот это вот всё.

[Видеоверсия](https://youtu.be/t3thJB3BSu4)

- Общий обзор мероприятия
- Облачные сессии (vCloud)
- Сессии про цифровое рабочее место (Digital Workspace)
- Крутые технологии AI/ML, Blockchain и тд  (Innovations)
- Мир современных приложений (Modern Apps)
- Сетевые технологии и безопасность (Network & Security)
- Телеком и 5Ж (Telecom & 5G)

# Дополнительно
- [VMworld 2020 - Регистрация](https://reg.rainfocus.com/flow/vmware/vmworld2020/reg/form/contactInfo)
- [VMWORLD 2020 - Официальный сайт](http://vmworld.com) 
- [КРИ-П](https://www.youtube.com/channel/UClg6v5K2QlMOdBf2ffGEJKw) - Критические Работы по Инфраструктуре - в Пятницу
