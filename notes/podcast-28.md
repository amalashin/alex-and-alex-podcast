Title: Эпизод двадцать восьмой
Date: 2019-12-11 10:00
Tags: vra, abx, carbonblack, vmc, pks, wone, uem, piv-d, 
Summary: Двадцать восемь! А новости всё не кончаются, новый эпизод уже ждёт в облаке, а ещё мы появились в Яндекс.Музыке!

# Новости облаков и автоматизации
- Биты и байты: опыт использования ABX на реальной задаче, [бага при импорте нескольких файлов](https://communities.vmware.com/thread/621997), запускаем [русский marketplace на базе GitLab](https://gitlab.com/vra8) и пробуем его развить
- [Магический квадрат Gartner по гиперконвергентным системам](https://blogs.vmware.com/virtualblocks/2019/12/02/leader-2019-gartner-magic-quadrant-hyperconverged-infrastructure/) (VMware - один из лидеров)
- VMware - лидер по [результатам исследования Forrester](https://blogs.vmware.com/euc/2019/12/vmware-leader-forrester-wave.html) в области Unified Endpoint Management
- VMware Carbon Black - лучший выбор в качестве платформы для защиты рабочих мест ([по результатам голосования заказчиков](https://www.vmware.com/company/news/releases/vmw-newsfeed.VMware-Carbon-Black-Recognized-as-a-2019-Gartner-Peer-Insights-Customers-Choice-for-Endpoint-Protection-Platforms-EPP.1955101.html))
- Лучший выбор для развития облачной стратегии - VMware on AWS ([по отзывам заказчиков](https://www.vmware.com/company/news/releases/vmw-newsfeed.Organizations-Globally-Select-VMware-Cloud-on-AWS-as-the-Foundation-for-their-Cloud-Strategy.1955506.html))
- [Запустилась услуга (бета-версия) AWS Outposts](https://cloud.vmware.com/community/2019/12/03/vmc-aws-outposts/) с vCloud Foundation внутри ([зарегистрироваться](https://cloud.vmware.com/vmc-aws-outposts/get-started))!
- Небольшие обучалки VMware по [Enterprise PKS](https://pathfinder.vmware.com/path/enterprisePKS) и [безопасности рабочих мест Workspace One с Carbon Black](https://pathfinder.vmware.com/path/workspacesecurity)
- Базовый материал о том, что такое [современные приложения (modern application)](https://octo.vmware.com/defining-modern-application/)

# Новости EUC
- Настройка [Workspace One UEM API v2](https://mobile-jon.com/2019/11/30/a-foray-into-working-with-the-vmware-workspace-one-apis/)
- Workspace One PIV-D Manager, [что это такое](https://www.air-watch.com/capabilities/piv-d-manager) + [видео](https://youtu.be/ru9P_IQI_Ao)
- [Windows 7 End of Life](https://www.vmgu.ru/news/vmware-horizon-7-and-windows-7-after-end-of-support)
