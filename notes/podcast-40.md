Title: Эпизод сороковой
Date: 2020-03-13 12:00
Tags: hybrid, cloud, tanzu, k8s, terraform, ai/ml, pivotal, wone, uem, android, macos, certificates
Summary: Пандемия 😱! Но это не мешает выпуску свежего эпизода нашего подкаста - напрямую из облаков, бесконтактно!

# Новости облаков и автоматизации
- [Отчёт IDC - анализ рынка смартфонов](https://www.idc.com/getdoc.jsp?containerId=prEUR246048620), снижение продаж из-за COVID-19
- [Анонс выхода новых мажорных версий продуктов VMware](https://www.vmware.com/company/news/releases/vmw-newsfeed.VMware-Announces-Expanded-Portfolio-of-Products-and-Services-to-Help-Customers-Modernize-Applications-and-Infrastructure.7ee66a70-1564-49d6-9d6b-730016ce92dc.html)
- vCloud Availability 3.5 - помощь в миграции со старых версий vSphere:
    - [Установка и подключение](http://bit.ly/32FuNs5)
    - [Настройка и использование](http://bit.ly/3a8LjTG)
    - [Сеть и миграция](http://bit.ly/2Ps30pg)
    - [Дополнительная информация и комментарии](http://bit.ly/2T3WHdK)
- Модернизация приложений в мульти-облачном мире: [документ-отчёт](https://www.vmware.com/content/dam/digitalmarketing/vmware/en/pdf/microsites/app-modernization/vmware-12594-modern-apps-launch-report-v3.2.pdf)
- [Пример использования современных приложений в гибридном облаке](https://www.vmware.com/content/dam/digitalmarketing/vmware/en/pdf/microsites/app-modernization/vmware-a-primer-modern-apps-and-the-hybrid-cloud.pdf)
- Tanzu - с начала до сегодняшних дней, историческая справка и текущее портфолио VMware для разработчиков
- [Почему Kubernetes на голом железе может быть лучше?](https://www.ericsson.com/en/blog/2020/3/benefits-of-kubernetes-on-bare-metal-cloud-infrastructure)
- [Spotify открыл terraform-модуль для Kubeflow ML](https://www.infoq.com/news/2020/03/spotify-terraform-kubeflow/)
- [Каталог решений Pivotal](https://network.pivotal.io)

# Новости EUC
- Анализ IDC MarketScape‘2020 - [VMware остаётся лидером в категории VDI](https://blogs.vmware.com/euc/2020/03/idc-marketscape-report-virtual-client-computing.html)
- [Эталонная архитектура всего Workspace One](https://techzone.vmware.com/resource/workspace-one-and-horizon-reference-architecture)
- [Таблица General Support для Workspace One UEM](https://kb.vmware.com/s/article/2960922)
- [Как VMware готовится к Android 11](https://kb.vmware.com/s/article/78104?lang=en_US)
- [Блог от Google об изменениях настроек приватности в режиме COPE в Android 11](https://blog.google/products/android-enterprise/work-profile-privacy)
- [Управление обновлениями в macOS через Workspace One UEM](https://techzone.vmware.com/sites/default/files/resource/managing_major_os_updates_for_mac_vmware_workspace_one_operational_tutorial.pdf)
- [Обновление всех сертификатов в Workspace One UEM](https://kb.vmware.com/s/article/2961630)
- [Системные и сетевые требования для Workspace One Access SaaS/On-Premise](https://youtu.be/OWYo6WneaMg)

# Дополнительно
[Весенние вёбинары русскоговорящей команды VMware](https://bit.ly/38Afp1r)
