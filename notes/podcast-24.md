Title: Эпизод двадцать четвёртый
Date: 2019-11-12 10:00
Tags: vmworld, serverless, vra, vrops, vcd, azure, wone, uem, vmc
Summary: Что произошло за неделю? Узнайте прямо из облака!

# Новости облаков и автоматизации
- VMworld EU - [главная сессия](https://www.vmworld.com/en/europe/learning/general-sessions.html) и [видеозаписи выступлений](https://videos.vmworld.com/global/2019?poster_filter=EUROPE)
- Про Serverless и [vRealize Automation 8 ABX](https://docs.vmware.com/en/vRealize-Automation/8.0/Using-and-Managing-Cloud-Assembly/GUID-55847415-5920-47E7-86BD-20CD9EB6BA6B.html)
- [Бета-программа vRealize Operations Cloud](https://cloud.vmware.com/community/2019/11/05/announcing-vrops-cloud-beta-program/)
- Окончание поддержки vSphere 6.0 (в марте 2020): [инфографика](https://www.vmware.com/content/dam/digitalmarketing/vmware/en/pdf/products/vsphere/vmware-top-10-reasons-to-upgrade-to-vsphere-6-7-infographic.pdf) и [сообщение об этом](https://blogs.vmware.com/vsphere/2019/10/vsphere-6-0-reaches-end-of-general-support-eogs-in-march-2020.html)
- [VMware Cloud Director Service](https://cloudsolutions.vmware.com/bite-sized-vmc) - бета-программа
- Партнёрство между VMware и Microsoft продолжается - [теперь Azure и SQL Server 2019](https://www.vmware.com/company/news/releases/vmw-newsfeed.VMware-and-Microsoft-Continue-to-Partner-to-Deliver-Greater-Impact-to-Customers-Across-Client-Cloud-and-Data-Initiatives.1940878.html)

# Новости EUC
- [Интеграция Workspace One UEM SDK с Appdome](https://blogs.vmware.com/euc/2019/11/appdome-for-vmware-workspace-one.html) и [более подробно](https://www.appdome.com/collateral-library/no-code-workspace-one-integration/)
- [Сравнение редакций Appdome](https://d26e7renmwszsg.cloudfront.net/wp-content/uploads/2019/11/appdome-for-workspace-ONE-2.png)
- [Выход Microsoft Endpoint Manager](https://www.microsoft.com/en-us/microsoft-365/blog/2019/11/04/use-the-power-of-cloud-intelligence-to-simplify-and-accelerate-it-and-the-move-to-a-modern-workplace/)
- [Управление Windows 10 с помощью Workspace ONE for Microsoft Endpoint Manager](https://blogs.vmware.com/euc/2019/11/workspace-one-microsoft-endpoint-manager.html)
- [Новый курс по управлению Win10 с помощью Workspace One](https://www.microinform.ru/vmwareAirWatch/WS1-UEMW10.htm)
