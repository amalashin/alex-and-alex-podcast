Title: Эпизод шестьдесят третий
Date: 2020-10-15 23:00
Tags: uag, seg, zabbix, mwc, hello, esxi, arm, monterey, smartnic, tanzu, vic 
Summary: VMworld кончился, но мы сегодня про другое - ведь вышел ESXi на ARM ⚡️! 

# Новости управления рабочими местами
- Релиз [Unified Access Gateway 20.09](https://techzone.vmware.com/vmware?share=video2909);
    - Обновления по безопасности без переустановки UAG с указанного репозитория;
    - Настройка SEG компонента без захода по SSH;
    - Выгрузка лога внутренних служб в syslog, выгрузка IP адреса клиента к Horizon;
    - Сервисная учётка для мониторинга UAG (read-only API).
- Крупные мероприятия на октябрь:
    - 30-го октября [Zabbix Summit](https://www.zabbix.com/events/zabbix_summit_2020)
    - Отменён [Mobile World Congress - America](https://www.mwclosangeles.com/)
    - 27-29 октября [AIDevWorld](https://aidevworld.com/)
- Настройка [Windows Hello for Business](https://brookspeppin.com/2020/09/28/how-to-configure-passport-for-work-csp-in-workspace-one/) - критерии к коду PIN в WS1 UEM; 
- Что интересного обещали по части EUC [на VMworld](https://www.vmworld.com/en/video-library/search.html#year=2020):
    - Доступ к порталу Access по расписанию;
    - Поддержка Linux в WS1 UEM, профили настроек, мониторинг и Assist;
    - Мониторинг пользователя в WS1 - "Digital Employee Experience Management DEEM";
    - Freestyle Orchestrator для Windows и macOS;

# Новости облаков и автоматизации
- [ESXi ARM Fling](https://flings.vmware.com/esxi-arm-edition) и [блогопост](https://blogs.vmware.com/vsphere/2020/10/announcing-the-esxi-arm-fling.html)
- Новый [логотип ESXi ARM](https://blogs.vmware.com/arm/2020/09/23/new-esxi-arm-logo/)
- [Project Monterey aka SmartNIC](https://blogs.vmware.com/vsphere/2020/09/announcing-project-monterey-redefining-hybrid-cloud-architecture.html)
- [Windows 10 тоже на ARM](https://blogs.windows.com/windowsexperience/2020/09/30/now-more-essential-than-ever-the-role-of-the-windows-pc-has-changed/)
- Новые редакции Tanzu без NSX
- vSphere Integrated Containers (aka VIC) - конец поддержки
- [vRealize Operations 8.2 теперь публично доступен (GA)](https://blogs.vmware.com/management/2020/10/whats-new-vrops-82.html)

# Дополнительно
- [КРИ-П](https://www.youtube.com/channel/UClg6v5K2QlMOdBf2ffGEJKw) - Критические Работы по Инфраструктуре - в Пятницу
