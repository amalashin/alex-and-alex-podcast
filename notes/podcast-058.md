Title: Эпизод пятьдесят восьмой
Date: 2020-09-04 12:00
Tags: vra8.2, vov, cybersecurity, workstation, fusion, instant clone, windows 10, workspace one, api
Summary: В этом выпуске мы погружаемся детальнее в технологии EUC, а также обсуждаем новый vRealize Automation, готовим почву для серьёзного разбора.

# Новости облаков и автоматизации
- [vRealize Automation Cloud 8.2](https://blogs.vmware.com/management/2020/08/whats-new-in-vrealize-cloud-management-8-2.html) - роадмап для онпрем
- Новая компетенция для партнёров - современные приложения
- VMware on VMware - [перед VMworld 2020](https://blogs.vmware.com/events/vmworld-2020-content-catalog/)
- Кибербезопасность - [раскол индустрии](https://www.vmware.com/content/dam/digitalmarketing/vmware/en/pdf/cio-vantage/vmware-the-cybersecurity-industry-is-broken.pdf)
- VMware Workstation 12 и Fusion 16 - [теперь с кубернетесом внутри](https://www.vmware.com/company/news/releases/vmw-newsfeed.VMware-Brings-Kubernetes-to-Fusion-12-and-Workstation-16-Releases.c0e58b22-b2fc-4c4a-9052-8db04c0f58dc.html)

# Новости EUC
- (bits & bytes) Сиротская жизнь мгновенных клонов - как ведут себя Instant Clones при проблемах с parent машиной?
- Windows 10 Update на стероидах - [функция перехода на целевой релиз](https://brookspeppin.com/2020/08/26/windows-10-feature-upgrades-full-control/)
- Миротворец: как с помощью Workspace One Access [подружить два домена без доверия между ними](https://blog.eucse.com/using-vmware-access-to-transform-users-between-active-directory-domains/)
- Здесь и сейчас! - [Синхронизация AD с Workspace One Access через API](https://blog.eucse.com/workspace-one-access-manual-ad-sync-via-api/)

# Дополнительно
- [КРИ-П](https://www.youtube.com/channel/UClg6v5K2QlMOdBf2ffGEJKw) - Критические Работы по Инфраструктуре - в Пятницу
- [CREEP](https://www.youtube.com/watch?v=qjdY1g0eL1M&list=PL0aZOZ-cWW_V4-5VFDBG0kBrOeSWdx0Sw) - Commit Every Experiment in Production
