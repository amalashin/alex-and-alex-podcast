Title: Эпизод двенадцатый
Date: 2019-08-18 12:00
Tags: pks, pivotal, wone, uem, mobilesso, airwatch
Summary: Мириан снова в отпуске, но новая загадка ждёт ответа!

# Новости облаков и автоматизации
- [PKS 1.5](https://blogs.vmware.com/cloudnative/2019/08/15/vmware-enterprise-pks-1-5-production-workloads/) - новый дашборд и поддержка Windows-контейнеров (бета), а так же другие улучшения
- [Анонс о возможности приобретении компании Pivotal компанией VMware](https://s2.q4cdn.com/112802898/files/doc_news/2019/08/Statement-regarding-Dell-Technologies-13D-filing-on-August-14-2019.pdf)

# Новости EUC
- [Новый инструмент миграции профилей Workspace One UEM](https://labs.vmware.com/flings/workspace-one-uem-workload-migration-tool)
- Workspace One UEM - [снова в лидерах магического квадранта Gartner](https://www.vmware.com/learn/304750_REG.html?cid=7012H000001ohxb) и дискуссия о поддержке Windows 7
- Новая версия iOS 13 и поддержка AirWatch, а так же окончание поддержки AirWatch Container
- Биты и байты - Mobile SSO для Android

# Дополнительно
- Бонус - как начать кодить, когда куча дел и некогда, challenge ["100 дней кода"](https://www.100daysofcode.com)
- Новая загадка - сегодня от Алексея
