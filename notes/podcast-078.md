Title: Эпизод семьдесят восьмой
Date: 2021-05-11 12:00
Tags: open grid alliance, microservices, vrealize, kubernetes, wone, uem, vmware, api
Summary: Праздники - хорошее время успеть сделать всё то, что постоянно откладываешь. Например, встретиться с Лёхой и обсудить свежие новости в ИТ, переплетения разных вариантов виртуализации, облака, доступы по API и многое другое. Присоединяйтесь к нашим посиделкам.

# Новости от ЛёхиМ 
- Интернет 2.0 - эволюция и [Open Grid Alliance](https://www.opengridalliance.org)
- От монолита к микросервисам - [вебинар от VMware IT](https://players.brightcove.net/1534342432001/default_default/index.html?videoId=6236870381001)
- Вышел [vRealize Network Insight 6.2](https://blogs.vmware.com/management/2021/04/whats-popping-with-vrealize-network-insight-6-2-cloud.html)
- [Как из кубера управлять виртуальными машинами?](https://blogs.vmware.com/vsphere/2021/04/introducing-the-vsphere-virtual-machine-service.html)

# Новости от ЛёхиР (alex8s) 
- Интеграция [Workspace ONE UEM и Azure Active Directory](https://skudzma.com/?p=157/)
- Интеграция [Worskapce ONE UEM и Access](https://theidentityguy.ca/2021/04/21/getting-started-with-workspace-one-uem-and-workspace-one-access/)
- Изменение [пароля локального пользователя в Windows 10] (https://digitalworkspace.one/2021/04/29/kb-change-a-local-user-password-via-csp/)
- Изучение [API vCenter на симуляторе](https://nicovibert.com/2021/01/06/terraform-python-vcenter-vcsim/)

# Дополнительно
- [КРИ-П](https://www.youtube.com/c/КРИПы) - Критические Работы по Инфраструктуре - в Пятницу (теперь и видеоверсия нас).
- Весенние вебинары: [анонсы](https://www.youtube.com/playlist?list=PLSHgmTQvHHVFg6C-d6_ncX8O0buyY0Jce)
