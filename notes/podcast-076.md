Title: Эпизод семьдесят шестой
Date: 2021-03-26 12:00
Tags: vrealize, network insight, vra, zero trust, nvidia, tanzu, mesh7, rest, horizon, instant clones, android, truesso, uag, vmware
Summary: Тёплым мартовским вечером мы собрались с Лёхой обсудить горячие новинки и вжарить крутые анонсы. Наливаем чайку/кофейку, берём закуски и присоединяемся к душевному разговору о высоких технологиях.

# Новости от ЛёхиМ 
- Анонс [нововведений в vRealize 8.4](https://blogs.vmware.com/management/2021/03/announcing-whats-new-in-vrealize-8-4.html)
- Анонс [vRealize Network Insight 6.2](https://blogs.vmware.com/management/2021/03/announcing-vmware-vrealize-network-insight-6-2.html)
- Анонс [vRealize Automation 8.4](https://blogs.vmware.com/management/2021/03/announcing-vra-8-4.html)
- Что ИТ VMware получили при [внедрении модели Zero Trust](https://blogs.vmware.com/vov/2021/03/08/steps-for-implementing-zero-trust-linked-to-building-a-better-colleague-experience-part-two)
- Новые компетенции для партнёров - версия с Tanzu 
- VMware с NVIDIA - инфраструктура для AI и другой взгляд на трансформацию бизнеса
- VMware [планирует приобрести Mesh7](https://mesh7.com/blog/mesh7-to-be-acquired-by-vmware/)

# Новости от ЛёхиР (alex8s) 
- [Horizon 21.03 - что нового?](https://techzone.vmware.com/blog/whats-new-vmware-horizon-version-8-2103)
- Настройка [TrueSSO в Horizon с помощью UAG](https://www.carlstalhood.com/vmware-horizon-true-sso-uag-saml/)
- Демо-превью [Android 12 для разработчиков - что нового с точки зрения MDM](https://blogs.vmware.com/euc/2021/03/android-12-enterprise-management-security-and-apps.html?utm_source=feedly&utm_medium=rss&utm_campaign=android-12-enterprise-management-security-and-apps):
- Миграция [с Android Legacy в Android for Enterprise](https://blogs.vmware.com/euc/2021/03/the-ultimate-guide-to-android-enterprise-migration-with-workspace-one.html?utm_source=feedly&utm_medium=rss&utm_campaign=the-ultimate-guide-to-android-enterprise-migration-with-workspace-one)
- [Вебинар Лёхи по ControlUp 8-го апреля](https://www.vmware.com/ru/learn/RU_WEBINARS_REG1.html)


# Дополнительно
- [КРИ-П](https://www.youtube.com/c/КРИПы) - Критические Работы по Инфраструктуре - в Пятницу (теперь и видеоверсия нас).
- Весенние вебинары: [анонсы](https://www.youtube.com/playlist?list=PLSHgmTQvHHVFg6C-d6_ncX8O0buyY0Jce)
