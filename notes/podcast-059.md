Title: Эпизод пятьдесят девятый
Date: 2020-09-11 12:00
Tags: vra8.2, vmworld2020, vmworldsecemea
Summary: А сегодня спецвыпуск - разговариваем про vRealize 8.2 с Сергеем Калугиным!

# vRealize Automation 8.2

# Дополнительно
- [КРИ-П](https://www.youtube.com/channel/UClg6v5K2QlMOdBf2ffGEJKw) - Критические Работы по Инфраструктуре - в Пятницу
- [CREEP](https://www.youtube.com/watch?v=qjdY1g0eL1M&list=PL0aZOZ-cWW_V4-5VFDBG0kBrOeSWdx0Sw) - Commit Every Experiment in Production
