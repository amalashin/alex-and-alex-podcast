Title: Эпизод двадцать пятый
Date: 2019-11-20 10:00
Tags: wone, horizon, pks, vra, gss, nfv
Summary: Пока Алексей в командировке, а Алексей готовится к конференции, слушайте наши анонсы и интервью с Димой!

# Новости EUC
- [Workspace One Intelligence for Modern Apps](https://blogs.vmware.com/euc/2019/11/workspace-one-intelligence-consumer-apps.html)
- [Horizon Control Plane: архитектура Horizon Cloud, что передаёт Horizon Cloud Connector?](https://docs.vmware.com/en/VMware-Horizon-7/7.6/horizon-integration/GUID-4B40CE42-70A8-43B0-A99B-D53A12FC698C.html)

# Новости облаков и автоматизации
- Спец предложение сервиса с Essential PKS - как VMware может помочь внедрить k8s
- vRealize Automation Standard EOA - сверьтесь c [матрицей жизненного цикла продуктов](https://www.vmware.com/content/dam/digitalmarketing/vmware/en/pdf/support/product-lifecycle-matrix.pdf)
- GSS выравнивает часы работы со 2го декабря, [сверьтесь с расписанием](https://www.vmware.com/support/policies/severity.html#business-hours)
- [VMware и Nokia - партнёрство по теме облачных решений и NFV](https://www.nokia.com/about-us/news/releases/2019/11/14/nokia-and-vmware-expand-partnership-to-ease-large-scale-multi-cloud-operations/)
