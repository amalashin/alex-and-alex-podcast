Title: Эпизод тридцать второй
Date: 2020-01-17 00:00
Tags: pivotal, vcf, iot, cloud, appvolumes, wone
Summary: 32, как зуба или 5, как у айтишников! В общем, первый выпуск этого года прямо из облака о том, как Лёха и Лёха провели новогодние каникулы!

# Новости облаков и автоматизации
*   [Welcome Pivotal!](https://www.forbes.com/sites/ilkerkoksal/2020/01/02/vmware-closes-27-billion-acquisition-of-pivotal-software/#7d6102d8e6ae)
*   Вышел vCloud Foundation 3.9.1 - [заметки о релизе](https://docs.vmware.com/en/VMware-Cloud-Foundation/3.9.1/rn/VMware-Cloud-Foundation-391-Release-Notes.html)
*   Рассказ Алексея о том, "как я провёл каникулы" (спойлер: автоматизация, но другая - [Internet of Things от VMware](https://www.vmware.com/products/pulse-iot-device-management.html))
*   [Предсказания по развитию технологий на 2020 год](https://www.vmware.com/radius/enterprise-tech-trends-2020)
*   Что нас ждёт в следующую пятилетку со стороны ИТ (2020-2025годы) - [топ 10 предположений от CIO](https://www.vmware.com/radius/10-cio-predictions-2020-2025)
*   [Важность наличия облачной стратегии](https://blogs.vmware.com/management/2019/12/the-importance-of-a-cloud-strategy.html)
*   Вебинар [Как предприятию быть готовым к современным приложениям](https://onlinexperiences.com/scripts/Server.nxp?LASCmd=AI:4;F:QS!10100&ShowUUID=238B7529-5249-48AE-8AFF-137A97897F27&AffiliateData=Fieldnews)

# Новости EUC
*   [JMP Automated Workflows](https://youtu.be/8ujAGEKrw2I)
*   [Что нового в VMware App Volumes 4](https://techzone.vmware.com/blog/whats-new-vmware-app-volumes-4)
*   [Best Practices and FAQs for Architecting VMware Workspace ONE Access](https://techzone.vmware.com/blog/announcing-best-practices-and-faqs-architecting-vmware-workspace-one-access)
