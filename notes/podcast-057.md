Title: Эпизод пятьдесят седьмой
Date: 2020-08-21 13:00
Tags: horizon 8, uag, performance, idc, vrealize, vmworld, tanzu, oracle, skyline
Summary: Лето почти закончилось, а интересные события в ИТ только начинаются! Слушаем! ☁️

# Новости управления рабочими местами
- Новый [Horizon 8 (2006) на дворе! Видеообзор новшеств.](https://youtu.be/KqphU5EAIZo)
- Подробная [статья с подробностями про Horizon 8](https://techzone.vmware.com/blog/whats-new-vmware-horizon-8-2006-cart-app-volumes-and-dynamic-environment-manager)
- Переезд [на Horizon 8](https://techzone.vmware.com/resource/modernizing-vdi-new-horizon)
- Новые возможности [Unified Access Gateway 3.10](https://techzone.vmware.com/blog/whats-new-vmware-unified-access-gateway-310)
    - Перезапись заголовка Re-Write Origin Header
    - Шаблон /gateway/resources/(.*) включает реверс-прокси для веб-ресурсов
    - Максимальный уровень использования ЦПУ

# Новости облаков и автоматизации
- VMware - снова #1 в мире среди ИТ-автоматизаторов ([по мнению IDC](https://www.vmware.com/learn/612434_REG.html?cid=7012H000001Oopn&src=af_5f22d668f2793))
- [Анонс vRealize Operations 8.2](https://blogs.vmware.com/management/2020/08/announcing-vrealize-operations-8-2.html)
- [Брошюра по архитектуре мульти-облаков](https://www.vmware.com/solutions/multi-cloud-architecture.html)
- Skyline - [пути изучения](https://cloud.vmware.com/skyline)
- [VMworld 2020](https://www.vmworld.com/en/index.html?cid=7012H000001xDgF&src=em_5ebc57bec5750) - более 800 сессий, обучения, консультации с экспертами и много чего ещё!
- Брошюра - [объединяем разработчиков и эксплуатацию](https://www.vmware.com/cio-vantage/articles/bridging-the-developer-and-operations-divide.html)
- Tanzu Build Services - [наконец-то бета](https://tanzu.vmware.com/build-service)
- [Oracle Cloud VMware Solutions](https://www.oracle.com/uk/news/announcement/oracle-cloud-vmware-solution-2020-08-06.html) - уже ждёт заказчиков

# Дополнительно
- [Кастомизация терминала на macOS с установкой ZSH](https://brandon.azbill.dev/how-to-customize-your-zsh-terminal/)
- [КРИ-П](https://www.youtube.com/channel/UClg6v5K2QlMOdBf2ffGEJKw) - Критические Работы по Инфраструктуре - в Пятницу
- [CREEP](https://www.youtube.com/watch?v=qjdY1g0eL1M&list=PL0aZOZ-cWW_V4-5VFDBG0kBrOeSWdx0Sw) - Commit Every Experiment in Production
