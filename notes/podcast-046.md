Title: Эпизод сорок шестой
Date: 2020-05-05 14:00
Tags: vvd, vsphere7, k8s, exams, vcd, vov, horizon, win10, ios
Summary: Мир! Труд! Май! Давай подкаст скачай! Свежие новости, прямо из облаков 🚩

# Новости облаков и автоматизации
- [VVD 6.0](https://docs.vmware.com/en/VMware-Validated-Design/6.0/rn/vmware-validated-design-60-release-notes.html)
- [vSphere 7 Academy](https://www.vmware.com/products/vsphere/academy.html)
- Новый курс Kubernetes Academy: [Создание приложений для Kubernetes](https://kube.academy/courses/building-applications-for-kubernetes)
- VMware IT [полученные знания, в ответ на COVID-19](https://www.vmware.com/cio-vantage/articles/lessons-learned-covid-19-bask-iyer-cio-vmware.html)
- Возможность [удалённой сдачи экзаменов VMware](https://blogs.vmware.com/education/2020/04/16/remote-exam-testing-is-here/)
- VMware Cloud Director 10.1 - [новая версия уже GA, ребрендинг и всё такое](https://blogs.vmware.com/cloudprovider/2020/04/vmware-cloud-director-10-1-generally-available-and-re-branded.html)
- VMware on VMware - [путь ИТ-отдела к гибридному облаку](https://blogs.vmware.com/vov/2020/04/22/vmware-its-journey-to-a-hybrid-cloud-solution/)
- gg - [новый фреймворк, старая парадигма](http://stanford.edu/~sadjad/gg-paper.pdf)
- [Как развернуть минимальный домашний Kubernetes](https://www.virtuallyghetto.com/2020/04/deploying-a-minimal-vsphere-with-kubernetes-environment.html)
- Обновляемый список SaaS-приложений, серверную часть которых можно запустить на своей системе - [Большой список по разделам](https://github.com/awesome-selfhosted/awesome-selfhosted)

# Новости EUC
- [Поддержка серверных и настольных ОС в Horizon 8](https://kb.vmware.com/s/article/78715)
- [Сенсоры по Windows 10](https://brookspeppin.com/2020/04/15/workspace-one-sensors-best-practices/)
- [Примеры готовых сенсоров для разных задач](https://github.com/vmware-samples/euc-samples/tree/master/Windows-Samples/Sensors)

# Дополнительно
- [Критическая уязвимость в iOS](https://blog.zecops.com/vulnerabilities/youve-got-0-click-mail/)
- КРИ-П - что это такое, узнаете в подкасте!
- [Весенние вёбинары русскоговорящей команды VMware](https://bit.ly/38Afp1r)
