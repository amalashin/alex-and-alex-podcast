Title: Эпизод семьдесят девятый
Date: 2021-05-29 12:00
Tags: wsone, uem, rest api, zero trust, airwatch, vmware, hol, security, carbon black, vcf, nsx
Summary: В канун тёплого лета мы связались с Лёхой, чтобы разобраться с программированием REST API, к чему может привести назначение нового генерального директора VMware, а также обсудить свежие анонсы и веб-мероприятия. Заваривайте чай/кофий и заходите в наш эфир.

# Новости от ЛёхиР (alex8s)
- Как [подключиться к WS1 UEM/AirWatch REST API?](https://digital-work.space/display/AIRWATCH/AirWatch+REST+API)
- Как [удалять устройства в AirWatch REST API?](https://macmule.com/2015/12/15/deleting-unenrolled-devices-via-the-airwatch-api/)
- Примеры [написания методов работы с REST API](https://digital-work.space/display/AIRWATCH/AirWatch+REST+API+Python+Examples)
- Скрипты к REST API (на Powershell) [с аутентификацией через сертификат](https://digitalworkspace.one/2021/04/19/certificate-authentication-for-workspace-one-api-with-powershell-scripts/)
- Что такое Zero-Trust: [версия от мобильного Джона](https://mobile-jon.com/2021/05/18/mobile-jons-guide-to-zero-trust-security/)
- Настройка Zero-Trust: [доступ по сертификатам к ресурсам с клиентов Windows10 и macOS](https://www.evengooder.com/2021/03/WS1-certificates-4-zero-trust.html)


# Новости от ЛёхиМ
- Рагу Рагурам заступил в должность CEO VMware
- [Лабораторная работа](https://labs.hol.vmware.com/HOL/catalogs/lab/9582) по Oracle Cloud VMware Solutions
- [VMware Connect Learning](https://learning.customerconnect.vmware.com/oltpublish/site/cms.do?view=home)
- Введение в платформу [VMware Blockchain](https://zoom.us/signin?_x_zm_rtaid=846FCHrDSnWWDBj65EEiwA.1621888140111.226edfa7206b20c2a5cd70f90966f52c&_x_zm_rhtaid=478) и несколько [вебинаров](https://vmware.sabacloud.com/Saba/Web_spf/NA1PRD0121/app/shared;spf-url=common%2Fledetail%2Fcours000000000058865) по теме
- Альянс VMware и HPE - [вёбинар по ВРМ](https://connect.hpe.com/VDI-Top-Trends-emea-en)
- Референсы: 
  - [Как Duke Energy использует Tanzu](https://www.vmware.com/company/customers/index/fullpage.html?path=/content/web-apps-redesign/customer-stories/2021/utility-provider-powers-lives-with-vmware-technology-and-tanzu-labs)
  - [Как Netflix использует Carbon Black](https://www.vmware.com/company/customers/index/fullpage.html?path=/content/web-apps-redesign/customer-stories/2021/using-the-vmware-approach-to-intrinsic-security-to-stay-safe)
  - [Как муниципалитет Гарлема использует vCF, NSX и WOne](https://www.vmware.com/company/customers/index/fullpage.html?path=/content/web-apps-redesign/customer-stories/2021/vmware-offers-a-stable-it-foundation-to-strengthen-digital-services)
- [Отчёт VMware по угрозам безопасности](https://blogs.vmware.com/security/2021/05/vmware-threat-landscape-report-blog.html)
- [VMware назван лидером в Endpoint Security (по мнению Forrester)](https://blogs.vmware.com/security/2021/05/forrester-names-vmware-a-leader-in-endpoint-security-software-as-a-service.html)
- Мероприятия: [Security Connect 2021](https://myevents.vmware.com/widget/vmware/securityconnect/connect21-catalog?tab.geo=1587669995174002GTIu)

# Дополнительно
- [КРИ-П](https://www.youtube.com/c/КРИПы) - Критические Работы по Инфраструктуре - в Пятницу
- Весенние вебинары: [анонсы](https://www.youtube.com/playlist?list=PLSHgmTQvHHVFg6C-d6_ncX8O0buyY0Jce)
