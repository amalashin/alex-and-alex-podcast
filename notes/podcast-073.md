Title: Эпизод семьдесят третий
Date: 2021-02-20 12:00
Tags: wone, fido2, byod, vdi, vmware, kubernetes, vrealize, neetworking, nsx-t, vcf, future:net
Summary: Свежие зимние новости мы решили с Лёхой обсудить в новомодном Clubhouse 👋. Присоединяйтесь к нам на подкаст/крипкаст, а теперь ещё на «клубкаст», где мы обсуждаем горячие вопросы последней недели!

# Новости от ЛёхиР (alex8s)
- Новые [возможности Workspace One Access по защите входа: Login Risk Score, FIDO2 Passwordless Authentication](https://blogs.vmware.com/euc/2021/02/fido2-support-and-login-risk-score-vmware-workspace-one-access.html?utm_source=feedly&utm_medium=rss&utm_campaign=fido2-support-and-login-risk-score-vmware-workspace-one-access)
- А собственно [что такое FIDO2 и чем полезно?](https://mobile-jon.com/2021/02/15/is-workspace-one-access-barking-up-the-right-tree-with-fido2/)
- Размышления [о VPN тоннеле при BYOD сценарии](https://docs.vmware.com/en/Unified-Access-Gateway/2012/uag-deploy-config/GUID-8FB7A32B-E388-476B-8A11-956A84730982.html)
    - Сравниваем VPN с VDI, чем же VDI лучше?

# Новости от ЛёхиМ
- [Проспект](https://blogs.vmware.com/vsphere/2021/01/why-choose-vmware-virtualization-kubernetes-containers.html), почему виртуализация VMware для контейнеров и кубера лучше
- GA vRealize 8.3, анонсы и материалы:
	- [Три бизнес-результата управления облаками](https://blogs.vmware.com/management/2021/02/3-outcomes-of-how-vrealize-8-3-manages-clouds.html)
	- [Технические нововведения vRA 8.3](https://blogs.vmware.com/management/2021/02/announcing-vmware-vrealize-automation-8.3.html)
	- [Погружаемся в новые фичи vRealize Network Insight 6.1](https://blogs.vmware.com/management/2021/02/whats-new-in-vrealize-network-insight-6-1.html)
	- [Миграция на NSX-T](https://blogs.vmware.com/management/2021/02/automating-nsx-v-to-nsx-t-migration-with-vrealize-automation-8.3.html)
	- [VMware Cloud Foundation 4.2](https://www.vmware.com/company/news/updates/2021/cloud-foundation-4-2.html) (теперь с поддержкой S3)
- [Future:NET - новое индустриальное мероприятие VMware](https://future-net.exceedlms.com/student/authentication/register)


# Дополнительно
- [КРИ-П](https://www.youtube.com/c/КРИПы) - Критические Работы по Инфраструктуре - в Пятницу
