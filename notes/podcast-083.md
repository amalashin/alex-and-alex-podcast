Title: Эпизод восемьдесят третий
Date: 2021-08-31 12:00
Tags: emm, windows hello, horizon, uag, jwt, springone, deem, cloud
Summary: Август - астры, август - звёзды, винограда гроздья, грозы и крипкасты. Почти как в [стихах Марины Цветаевой](https://youtu.be/1n5rvWnkIIw). Лёха переехал в новую музыкальную студию, и мы посидели с ним на концах видео-канала, обсудили анонсы и отчёты по облачным системам, технические статьи об искусстве управления устройствами и организации сетевого шлюза в VDI.

# Новости от ЛёхиР (alex8s)
- Смена привязки [EMM в учётной записи Android for Enterprise](https://play.google.com/work/adminsettings)
- Подробно про настройку [Windows Hello for Business](https://www.brookspeppin.com/2021/08/13/how-to-setup-windows-hello-for-business-key-trust-method/) 
- События для аудита и [автоматизации работы Horizon](https://williamlam.com/2021/08/listing-all-vmware-horizon-events.html)
- Интеграция [UAG и WS1 Access с помощью Java Web Tokens (JWT)](http://nicksitblog.com/2021/07/integrating-workspace-one-access-and-uag-with-jwt/)
- Автоматическая [замена сертификатов на UAG с помощью скрипта](https://askaresh.com/2021/07/09/script-to-replace-vmware-unified-access-gateway-certificates-admin-and-internet/)

# Новости от ЛёхиМ
- Осенняя [конференция SpringOne 2021](https://springone.io)
- Отчёт IDC [доля рынка автоматизации и управления](https://www.vmware.com/content/dam/learn/en/amer/fy22/pdf/1010453_IDC_Worldwide_IT_Automation_Configuration_Management_Software_Market_Shares_2020.pdf)
- [Что такое DEEM и почему это важно?](https://blogs.vmware.com/vov/2021/08/03/what-is-deem-and-why-did-vmware-it-deem-it-mission-critical/)
- Анонс раннего доступа к проекту [Monterey](https://blogs.vmware.com/vsphere/2021/08/project-monterey-early-access.html)
- Sterling National Bank [полностью перешёл в облака](https://www.vmware.com/company/customers/index/fullpage.html?path=/content/web-apps-redesign/customer-stories/2021/first-traditional-bank-to-fully-operate-in-the-public-cloud)

# Дополнительно
- [КРИ-П](https://www.youtube.com/c/КРИПы) - Критические Работы по Инфраструктуре - в Пятницу
- [VMworld 2021](http://vmworld.com)
