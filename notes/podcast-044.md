Title: Эпизод сорок четвёртый
Date: 2020-04-13 12:00
Tags: vsphere7, tanzu, tmc, tkg, vmc, vov, skyline, wone, covid-19, uem, avinetworks, horizon, dell
Summary: Мы переходим на CI/CD для подкаста - из облака прямо в облако и вам в наушники!

# Новости облаков и автоматизации
- [Официальный запуск vSphere 7](https://www.vmware.com/vsphere-launch-event.html)
- Tanzu Mission Control: [что это такое и как выглядит](https://tanzu.vmware.com/content/blog/managing-kubernetes-tanzu-mission-control)
- Tanzu Kubernetes Grid+ [для VMware Cloud on AWS](https://cloud.vmware.com/community/2020/03/12/containers-kubernetes-vmcaws-tanzu/)
- Вёбинар VMware on VMware: [как решения AviNetworks используются внутри компании](https://vmware.zoom.us/webinar/register/WN_H_aezSjcTaagq0f4nB_F4w)
- [VMware Skyline 4.2](https://www.youtube.com/watch?v=RvGtfEzXrN4) - что нового и как получить максимум от поддержки! [Серия видеороликов](https://www.youtube.com/playlist?list=PLsVuPQkOh8TYGdrBpBIuIi1-IFq-g6-bP) и [полезный документ](https://www.vmware.com/learn/480361_SKYLINE_TY.html)
- [История успеха](https://www.youtube.com/watch?v=Jp6hDzxWRJw): как обеспечить управление для 72000 устройств в 147 школах при помощи Workspace One 
- [Интервью VMware COO Sanjay Poonen каналу CNBC](https://www.youtube.com/watch?v=fUthsIgZowE&feature=emb_title) - про COVID-19, про технологии, про 5G и про то, как VMware помогает бизнесу
- [Программа Cloud Provider vExpert](https://www.vmware.com/learn/480361_SKYLINE_TY.html)

# Новости EUC
- [Выход Workspace One UEM 20.04](https://docs.vmware.com/en/VMware-Workspace-ONE-UEM/2004/rn/VMware-Workspace-ONE-UEM-Release-Notes-2004.html)
- [Настройка AVI Vantage под VMware Horizon](https://avinetworks.com/docs/18.2/configure-avi-vantage-for-vmware-horizon/)
- [Онлайн-семинары по AVI с 6 по 9 апреля](https://info.avinetworks.com/workshop/online/april6-9-2020)
- [Блог с обсуждением вариантов развёртывания AVI Vantage для Workspace One задач](https://www.evengooder.com/2020/03/primer-avi-vantage-for-Horizon-and-WS1.html)
- [Форум с обсуждением различных вопросов по EMM-системам](https://emm.how)
- Способы развернуть MSOffice ProPlus с помощью Workspace One
    - [статья в КБ](https://digital-work.space/display/AIRWATCH/AirWatch+Deploy+MSOffice+at+Win+or+Mac)
    - [и новая очень подробная статья](https://brookspeppin.com/2020/03/20/how-to-deploy-office-365-with-ws1/)
- [Что нужно знать, чтобы развернуть vSphere 7 дома](https://www.virtuallyghetto.com/2020/03/homelab-considerations-for-vsphere-7.html#more-167448)
- [Опыт DELL в развёртывании VMware Horizon в группе компаний ПИК](https://www.dellemc.com/resources/en-us/asset/customer-profiles-case-studies/solutions/dell-technologies-video-pik.mp4)

# Дополнительно
- [Запись](https://dropmefiles.com/9xaDt) c 4х дней технических вёбинаров Avi Networks
- [Весенние вёбинары русскоговорящей команды VMware](https://bit.ly/38Afp1r)
