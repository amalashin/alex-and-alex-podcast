Title: Эпизод шестьдесят пятый
Date: 2020-11-11 10:00
Tags: wone, connector, vrops, cloud director, networking, vrealize, samsung, kubernetes, multi-cloud
Summary: Добрый день, товарищи! В 65-ом эпизоде мы расскажем о выходе ряда революционных продуктов, а также обсудим последние новости VMware и мира ИТ.

# Новости управления рабочими местами
- Новая локальная версия [Workspace One Access 20.10](https://docs.vmware.com/en/VMware-Workspace-ONE-Access/20.10/rn/VMware-Workspace-ONE-Access-Release-Notes--On-Premises-.html)
- Обновление коннектора [Workspace One Access Connector 19.3.0.1](https://docs.vmware.com/en/VMware-Workspace-ONE-Access/19.03/rn/WS1Access-190301-Release-Notes.html)
- Особенности [миграции на Workspace One Access 20.10](https://docs.vmware.com/en/VMware-Workspace-ONE-Access/20.10/ws1_access-2010-upgrade.pdf)
    - В том числе [обновление кластера из 3-х нод](https://docs.vmware.com/en/VMware-Workspace-ONE-Access/20.10/ws1_access_upgade.doc/GUID-B9CF23F0-B090-47A5-BEB0-96F007C65E0F.html)
- Мониторинг Horizon с помощью [vRealize Operations Management Pack for Horizon - новая версия в облаке](https://blogs.vmware.com/management/2020/10/announcing-vrealize-operations-management-pack-for-horizon.html)


# Новости облаков и автоматизации
- VMware Cloud Director - [глобальная доступность](https://blogs.vmware.com/cloudprovider/2020/10/cds-global-availability.html)
- Вышел [vRealize Network Insight 6.0](https://blogs.vmware.com/management/2020/10/now-available-vrealize-network-insight-6-0.html)
- Партнёрство [VMware](https://www.vmware.com/company/news/releases/vmw-newsfeed.VMware-and-Samsung-Form-Alliance-to-Accelerate-Communication-Service-Providers-Transformation-to-5G.80be820d-3352-4bca-b666-d070be606c4d.html) и [Samsung](https://news.samsung.com/us/5g-networks-vmware-samsung-partner-service-provider-csps/) в области технологий Telco и 5G
- Мониторинг Kubernetes при помощи [vRealize Operations](https://blogs.vmware.com/management/2020/10/vrops-k8s-whitepapers.html)
- [Выбор решения для управления мульти-облаком](https://www.vmware.com/learn/683326_REG.html)

# Дополнительно
- [КРИ-П](https://www.youtube.com/channel/UClg6v5K2QlMOdBf2ffGEJKw) - Критические Работы по Инфраструктуре - в Пятницу
