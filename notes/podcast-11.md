Title: Эпизод одиннадцатый
Date: 2019-08-09 12:00
Tags: wone, mobilesso, sccm, uag, vpn, seg, uhana
Summary: Пока все отдыхают в отпусках мы выпускаем новый эпизод, а так же к нам вернулся Мириан!

# Новости EUC
- [Workspace One Access](https://techzone.vmware.com/blog/why-we-renamed-vmware-identity-manager-workspace-one-access) - новое имя VMware Identity Manager
- [MobileSSO](https://digital-work.space/display/IDM/MobileSSO) - проверка устройства на валидность при доступе к Identity Manager
- [Работа Workspace ONE Enrollment c SCCM для управления Windows 10](https://techzone.vmware.com/blog/maintain-workspace-one-enrollment-sccm-configuration-baselines)
- [Что нового в VMware Unified Access Gateway 3.6](https://www.vmgu.ru/news/vmware-unified-access-gateway-96), [Secure Email Gateway Edge](https://youtu.be/J9sCCorhxe0), [функции OCSP stapling](https://en.wikipedia.org/wiki/OCSP_stapling), [настройка Per-App VPN в UAG](http://www.evengooder.com/2018/06/leveraging-vmware-horizons-unified.html)

# Новости облаков и автоматизации
- [VMware анонсировала приобретение](https://blogs.vmware.com/telco/vmware-to-add-uhana-to-telecommunications-portfolio-harnessing-the-power-of-ai-for-mobile-networks/) компании [Uhana](http://www.uhana.io) - компанию, разработавшую движок анализа в реальном времени сетей телко-провайдеров на основе глубокого обучения

# Дополнительно
- Рубрика Мириана и новая загадка!
