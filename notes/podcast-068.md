Title: Эпизод шестьдесят восьмой
Date: 2020-12-25 12:00
Tags: cloud, horizon, zscaler, hackers, rajiv, bitnami, tanzu, fusion, uag
Summary: Прошлая неделя в VMware вышла богатой на звонкие заголовки событий: русские хакеры ломают Workspace One! Компанию покинул вице-президент и уходит к конкурентам! Лёха устроил ремонт на даче!.. В общем, есть что обсудить и разобраться - есть ли повод переживать или можно жить спокойно. Присоединяйтесь к нам!

# Новости облаков и автоматизации
- VMware - [лидер оптимизации затрат на облако](https://www.vmware.com/company/news/releases/vmw-newsfeed.VMware-Named-a-Leader-in-Cloud-Cost-Optimization-and-Hybrid-Cloud-Management-Reports-by-Global-Analyst-Firm.c4f986cd-e05c-44d6-876e-dfd1a0062459.html)
- Horizon Perpetual Licensies - всё?
- VMware + ZScaler = [безопасность цифровой среды](https://ir.vmware.com/websites/vmware/English/8411/news-detail.html?airportNewsID=8b9a62f7-05e4-4aef-b21a-8faeb7b21707)
- Русские хакеры? Опять?! [NSA Cybersecurity Alert](https://www.nsa.gov/News-Features/Feature-Stories/Article-View/Article/2434988/russian-state-sponsored-malicious-cyber-actors-exploit-known-vulnerability-in-v/)
- [Rajiv ушёл из VMware](https://www.vmware.com/company/leadership/rajiv-ramaswami.html)
- Со второго ноября каталог Bitnami [стал доступен провайдерам](https://blogs.vmware.com/cloudprovider/2020/11/developer-ready-clouds-for-vmware-cloud-providers-bitnami-content-is-free-for-cloud-provider-partners.html)
- [Про Tanzu и про Kubernetes](https://www.youtube.com/watch?v=U8oBDqkauAE&feature=youtu.be) - начало (уровень 101)

# Новости EUC
- Работа [VMware Fusion на Apple Silicon](https://blogs.vmware.com/euc/2020/12/apple-m1-chip-how-vmware-makes-it-work-for-the-enterprise.html?utm_source=feedly&utm_medium=rss&utm_campaign=apple-m1-chip-how-vmware-makes-it-work-for-the-enterprise)
- Вышел VMware [Unified Access Gateway 20.12](https://youtu.be/oN9yxldgBLI)
    - Парольные политики (только при установке)
    - Конфигурирование параметров сетевых адаптеров
    - Поддержка SNMP v3
    - Сертификация FIPS (Tunnel, Horizon, SEG)
- Инструменты в помощь, когда нужен [разбор проблем в инициированном macOS](https://mobile-jon.com/2020/12/10/a-beginners-guide-to-macos-support-for-a-uem-engineer/)
    - [BBedit](https://www.barebones.com/products/textwrangler/download.html)
    - [JAMF PPPC Utility](https://github.com/jamf/PPPC-Utility)
    - xCode

# Дополнительно
- [КРИ-П](https://www.youtube.com/c/КРИПы) - Критические Работы по Инфраструктуре - в Пятницу (теперь и видеоверсия нас).
