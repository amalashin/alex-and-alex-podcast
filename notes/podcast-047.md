Title: Эпизод сорок седьмой
Date: 2020-05-15 12:00
Tags: horizon, tools, database, ubuntu, flings, helpdesk, nsx, vsphere7, azure, avs, tanzu, kb
Summary: Пока мы выходим из рабочих нерабочих дней, обновилось много утилит и статей БЗ. Прямо из облака!

# Новости EUC
* [Решение проблем с соединениями в Horizon](https://techzone.vmware.com/resource/understand-and-troubleshoot-horizon-connections)
* [Сетевые порты в Horizon 7.12](https://techzone.vmware.com/resource/network-ports-vmware-horizon-7)
* Полезные утилиты Horizon:
    * [Horizon Cloud Pod Architecture Tools](https://flings.vmware.com/horizon-cloud-pod-architecture-tools)
    * [Horizon View Events Database Export Utility](https://flings.vmware.com/horizon-view-events-database-export-utility)
    * [Ubuntu OVA for Horizon](https://flings.vmware.com/horizon-ova-for-ubuntu)
    * [VMware OS Optimization Tool](https://flings.vmware.com/vmware-os-optimization-tool)
    * [Horizon Helpdesk Utility](https://flings.vmware.com/horizon-helpdesk-utility)
    * [Horizon Session Recording](https://flings.vmware.com/horizon-session-recording)

# Новости облаков и автоматизации
- [Стал доступен NSX Container Plugin 3.0.1](https://my.vmware.com/web/vmware/details?downloadGroup=NSX-T-PKS-301&productId=982)
- [Блог vSphere 7 - море полезных статей](https://blogs.vmware.com/vsphere/tag/vsphere-7)
- [Анонс Microsoft следующего шага в эволюции Azure VMware Solutions (AVS)](https://azure.microsoft.com/en-us/blog/microsoft-announces-next-evolution-of-azure-vmware-solution/)
- [Tanzu Observability для приложений Spring Boot](https://tanzu.vmware.com/content/practitioners-blog/zero-cost-no-sign-up-introducing-tanzu-observability-for-spring-boot-applications)
- Новые статьи БЗ:
    - [CPU Scheduling Affinity недоступно в клиенте HTML5 (77129)](https://kb.vmware.com/s/article/77129)
    - [Расширенная информация про диски в vSphere 7.0 (78427)](https://kb.vmware.com/s/article/78427)
    - [Устаревание использования cookie-аутентификации в vSphere REST APIs (vAPIs) (78315)](https://kb.vmware.com/s/article/78315)
    - [Правильное использование снапшотов для vCenter ВМ (78146)](https://kb.vmware.com/s/article/78146)
- [Термо-камера Vodafone](https://www.totaltele.com/505819/Vodafone-introduces-thermal-IoT-camera-to-help-businesses-safely-reopen-after-COVID-19)

# Дополнительно
- [КРИ-П](https://www.youtube.com/channel/UClg6v5K2QlMOdBf2ffGEJKw) - Критические Работы по Инфраструктуре - в Пятницу 
- [Весенние вёбинары русскоговорящей команды VMware](https://bit.ly/38Afp1r)
