Title: Эпизод сорок пятый
Date: 2020-04-24 19:00
Tags: android, cope, afe, horizon, carbonblack, nsx, vrni, vmc, vov, tanzu, vmworld, vsphere
Summary: Сидим дома, изучаем новые технологии. О них мы вам и расскажем, прямо из облака!

# Новости EUC
- [Android COPE vs Legacy](https://emm.how/t/android-enterprise-cope-mode/1158)
- [Видео о миграции Android Legacy в Android for Enterprise](https://youtu.be/KDnad4Lgpo0)
- [Android Work Profile Apps](https://arsenb.wordpress.com/2019/02/04/google-play-managed-iframe-in-workspace-one-uem-airwatch-private-apps/)
- [Развёртывание приложений без инициации устройства](https://techzone.vmware.com/blog/how-enable-intelligent-hub-and-workspace-one-apps-without-full-management)
- [Обновление Horizon - последовательность](https://kb.vmware.com/s/article/78445)

# Новости облаков и автоматизации
- [Анонс поддержки linux в Carbon Black](https://www.carbonblack.com/2020/04/01/announcing-the-release-of-malware-prevention-for-linux/) (для защиты от вредоносных программ). И техническое описание решения [Carbon Black Endpoint](https://www.carbonblack.com/resource/endpoint-standard-datasheet/)
- [Новая веха](https://www.vmware.com/company/news/releases/vmw-newsfeed.VMware-Surpasses-Major-Virtual-Cloud-Network-Milestones.522aece5-adb1-4cd5-97c7-d8bce81ea91e.html) в развитии виртуальной облачной сети (Virtual Cloud Network) - выход [NSX-T 3.0](https://www.vmware.com/company/news/releases/vmw-newsfeed.VMware-Surpasses-Major-Virtual-Cloud-Network-Milestones.522aece5-adb1-4cd5-97c7-d8bce81ea91e.html) и [vRealize Network Insight 5.2](https://blogs.vmware.com/management/2020/04/announcing-vmware-vrealize-network-insight-5-2.html)
- [Vodafone завершил развёртывание сетевой виртуальной инфраструктуры](https://www.businesswire.com/news/home/20200407005175/en/) (на базе решений VMware) в Европе
- VMware Cloud on AWS - [теперь можно купить онлайн](https://cloud.vmware.com/community/2020/04/01/vmware-cloud-aws-purchase-online-get-started-today/)
- VMware on VMware - [путь ИТ компании к Tanzu Kubernetes Grid на vSAN](https://blogs.vmware.com/vov/2020/04/03/vmware-its-tanzu-kubernetes-grid-on-vsan/) и [предстоящий вебинар](https://vmware.zoom.us/webinar/register/WN_Z6T-xgv3Q2SFobOHe0EzkQ)
- Tanzu - это не просто набор продуктов, это [единая согласованная экосистема](https://tanzu.vmware.com/content/blog/consistency-matters-tanzu-application-service-kubernetes)
- Tanzu Service Mesh - [что такое + демо](https://www.youtube.com/watch?v=EquVhIkS1oc)
- VMworld 2020 [пройдёт в виртуальном формате](https://www.vmworld.com/en/us/faqs.html)
- Окончание жизненного цикла [vSphere Web Client на основе Flash](https://blogs.vmware.com/vsphere/2020/04/vsphere-web-client-support-beyond-adobe-flash-eol.html) 

# Дополнительно
- [Весенние вёбинары русскоговорящей команды VMware](https://bit.ly/38Afp1r)
