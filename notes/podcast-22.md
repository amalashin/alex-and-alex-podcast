Title: Эпизод двадцать второй
Date: 2019-10-29 10:00
Tags: vcf, pks, nsx, vra, vmworld, k8s, pks, wone, uem, dem, appvolumes, mwc
Summary: Подборка интересных новостей из облака и про облако!

# Новости облаков и автоматизации
- [Вышел vCF 3.9.0](https://docs.vmware.com/en/VMware-Cloud-Foundation/3.9/rn/VMware-Cloud-Foundation-39-Release-Notes.html) - теперь в комплекте с PKS 1.5.0 и NSX-T 2.5.0!
- [Сентябрьский выпуск vRA](https://cloud.vmware.com/community/2019/09/24/vmware-vrealize-automation-cloud-september-launch/)
- [Документация и обзорные статьи по vRA](https://docs.vmware.com/en/vRealize-Automation/8.0/rn/vRealize-Automation-80-release-notes.html)
- [Статья с картинками по установке vRA 8](http://it-notes.co.uk/2019/10/vra8-vrealize-automation-8-0-installation/)
- [Постер по всем продуктам VMware](https://alex-and-alex-podcast.s3.eu-central-1.amazonaws.com/vmware-portfolio-241019.pdf)
- [Список Cloud-сессий VMworld EU](https://cloud.vmware.com/community/2019/10/01/vmworld-eu-2019-dont-miss-key-sessions-vmware-cloud-aws/)
- [Что ждать от VMworld EU кубероводам](https://k8s.vmware.com/vmworld-europe/) (и список сессий по kubernetes)
- Графический помощник для k8s - [IBM Kui](https://github.com/IBM/kui)
- [Видеоуроки по PKS Ninja Lab](https://www.youtube.com/channel/UCE1gWdB5Z5Tvy2G0u8hXUIg/videos)
- [Windows 7 скоро всё](https://blogs.windows.com/windowsexperience/2019/03/12/making-the-transition-to-windows-10-and-office-365/)

# Новости EUC
- [Workspace One UEM 1909 Release Notes](https://docs.vmware.com/en/VMware-Workspace-ONE-UEM/1909/rn/VMware-Workspace-ONE-UEM-Release-Notes-1909.html)
- [Dynamic Environment Manager 9.9](https://docs.vmware.com/en/VMware-Dynamic-Environment-Manager/9.9.0/rn/VMware-Dynamic-Environment-Manager-Release-Notes-990.html) (Extended Service Branch)
- [AppVolumes 2.18](https://docs.vmware.com/en/VMware-App-Volumes/2.18/rn/VMware-App-Volumes-218-Release-Notes.html) (Extended Service Branch)
- [AppVolumes API](https://code.vmware.com/apis/561/app-volumes-rest)
- [Открыта регистрация на Mobile World Congress](https://www.mwcbarcelona.com/register/?j=311807&sfmc_sub=39977457&l=985_HTML&u=12393443&mid=7299389&jb=563&ID=a6g1r000000xZTuAAM&JobID=311807&HE=9a16a29412f2303c5eb9b9bf13ade97dfa499e85d9de3885e5359ca160eeb680&utm_source=sfmc&utm_medium=email&utm_campaign=MWC20_PVR_RegOpen+-+20191024_110545&utm_content=hero) в Барселоне 24-27 февраля 2020
