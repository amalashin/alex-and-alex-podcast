Title: Эпизод восемьдесят первый
Date: 2021-07-01 12:00
Tags: wone, android, bitlocker, hub, vmware, vforum, kubernetes, tkg, nsx-t, vmworld
Summary: Погода на улице стала настолько жаркой, что при добавлении к ней ещё и нескольких горячих новостей, нам с Лёхой пришлось пойти на экстренные меры по охлаждению ситуации. Самое время сделать кофе гляссе или просто шарик мороженого с чаем, и присоединяйтесь к нашей встрече!

# Новости от ЛёхиР (alex8s)
- Выход [Workspace ONE UEM 2105 в облаке](https://docs.vmware.com/en/VMware-Workspace-ONE-UEM/2105/rn/Workspace-ONE-UEM-2105-Release-Notes.html)
   - Новый интерфейс для профилей Android и tvOS
   - Android Launcher Offline mode, очищение данных учётной записи между сессиями
   - Поддержка и управление (приостановка работы) Windows BitLocker to Go
   - Дупликация правил Baselines для настройки ОС Windows
   - Hub для macOS Big Sur более не требует Rosetta 2 для установки на Apple Silicon
- Свежее сравнение [Microsoft Endpoint Manager против VMware Workspace ONE UEM](https://mobile-jon.com/2021/06/20/evaluating-endpoint-manager-vs-workspace-one-uem-2021-edition/)
- Способы автоматизации [управления устройствами на основе данных с Workspace ONE Intelligence](https://www.evengooder.com/2021/06/Ruthless-Automation-With-WS1-Intelligence.html)

# Новости от ЛёхиМ
- Прошёл [vForum 2021](https://www.vmware.com/learn/896713_VFO_REG.html?src=em_60b78d0063518&cid=7012H000001l8of)
- Готовые решения для [среды Kubernetes](https://tanzu.vmware.com/content/blog/kubernetes-ready-solutions-for-cloud-native-applications?src=so_602d54e7e837c&cid=7012H000001KW9I) на [VMware Marketplace](https://marketplace.cloud.vmware.com)
- [TKG теперь с Kubeapps](https://blog.bitnami.com/2021/06/kubeapps-meets-tanzu-kubernetes-grid.html)
- Миграция на [NSX-T для vCloud Director](https://blogs.vmware.com/cloudprovider/2021/05/vmware-nsx-migration-for-cloud-director-assessment-mode.html)
- [Сервисы Rackspace для VMware Cloud](https://www.rackspace.com/cloud/vmware/vmware-cloud)
- Открылась регистрация на [VMworld 2021](https://www.vmware.com)

# Дополнительно
- [КРИ-П](https://www.youtube.com/c/КРИПы) - Критические Работы по Инфраструктуре - в Пятницу