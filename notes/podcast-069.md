Title: Эпизод шестьдесят девятый
Date: 2021-01-14 18:00
Tags: deep learning, gpu, olivia, horizon, appvolumes, 2021, gartner, devsecops, vov, tanzu
Summary: Первый крипкаст в 2021! Мы начинаем год на позитивной ноте - встречаемся с Лёхой очно, чтобы обсудить свежие новости за первые 14 дней нового года. Мы думали год начнётся тихо-спокойно, но всё обернулось куда интереснее. Разбираемся вместе.

# Новости от ЛёхиР (alex8s)
* Ускорение Deep Learning через [переход с GPU на CPU](https://octo.vmware.com/accelerating-machine-learning-inference-cpu-vmware-vsphere-neural-magic/) (не наоборот!) 
* Встречаем Оливию, подругу Алисы [и новую ведущую курсов VMware Livefire](https://www.livefire.solutions/uncategorized/ai-is-taking-over-livefire/)
* Оптимизация [MS Teams в VMware Horizon 7-8](https://blogs.vmware.com/euc/2021/01/take-advantage-of-the-vmware-horizon-optimization-for-microsoft-teams-today.html)
* Управление приложениями в [Horizon и AppVolumes4](https://blogs.vmware.com/euc/2021/01/horizon-control-plane-application-management.html)

# Новости от ЛёхиМ
* [10 Предсказаний на 2021 от Sandjay Poonen](https://www.linkedin.com/pulse/my-top-10-predictions-2021-sanjay-poonen)
* [5 технологических предсказаний от Chris Wolf](https://www.vmware.com/radius/5-enterprise-tech-predictions-2021/)
* [Спасибо, Пэт!](https://www.vmware.com/company/news/releases/vmw-newsfeed.VMware-Board-of-Directors-Initiates-Search-for-Chief-Executive-Officer.111a263c-9ba0-43d4-b608-7da7f956484d.html)
* [VMware - выбор заказчиков для управления пользовательскими окружениями в 2021](https://blogs.vmware.com/euc/2021/01/vmware-gartner-peer-insights-customers-choice-for-uem.html) (по мнению Gartner)
* [DevSecOps для современных приложений с VMware Tanzu](https://tanzu.vmware.com/content/blog/tanzu-advanced-generally-available-devsecops)
* Вебинары VMware on VMware:
	- [Управление современными приложениями](https://register.gotowebinar.com/register/6774621514869444877) (18 января, 11:00)
	- [Как VMware управляет частным облаком с vRealize Operations](https://register.gotowebinar.com/register/6132414365271769869) (20 января, 21:00)
	- [Всё для разработчика на опыте VMware ИТ](https://register.gotowebinar.com/register/4631642566008865293) (25 января, 22:00)
	- [Трансформация сети WAN и SD-WAN](https://register.gotowebinar.com/register/1222087794480403216?source=vmware) (26 января, 21:00)
	- [Horizon 7 и концепция нулевого доверия для ВРМ и приложений](https://attendee.gotowebinar.com/register/3723853880298019856) (29 января, 7:30)
	- [Виртуальная реальность, трансформирующая рабочие места](https://vmware.zoom.us/webinar/register/WN_YlXp61wvTxSAgIaNC3fmwQ) (9 февраля, 20:00)

# Дополнительно
- [КРИ-П](https://www.youtube.com/c/КРИПы) - Критические Работы по Инфраструктуре - в Пятницу
