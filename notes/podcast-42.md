Title: Эпизод сорок второй
Date: 2020-03-30 18:00
Tags: vra, interview, covid-19, horizon
Summary: А в нашей виртуальной облачной студии опять гость - Сергей Калугин, с которым мы пообщались про автоматизацию!

# Новости облаков и автоматизации
- Технический обзор vRealize Automation 8.1 от гостя - Сергея Калугина: [основные нововведения платформы автоматизации](https://blogs.vmware.com/management/2020/03/announcing-vrealize-automation-8-1.html)!

# Новости EUC
- [Как разные вендоры EUC реагируют на коронавирус](https://www.brianmadden.com/opinion/EUC-resources-and-stories-on-the-Coronavirus)
- [Обновлена карта сетевых портов для Horizon 7.12](https://techzone.vmware.com/resource/network-ports-vmware-horizon-7)


# Дополнительно
[Весенние вёбинары русскоговорящей команды VMware](https://bit.ly/38Afp1r)
