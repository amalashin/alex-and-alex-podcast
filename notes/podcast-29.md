Title: Эпизод двадцать девятый
Date: 2019-12-16 10:00
Tags: wone, uem, horizon, livefire, hpc, ai/ml, hamlet, pacific, k8s, saml, sd-wan, devops
Summary: До Нового Года остаётся чуть меньше двух недель, но вас по прежнему ждёт подборка свежих новостей. Прямо из облака!

# Новости EUC
- Выход Workspace One UEM 1912: [заметки о выпуске](https://docs.vmware.com/en/VMware-Workspace-ONE-UEM/1912/rn/VMware-Workspace-ONE-UEM-Release-Notes-1912.html) и [видео](https://youtu.be/zJCYJXXereA)
- Новые лабораторные работы: [по Horizon 7](https://labs.hol.vmware.com/hol/catalogs/lab/6538), [Horizon 7 RDSH](https://labs.hol.vmware.com/hol/catalogs/lab/6537) и [Workspace One UEM](https://labs.hol.vmware.com/HOL/catalogs/lab/6018)
- Обновление материала курса [EUC Livefire Adv Integrations](https://euc-labs.livefire.solutions/m/84508)
- [Как задействовать вычислительные ускорители в vSphere 6.5](https://octo.vmware.com/enable-compute-accelerators-vsphere-6-5-machine-learning-hpc-workloads/) для машинного обучения и других нагрузок HPC (High Perfomance Computing)

# Новости облаков и автоматизации
- Гамлет - [новый Open Source проект VMware](https://octo.vmware.com/project-hamlet-secure-multi-vendor-multi-mesh-federation-open-source/) для построения мульти-вендорной безопасной сети Service Mesh
- [Вебинар Проект Pacific](https://secure.vmware.com/405092_REG) – новая архитектура vSphere с Kubernetes внутри
- [Чёрная магия SAML](https://techzone.vmware.com/vmware?share=video1765) и портал TechZone
- [VMware - опять лидер](https://blogs.vmware.com/velocloud/2019/12/04/gartner-magic-quadrant-wei-2019/)! На этот раз в области решений SD-WAN
- Блог DevOps - [Десять основ построения традиционных приложений: виртуальные машины не умерли!](https://blogs.vmware.com/management/2019/12/the-10-foundations-of-devops-for-traditional-applications-virtual-machines-are-not-dead.html)
