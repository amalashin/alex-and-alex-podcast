Title: Эпизод седьмой
Date: 2019-07-14 12:00
Tags: ios, boxer, ens, openstack, opensource, vmc
Summary: Пробуем новую рубрику "биты и байты", а Мириан болеет.

# Новости EUC
- Биты и байты: Алёха рассказывает про синхронизацию контента, а подробности [в его блоге](https://digital-work.space/display/BLOG/2019/07/10/Corporate+and+Personal+Documents+Sync)
- Почтовые сообщения не приходят сами на Ваш смартфон! [Как же он их получает?](https://digital-work.space/display/AIRWATCH/AirWatch+Integration+with+EMail)
- [Безопасность в iOS](https://mobile-jon.com/2018/07/18/ios-security-overview/) и [функционал ENSv2 и ENS Key Escrow в Boxer](https://www.brianmadden.com/opinion/VMware-Boxer-FastSync-could-usher-in-better-third-party-email-experience)

# Новости облаков и автоматизации
- [OpenStack от VMware](https://www.vmware.com/ru/products/openstack.html) ([пошаговый обзор](http://featurewalkthrough.vmware.com/#!/vmware-integrated-openstack), [лабораторная работа](https://labs.hol.vmware.com/HOL/catalogs/lab/4906) и [блог](https://blogs.vmware.com/openstack/))
- Проекты VMware с открытым исходным кодом:
    - [Contour](https://github.com/heptio/contour) (Ingress-контроллер для Kubernetes)
    - [Gimbal](https://github.com/heptio/gimbal) (балансировщик 7го уровня)
    - [Sonobouy](https://github.com/heptio/sonobuoy) (диагностическая утилита для k8s)
    - [Tern](https://github.com/vmware/tern/) (инспекция контейнеров)
- А так же [исследовательские проекты от R&D](https://research.vmware.com/projects)
- Подписки в VMC, [как они работают и где их активировать](https://youtu.be/w66ofnMM0Hg)?

# Дополнительно
[Видеоответ на вопрос](https://www.youtube.com/watch?v=zvOmxuC0Duk)
