Title: Эпизод семидесятый
Date: 2021-01-29 12:00
Tags: kubernetes, tanzu, networking, vov, vrealize cloud universal, saas, multi-cloud, datalog, wone, uem
Summary: Мы продолжаем бодрое начало года с новой порцией новостей. Кругом столько всего творится, что границы тем "облачные/EUC новости" размылись, и мы с Лёхой уже в целом обсуждаем что-то очень крутое и глобальное, что коснулось нас, и возможно вас тоже! Поэтому подключаем наушники, мониторы, шлемы - и вперёд!

# Новости от ЛёхиМ 
- [Анализ преимуществ Kubernetes от VMware от ESG](https://tanzu.vmware.com/content/analyst-reports/analyzing-the-economic-benefits-of-operationalizing-kubernetes-with-vmware-tanzu-standard)
- Что даёт сетевая аналитика? [Отчёт IDC](https://www.vmware.com/learn/682475_TY.html)
- VMware on VMware: [обратите внимание на управление защитой](https://blogs.vmware.com/vov/)
- [vRealize Cloud Universal](https://cloud.vmware.com/vrealize-cloud-universal)
- Кейсы заказчиков Multi-Cloud:
	- [Глостерширский госпиталь](https://www.vmware.com/company/customers/index/fullpage.html?path=/content/web-apps-redesign/customer-stories/2020/improving-patient-care-with-a-secure-hybrid-cloud-platform)
	- [NTT DOCOMO](https://www.vmware.com/content/dam/digitalmarketing/vmware/en/pdf/customers/vmw-ntt-docomo-inc-customer-success-story.pdf)
	- [Финансы Мехико](https://www.vmware.com/company/customers/index/fullpage.html?path=/content/web-apps-redesign/customer-stories/2020/VMware_Cloud_on_AWS_Propels_Financial_Services_Companys_Digital_Transformation_en_41609077890000)
	- [Deutsche Telecom RUS](https://www.vmware.com/content/dam/digitalmarketing/vmware/en/pdf/customers/vmw-deutsche-telekom-it-solutions-rus-success-summary.pdf)

# Новости от ЛёхиР (alex8s) 
- Новый [язык программирования от VMware](https://octo.vmware.com/differential-datalog-new-programming-language-computing-changes/)
- Atlassian закрывают локальную версию [вики Confluence - портал переходит в облако](https://www.atlassian.com/migration/journey-to-cloud?tab=data-center-key-changes)
- Пэт Гельсингер ещё не заступил на CEO, но уже [навёл шороху в Intel](https://habr.com/ru/company/dcmiran/blog/538694/)
- Вышла [VMware Workspace One UEM 2011](https://docs.vmware.com/en/VMware-Workspace-ONE-UEM/2011/rn/VMware-Workspace-ONE-UEM-Release-Notes-2011.html)
    - Устройства Android 10 обязаны быть только AfE
    - VMware Tunnel - политики на правила для подключения устройств в VPN
    - (macOS) расширение SSO на основе пользовательских учётных записей в Big Sur
    - (общие iPad-ы) просмотр текущих зашедших пользователей, истории заходов, с возможностью делать logout пользователю с консоли

# Дополнительно
- [КРИ-П](https://www.youtube.com/c/КРИПы) - Критические Работы по Инфраструктуре - в Пятницу (теперь и видеоверсия нас).
