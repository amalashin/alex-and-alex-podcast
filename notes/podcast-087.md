Title: Эпизод восемьдесят седьмой
Date: 2021-11-24 12:00
Tags: dem, wone, workflows, boomi, horizon, vsphere, bugs, marketplace, vrealize, modernization, vmware
Summary: В канун vForum мы собрались с Лёхой обсудить небывалое дело - отзыв обновления vSphere U3, о котором мы вот только недавно рассказывали. Ну а ещё подъехали несколько интересных технических статей о развитии мобильных процессов, новости об обнове на vRealize True Visibility Suite и так далее. Завариваем чайку и присоединяемся к нашим посиделкам!

# Новости от ЛёхиР (alex8s)
- Интеграция [Dynamic Environment Manager и Worskapce ONE UEM](https://roderikdeblock.com/how-to-integrate-vmware-dynamic-environment-manager-and-workspace-one-uem/)
- Мобильные рабочие процессы [Mobile Workflows вместе с Boomi](https://mobile-jon.com/2021/11/22/workspace-one-experience-workflows-the-evolution-of-mobile-flows/)
- Типовые [Mobile Workflows с Boomi](https://docs.vmware.com/en/VMware-Workspace-ONE/services/workspace_one_experience_workflows/GUID-B639F2BC-D2F6-4470-904B-49923CA18538.html)
- Поддержка [120мс задержек в Horizon 7/8](https://kb.vmware.com/s/article/86304?lang=en_US)

# Новости от ЛёхиМ
- Отозвали vSphere U3, [подробности в БЗ](https://kb.vmware.com/s/article/86398)
- [Серьёзная уязвимость Windows 10, Windows 11, Windows Server 2022](https://msrc.microsoft.com/update-guide/vulnerability/CVE-2021-41379)
- VMware Marketplace - обновления: [новые пакеты управления](https://blogs.vmware.com/cloud/2021/11/04/discover-40-vrealize-true-visibility-suite-management-packs-on-vmware-marketplace/) и [поддержка третьесторонних приложений](https://blogs.vmware.com/cloud/2021/11/02/vmware-marketplace-new-release-support-for-third-party-commerce/)
- [Грандиозное обновление vRealize True Visibility Suite](https://blogs.vmware.com/management/2021/11/a-big-vrealize-true-visibility-update.html)
- [Автоматическая модернизация приложений](https://www-netone-co-jp.translate.goog/knowledge-center/netone-blog/20211028-1/?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en-US&_x_tr_pto=nui)


# Дополнительно
- [КРИ-П](https://www.youtube.com/c/КРИПы) - Критические Работы по Инфраструктуре - в Пятницу
