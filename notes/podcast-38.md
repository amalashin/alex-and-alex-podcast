Title: Эпизод тридцать восьмой
Date: 2020-02-28 16:00
Tags: platinum, carbonblack, cloud, vcf, partners, vra, ansible, iot, wone, uem, horizon
Summary: Облака позволяют безопасно передавать информацию даже в условиях сложной эпидемиологической обстановки и свежий эпизод уже ждёт вас в облаке!

# Новости облаков и автоматизации
- Линейка продуктов Platinum [станет недоступной с апреля](https://blogs.vmware.com/vsphere/2020/02/announcing-end-of-availability-for-vsphere-platinum-vcloud-suite-platinum-and-cloud-foundation-platinum.html)
- [Обзорный отчёт по кибер-безопасности VMware Carbon Black '2020](https://www.vmware.com/company/news/releases/vmw-newsfeed.VMware-Carbon-Black-2020-Cybersecurity-Outlook-Report-Reveals-Evolving-Attacker-Behaviors-Relationship-Dynamics-Between-IT-and-Security-Teams.06cd69f2-3caf-4462-a742-f139cd4f53f5.html)
- [Безопасность Cloud Foundation](https://www.vmware.com/company/news/releases/vmw-newsfeed.VMware-Makes-Comprehensive-Workload-and-Network-Security-More-Economical-and-Easier-to-Operate-Inside-Data-Centers-and-Clouds.51a6d3ef-9b13-4f1d-8da1-134bde8dbe5a.html)
- [Обновление партнёрской программы VMware](https://www.vmware.com/partner-connect-live-geo.html)
- [Интеграция vRealize Automation и Ansible Tower](https://blogs.vmware.com/management/2020/02/introducing-ansible-tower-integration-with-vrealize-automation.html) (пока только в облаке)
- Современные приложения IoT - [какие они](https://blogs.vmware.com/services-education-insights/2020/02/ingestion-in-a-modern-iot-application.html)?

# Новости EUC
- [Выпуск VMware Workspace One UEM 20.01](https://docs.vmware.com/en/VMware-Workspace-ONE-UEM/2001/rn/VMware-Workspace-ONE-UEM-Release-Notes-2001.html)
- [Компоненты Workspace One Access](https://youtu.be/8_XGqkHGuZw)
- [Интеграция VMware Workspace One UEM и Active Directory](https://techzone.vmware.com/vmware?share=demo1927)
- [VMware Horizon for Windows 10 support](https://kb.vmware.com/s/article/2149393)
- Тонкие клиенты Тонк и безопасность
