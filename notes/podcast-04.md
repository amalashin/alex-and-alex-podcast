Title: Эпизод четвёртый
Date: 2019-06-19 12:00
Tags: vov, vmworld, openshift, k8s, bluemedora, idm, wone
Summary: Второй подкаст на удалёнке, Лёха всё ещё в отпуске, а Лёха по прежнему в офисе. Зато сегодня у нас гость!

# Новости облаков и автоматизации
- Как ИТ VMware использует собственные продукты [блог](https://www.vmware.com/company/vmware-on-vmware.html), [сайт](https://www.vmware.com/company/vmware-on-vmware.html), [сессии VMworld 2018](https://videos.vmworld.com/global/2018) (LDT1252, LDT1324, LDT1873, LDT1940, END1156, VIN1170, VIN1180, HYP1215, CNA1232, WIN1236, VIN1253, LDT1869, LDT1873, LDT1899, LDT1940)
- [Статья про 10 отличий OpenShift и Kubernetes](https://cloudowski.com/articles/10-differences-between-openshift-and-kubernetes/)
- Партнёрство [BlueMedora и VMware](https://www.vmware.com/promotions/2019-vrealize-vcloud-suite-extensibility.html)

# Новости EUC
- [Deep dive видео про Identity Manager](https://techzone.vmware.com/vmware?share=video1536) от Peter Björk
- Новые статьи по работе с Workspace One Intelligence:
    - [начало работы и активация](https://techzone.vmware.com/getting-started-workspace-one-intelligence-reports-and-dashboards-vmware-workspace-one-operational)
    - [определение устройств требующих замену аккумулятора](https://techzone.vmware.com/automating-battery-replacement-workspace-one-intelligence-vmware-workspace-one-operational-tutorial)
    - [автоматизация патчинга ПО и ОС](https://techzone.vmware.com/automating-patch-remediation-workspace-one-intelligence-vmware-workspace-one-operational-tutorial)
- Рассказ Мириана про реальный опыт использования универсального рабочего места в отпуске

# Дополнительно
- [Идеальные блинчики](https://journals.aps.org/prfluids/abstract/10.1103/PhysRevFluids.4.064802), они существуют - [статья в PDF](https://arxiv.org/pdf/1901.06028)!
- Загадка
