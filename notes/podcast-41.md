Title: Эпизод сорок первый
Date: 2020-03-20 18:00
Tags: horizon, hub, tunnel, serverless, vsphere7, covid-19
Summary: Обсервация и работа из дома - самое время изучить что-то новое и поделиться этим с вами (а Лёху понесло)! Слушаем напрямую из облаков!

# Новости EUC
- Вышел последний Horizon 7-ой версии. [Заметки к релизу 7.12](https://docs.vmware.com/en/VMware-Horizon-7/7.12/rn/horizon-712-view-release-notes.html)
- [Как включить Hub Catalog под Win10](https://brookspeppin.com/2020/03/18/how-to-enable-the-new-intelligent-hub-catalog-on-windows-10/)
- [Туннель для приложения в Win10](https://brookspeppin.com/2020/01/23/how-to-setup-the-new-workspace-one-tunnel-windows-desktop-application/)
- Построение политик для WIndows 10 в [VMware Policy Builder](https://www.vmwarepolicybuilder.com)
- [Политика допуска пользователей на Windows 10 машины через Azure AD](https://emm.how/t/limiting-users-that-can-log-into-a-windows-10-machine-using-intune-and-restricted-groups/1205)
- Комментарии по данной политике [от Арсена](https://arsenb.wordpress.com/2020/02/17/win10-limiting-users-that-can-log-into-a-workstation-using-restricted-groups-csp/#more-1294)

# Новости облаков и автоматизации
- Alibaba Cloud: [Serverless и FaaS](https://www.alibabacloud.com/blog/create-a-serverless-website-with-alibaba-cloud-function-compute_594594?spm=a2c41.14027722.0.0)
- Технический обзор vSphere 7: [основные новинки платформы виртуализации](https://blogs.vmware.com/vsphere/vsphere-7), не касающиеся Kubernetes!

# Дополнительно
- Лёха побывал на конференции [РусКрипто](https://www.ruscrypto.ru) и делится впечатлениями
- Микросервисы - это не всегда хорошо, как и почему в компании Segment отказались от этой идеи и [вернулись на монолитное приложение](https://segment.com/blog/goodbye-microservices/).
- [Полным ходом идут весенние вёбинары русскоговорящей команды VMware](https://bit.ly/38Afp1r)
- **Ииииии, наконец то загадка с призом! 🎁**
