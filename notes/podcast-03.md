Title: Эпизод третий
Date: 2019-06-12 12:00
Tags: horizon, openid, oauth, idm, jwt, aws, cloud, ibm, blockchain, daml, k8s, velero
Summary: Первый подкаст на удалёнке, Лёха в отпуске, а Лёха в офисе. И вот что у нас получилось.

# Новости EUC
- Выход Horizon 7.9, утилита поддержки [Horizon Toolbox](https://labs.vmware.com/flings/horizon-toolbox-2) и утилита записи сессии [Horizon Session Recording](https://labs.vmware.com/flings/horizon-session-recording)
- OpenID Connect (OAuth 2.0) в [Identity Manager](https://github.com/vmware/idm/wiki/Single-sign-on-for-Mobile) и [статья](https://github.com/vmware/idm/wiki/Validating-Access-or-ID-Token) про JWT (Java Web Tokens)
- [Генератор демонстраций](https://script.google.com/macros/s/AKfycbwUts3fR4SQKYTjaghul4iRmdfRKRhFahm8h9lJqOQyg77vDFIn/exec) Android for Enterprise

# Новости облаков и автоматизации
- Гибридные облака VMware, три варианта: [VMware Cloud on AWS](https://cloud.vmware.com/vmc-aws), [IBM Cloud for VMware Solutions](https://www.ibm.com/cloud/vmware), [Решения VMware в Azure](https://azure.microsoft.com/ru-ru/overview/azure-vmware)
- [VMware Blockchain](https://blogs.vmware.com/blockchain/), язык написания смарт-контрактов [DAML](https://daml.com) и [информация про совместную работу](https://medium.com/daml-driven/daml-vmware-fd18013632f7). [Запись вебинара](https://vts.inxpo.com/Launch/Event.htm?ShowKey=65577) про блокчейн.
- KubeCon, контейнеры и DR для Kubernetes - [Velero 1.0](https://velero.io/velero-1.0-has-arrived)
