Title: Эпизод шестьдесят четвёртый
Date: 2020-10-28 12:00
Tags: saltstack, future ready, vrealize, cloud, modern network, horizon, windows 10, mdm
Summary: Сегодня по счёту 64-ый выпуск нашего подкаста. Можно считать его своего рода юбилейным, степень двойки же! Последний месяц мы активно экспериментировали с форматом видео для ведения шоу, и этот выпуск сделаем в виде Video/Audio или V/A-каст, который можно только слушать, или только смотреть, ну или слушать и смотреть всё вместе!

# Новости облаков и автоматизации
- Приобретение [SaltStack](https://www.saltstack.com)
- [Future Ready](https://www.vmware.com/learn/655750_REG.html) - управление SaaS
- vRealize Code Stream - [всё](https://kb.vmware.com/s/article/81173), EOA
- vRealize Automation Cloud - [октябрьский выпуск](https://cloud.vmware.com/community/2020/10/21/vrealize-automation-cloud-10-20-launch-update/)
- Конференция [Modern Network for a Future Ready Business](https://www.vmware.com/uk/modern-network.html)
- [Видение VMware работника будущего](https://www.vmware.com/company/news/releases/vmw-newsfeed.VMware-Announces-Future-Ready-Workforce-Solutions-to-Address-the-Needs-of-the-Distributed-Workforce.fb37aa5d-66f3-4e59-89cb-160b36ce87f9.html)

# Новости EUC
* Последний [выпуск Horizon 7.13 и его поддержка](https://kb.vmware.com/s/article/81189)
* Разбираем [Horizon 8 - ставим основные компоненты](https://darrylmiles.blog/2020/10/14/getting-started-with-horizon-8/)
* Соединение мобильных приложений с [Mobile Flows](https://mobile-jon.com/2020/10/18/vmware-workspace-one-mobile-flows-and-the-ootb-journey-with-servicenow/)
* Документация по [решению проблем с Windows 10](https://techzone.vmware.com/troubleshooting-windows-10-vmware-workspace-one-operational-tutorial)
* Документация по [решению проблем с macOS](https://techzone.vmware.com/troubleshooting-macos-management-vmware-workspace-one-operational-tutorial)
* Управление мобильными устройствами - [китайский MDM](https://arsenb.wordpress.com/2020/09/04/android-management-in-china-what-are-your-options/)

# Дополнительно
- [КРИ-П](https://www.youtube.com/channel/UClg6v5K2QlMOdBf2ffGEJKw) - Критические Работы по Инфраструктуре - в Пятницу