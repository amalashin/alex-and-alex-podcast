Title: Эпизод сорок девятый
Date: 2020-05-29 12:00
Tags: instant clone, linux, vsphere, rest, api, techzone, horizon, controlup, ports, iot, ml, google, vra, vcap, tanzu
Summary: 49! Маленький, но юбилей - нам годик, прямо в облаке! :)

# Новости EUC
- Мгновенные клоны (Instant Clones) с настройкой, в том числе для Linux:
    - [новая архитектура клонирования в vSphere 6.7 от William Lam](https://www.virtuallyghetto.com/2018/04/new-instant-clone-architecture-in-vsphere-6-7-part-1.html)
    - [обзор возможностей в vSphere 7.0 от William Lam](https://www.virtuallyghetto.com/2020/05/guest-customization-support-for-instant-clone-in-vsphere-7.html)
    - [движок настройки](https://code.vmware.com/docs/11721/vmware-vsphere-web-services-sdk-programming-guide/GUID-E63E67E4-370C-4437-9EC6-444D17DC4E05.html)
    - [код и документация](https://code.vmware.com/docs/11721/vmware-vsphere-web-services-sdk-programming-guide/GUID-E63E67E4-370C-4437-9EC6-444D17DC4E05.html)
- Как задействовать [REST API в Horizon](https://www.retouw.nl/horizon/horizonapi-getting-started-with-the-horizon-rest-api/)
- Инструменты для дополнения EUC теперь можно [забирать с Techzone](https://techzone.vmware.com/tools)
- Слухи о системе мониторинга для Horizon [- контроль вверх?](https://blogs.vmware.com/euc/2020/05/controlup-advanced-monitoring.html)

# Новости облаков и автоматизации
- Потрясающий [сборник портов](https://ports.vmware.com/home/vSphere)
- Разговор про IoT & ML
- Анонс [Google Cloud VMware Engine](https://cloud.google.com/blog/topics/hybrid-cloud/announcing-google-cloud-vmware-engine)
- Майский апдейт vRA 8.1:
    - [самосборные ресурсы](https://blogs.vmware.com/management/2020/04/introducing-custom-resources-and-resource-actions-in-vrealize-automation.html)
    - [политики подтверждений](https://blogs.vmware.com/management/2020/03/approvals-in-vra.html)
    - [DevOps для инфраструктуры (вебинары)](https://cloud.vmware.com/events?event=vRealize%20Automation%20Cloud)
- Новый дизайн лаборатория для [сдачи экзамена VCAP](https://blogs.vmware.com/education/2020/05/13/vcap-deploy-lab-exam-improvements/)
- [Дискуссия про современную разработку](https://spring.io/blog/2020/05/14/vmware-coo-sanjay-poonen) в блоге Sprint (Josh Long & Sanjay Poonen): про приложения, про облака, Tanzu, сети и многое другое

# Дополнительно
- С Днём Рождения подкаст! 1 год с момента запуска проекта.
- [КРИ-П](https://www.youtube.com/channel/UClg6v5K2QlMOdBf2ffGEJKw) - Критические Работы по Инфраструктуре - в Пятницу 
- [Весенние вёбинары русскоговорящей команды VMware](https://bit.ly/38Afp1r)
