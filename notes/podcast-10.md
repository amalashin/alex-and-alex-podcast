Title: Эпизод десятый
Date: 2019-08-02 12:00
Tags: vra, pks, harbor, opensource, airwatch, interview
Summary: Рурбрика "Биты и байты" по автоматизации, а так же интервью со спец-гостем!

# Новости облаков и автоматизации
- Биты и байты: Custom Forms в vRA, как их готовить правильно (скрытые возможности)
- [PKS Essentials 1.15](https://blogs.vmware.com/cloudnative/2019/06/19/performance-bootstrapping-and-crds-in-kubernetes-1-15/), [Harbor и другие продукты OpenSource получили Enterprise Support](https://blogs.vmware.com/cloudnative/2019/07/25/vmware-essential-pks-1-15-support-harbor/)

# Новости EUC
- Скорость работы сервисов AirWatch и нагрузочное тестирование, [твики на скорость](https://digital-work.space/display/AIRWATCH/AirWatch+Performance)

# Дополнительно
- Ответ на загадку предыдущего эпизода и объявление победителя
- Интервью с Маркусом Кляйном: про жизнь, про работу, про технологии, а так же новая загадка!
