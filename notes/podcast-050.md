Title: Эпизод пятидесятый
Date: 2020-06-09 18:00
Tags: lastline, operations, cloud, nist, vsphere7, kb, cloud director, kubernetes, android, horizon7, techzone, wone, api
Summary: Полтишок! Пятидесятый подкаст - прямо из облака!

# Новости облаков и автоматизации
- Новое приобретение? [Lastline](https://blogs.vmware.com/networkvirtualization/2020/06/lastline.html/)
- Новая книжка: ["Готовы ли ваша эксплуатация к облаку?"](https://pages.cloud.vmware.com/l/338801/2020-03-27/356s46)
- [Что такое облако в определении NIST](https://csrc.nist.gov/publications/detail/sp/800-145/final)
- Релиз vSphere 7.0.0a [основные изменения связаны с kubernetes](https://docs.vmware.com/en/VMware-vSphere/7.0/rn/vsphere-vcenter-server-700a-release-notes.html)
- Ура, [поддержка vSphere 6.7 продлена!](https://blogs.vmware.com/vsphere/2020/06/announcing-extension-of-vsphere-6-7-general-support-period.html)
- Сервис [VMware Cloud Director](https://cloud.vmware.com/cloud-provider-hub/cloud-director-service)
- Новые блогопосты:
    - [Что поменялось в системном томе vSphere 7](https://blogs.vmware.com/vsphere/2020/05/vsphere-7-esxi-system-storage-changes.html)
    - [Pod'ы в vSphere 7 - всё об этом](https://blogs.vmware.com/vsphere/2020/05/vsphere-7-vsphere-pods-explained.html)
    - [Не стоит больше использовать Integrated Windows Auth](https://blogs.vmware.com/vsphere/2020/05/vsphere-7-integrated-windows-authentication-iwa-ldap.html)
    - [vSphere 7 с Kubernetes - супервизор-кластер, часть 1 ](https://blogs.vmware.com/vsphere/2020/05/vsphere-7-with-kubernetes-network-service-part-1-the-supervisor-cluster.html)
- Новые статьи БЗ:
    - [Проверка срока сертификата STS на vCenter](https://kb.vmware.com/s/article/79248) по следам [соответствующей статьи](https://blogs.vmware.com/vsphere/2020/05/signing-certificate-is-not-valid-security-token-service-certificate-issue-in-vsphere.html)
    - [Проблема сертификатов при апгрейде vCenter на 7.0](https://kb.vmware.com/s/article/78657)
    - [Root-аккаунт не может модифицировать файлы на ESXi 7.0](https://kb.vmware.com/s/article/78689)
    - [В клиенте vSphere 7.0 невозможно добавить устройства типов: Floppy, Parallel Port, SCSI](https://kb.vmware.com/s/article/78978)

# Новости EUC
- Напоминалка что [в ноябре Android Device Administrator закроют](https://blogs.vmware.com/euc/2020/05/important-changes-to-android-management-with-workspace-one-uem.html)
- Настройка [гостевой системы-клона Instant Clone в vSphere 7](https://www.virtuallyghetto.com/2020/05/guest-customization-support-for-instant-clone-in-vsphere-7.html)
- Непрерывность [бизнеса на Horizon 7.x - статья в Techzone](https://techzone.vmware.com/resource/business-continuity-vmware-horizon)
- Клиент или сервер? Точка входа пользователя в Workspace One
- Начало работы [с Horizon API](https://www.retouw.nl/horizon/horizonapi-getting-started-with-the-horizon-rest-api/)


# Дополнительно
- [КРИ-П](https://www.youtube.com/channel/UClg6v5K2QlMOdBf2ffGEJKw) - Критические Работы по Инфраструктуре - в Пятницу
- [Весенние вёбинары русскоговорящей команды VMware](https://bit.ly/38Afp1r)
