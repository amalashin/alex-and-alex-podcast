Title: Эпизод девятнадцатый
Date: 2019-10-08 10:00
Tags: uag, jwt, sso, tunnel, iot, pks, pacific
Summary: Одной осенней тёмной ночечкой мы записали новый эпизод, который уже ждёт вас из облака!

# Новости EUC
- Новая версия UAG 3.7, [видео по новым фичам](https://youtu.be/eZHDGfC-Gro) (Linux Kernel 4.19, RE 1.8 u221, консолидация syslog, RADIUS SSO, поддержка динамического JWT, поддержка XL размера установки)
- [Скрытые инструменты в UAG, tcpdump и ethtool](https://digital-work.space/display/HORIZON/Unified+Access+Gateway)
- [Статья про настройку VMware Tunnel Edge Services on Unified Access Gateway](https://techzone.vmware.com/confguring-vmware-tunnel-edge-service-vmware-workspace-one-operational-tutorial#1151181)
- Более старая [статья по Tunnel в режиме Basic Mode](https://www.evengooder.com/2018/06/leveraging-vmware-horizons-unified.html)
- Более старая [статья по Tunnel в режиме Cascade Mode](https://www.evengooder.com/2019/02/uag-34-cascade-mode-deployment-for.html)
- Новая [лабораторная работа Hands-on-Labs по управлению всеми видами устройств](https://labs.hol.vmware.com/hol/catalogs/lab/6018)

# Новости облаков и автоматизации
- VMware - один из лидеров платформ автоматизации, [отчёт Forrester](https://www.vmware.com/company/news/releases/vmw-newsfeed.Independent-Research-Firm-Names-VMware-a-Leader-in-2019-Report-for-Infrastructure-Automation-Platforms.1922447.html)
- Про IoT (Pulse IoT Center 2.0 - очень крут!)
- Новая [лабораторная работа по PKS 1.5 (от основ до deep dive)](https://labs.hol.vmware.com/HOL/catalogs/lab/6633)
- [Расширенная телеметрия в PKS](https://content.pivotal.io/blog/announcing-enhanced-telemetry-for-pks)
- Наши коллеги из vSpeaking Podcast пригласили VP & CTO по облачным платформам VMware (Kit Colbert), который [рассказывает про проект Pacific](https://www.vspeakingpodcast.com/episodes/129)
- [Книга историй успеха VMware у крупнейших заказчиков](https://www.vmware.com/content/dam/digitalmarketing/vmware/en/pdf/customers/vmw-customer-stories-ebook.pdf)
