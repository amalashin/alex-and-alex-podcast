Title: Эпизод шестидесятый
Date: 2020-09-18 13:00
Tags: controlup, vmworld2020, vmworldsecemea
Summary: И у нас снова гость! Разговариваем с Евгением Клюшиным, техническим экпертом компании ControlUp!

# ControlUp 
- Немного истории о [ControlUp](https://www.controlup.com/), как появилось соглашение между ControlUp и VMware
- Чем отличается ControlUp от других систем мониторинга, включая недавнее решение vRealize Operations for Hoprizon (так называемый "v4h")
- Много интересных подробностей вокруг новой системы мониторинга для VMware Horizon

# Дополнительно
- [VMworld 2020 - Регистрация](https://reg.rainfocus.com/flow/vmware/vmworld2020/reg/form/contactInfo)
- [КРИ-П](https://www.youtube.com/channel/UClg6v5K2QlMOdBf2ffGEJKw) - Критические Работы по Инфраструктуре - в Пятницу
- [CREEP](https://www.youtube.com/watch?v=qjdY1g0eL1M&list=PL0aZOZ-cWW_V4-5VFDBG0kBrOeSWdx0Sw) - Commit Every Experiment in Production
