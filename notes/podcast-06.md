Title: Эпизод шестой
Date: 2019-07-04 12:00
Tags: avinetworks, k8s, vmc, horizon, saml, idm, wone
Summary: Эпизод получился не очень длинным, но довольно интересным по технологическому наполнению.

# Новости облаков и автоматизации
- VMware выразило намерения приобрести AviNetworks
- Статья Gartner [Best Practices for Running Containers and Kubernetes in Production](https://k8s.vmware.com/running-containers-and-kubernetes-in-production/)
- [Новый релиз](https://docs.vmware.com/en/VMware-Cloud-on-AWS/0/rn/vmc-on-aws-relnotes.html) VMC on AWS (M7)

# Новости EUC
- Обновление [Horizon 7.9](https://docs.vmware.com/en/VMware-Horizon-7/7.9/rn/horizon-79-view-release-notes.html)
- Проверка SAML-протокола в vIDM с помощью SAML Test Service Provider: [https://sptest.iamshowcase.com/](https://sptest.iamshowcase.com/)
- [10 причин зачем нужен Workspace One](https://blogs.vmware.com/euc/2019/06/workspace-one-use-cases.html) и более подробно для [Horizon](http://whychoosehorizon.com)

# Дополнительно
- Робот Фёдор
- Рубрика Мириана (Загадка)
