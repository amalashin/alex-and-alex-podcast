Title: Эпизод двадцать шестой
Date: 2019-11-26 10:00
Tags: google, pulse, iot, wone, serverless, k8s, devops, vra, blast
Summary: Свежие новости, интересные факты, облачные решения и конечно же K8s, прямо сейчас из облака!

# Новости облаков и автоматизации
- Компания [Google приобрела CloudSimple](https://cloud.google.com/blog/topics/inside-google-cloud/helping-customers-run-more-vmware-workloads-in-the-cloud) - компанию, предоставляющую VMware as a Service в публичных облаках
- [Интересные внедрения Pulse IoT Center](https://www.vmware.com/company/news/releases/vmw-newsfeed.VMware-Pulse-IoT-Center-Helps-Customers-Accelerate-Their-Digital-Transformation-Journey-at-the-Edge-with-More-Secure-Edge-Infrastructure-Management.1940198.html) - от управления системаими банка до жизнеобеспечения Бурёнок
- Сервисы по внедрению и обследыванию IoT для заказчиков
- Про сенсоры WorkspaceOne[как настроить](https://blogs.vmware.com/services-education-insights/2019/11/getting-started-with-workspace-one-sensors.html)
- Разговор про FaaS и как сделать своего бота в телеграм

# Новости EUC
- Погружение в контейнеры и их орекстрацию - как микросервисы и Kubernetes используются в продуктах VMware. [Конференция DevOps](https://www.devopspro.ru/ru/)
- Автоматизация VDI шаблона с помощью HashiCorp Packer, Code Stream и vRealize Automation, [часть 1](https://virtualhobbit.com/2019/10/01/automating-vdi-template-creation-with-vmware-code-stream-and-hashicorp-packer-part-1-building-windows/), [часть 2](https://virtualhobbit.com/2019/10/10/automating-vdi-template-creation-with-vmware-code-stream-and-hashicorp-packer-part-2-installing-the-vdi-agents/) и [часть 3](https://virtualhobbit.com/2019/10/17/automating-vdi-template-creation-with-vmware-code-stream-and-hashicorp-packer-part-3-automating-the-solution/)
- [Описание работы протокола Blast Extreme. Оптимизация, проверка оптимизации](https://techzone.vmware.com/resource/vmware-blast-extreme-optimization-guide#sec5-sub1)
- Новое приложение [Workspace One Smartfolio](https://blogs.vmware.com/euc/2019/11/workspace-one-smartfolio.html) для доступа к документам. Целевое использование, отличие от Workspace One Content.
