Title: Эпизод сорок восьмой
Date: 2020-05-25 12:30
Tags: developers, tanzu, modern apps, octarine, tkg, harbor, uem, wone, hub, horizon, controlup
Summary: А у нас порция свежих как майский воздух новостей! Погнали слушать, прямо из облака!

# Новости облаков и автоматизации
- Открылся [портал для девелоперов](https://tanzu.vmware.com/developer/) и первая заметка в [блоге](https://tanzu.vmware.com/developer/blog/a-place-to-build-apps-and-build-skills/)
- Новый обучающий портал по Tanzu: [ModernApps Ninja](https://www.modernapps.ninja)
- Новое приобретение VMware - [анонс о намерениях о покупке компании Octarine](https://www.octarinesec.com/vmware-to-acquire-octarine/)
- William Lam рассказал, [как установить демо Tanzu Kubernetes Grid на VMC или vSphere](https://www.virtuallyghetto.com/2020/05/tanzu-kubernetes-grid-tkg-demo-appliance-for-vmc-and-vsphere.html)
- [Harbor 2.0 GA](https://goharbor.io/blog/harbor-2.0/) - множество нововведений и улучшений, а так же OCI-совместимость!


# Новости EUC
- VMware Workspace ONE UEM [выход версии 20.05](https://docs.vmware.com/en/VMware-Workspace-ONE-UEM/2005/rn/VMware-Workspace-ONE-UEM-Release-Notes-2005.html)
    - Отсрочка инициации Android 10 Work Profile
    - Новые ограничения iOS 13.4 и macOS 10.15 (подключение по старым алгоритмам шифрования, настройка одного iPhone через другой)
    - Внедрение нескольких сертификатов для macOS с автопереключением между ними в зависимости от URL доступа
    - Автопочитнка Intelligent Hub под Windows
- Новая [система мониторинга ControlUp для Horizon](https://www.controlup.com/solutions/end-user-computing/)
- Horizon Flex умер, да здравствует самосборный вариант на Workspace One UEM?
- Интересный разбор в блоге от Арсена про [безопасность видео-конференций Zoom vs MS Teams](https://arsenb.wordpress.com/2020/04/29/zoom-vs-teams-security-options/)

# Дополнительно
- [КРИ-П](https://www.youtube.com/channel/UClg6v5K2QlMOdBf2ffGEJKw) - Критические Работы по Инфраструктуре - в Пятницу
- [Весенние вёбинары русскоговорящей команды VMware](https://bit.ly/38Afp1r)
