Title: Эпизод тридцать пятый
Date: 2020-02-07 10:00
Tags: wone, oauth, android, afe, vra, hci, terraform, bitnami
Summary: Свежие новости из мира EUC и автоматизации VMware уже ждут вас в облаке!

# Новости EUC
- Вышел [VMware Workspace ONE® Access 20.01.0.0](https://docs.vmware.com/en/VMware-Workspace-ONE-Access/20.01/rn/VMware-Workspace-ONE-Access-Release-Notes.html) (on-premise):
- [Установка и управление VMware WOne Access 20.01](https://docs.vmware.com/en/VMware-Workspace-ONE-Access/20.01/workspace_one_access_install/GUID-96E2F98A-5B90-4F81-A302-8264E6362494.html):
- VMware Identity Manager Windows [конец поддержки 24 ноября 2020](https://kb.vmware.com/s/article/2961184)
- VMware Workspace ONE Access 20.01 [технический обзор и архитектура коннектора](https://www.youtube.com/watch?v=1YCeCRNJuRQ)
- [Крутой тренинг](https://www.linkedin.com/learning/web-security-oauth-and-openid-connect/) по OAuth and OpenID Connect
- В Сиднее прошла конференция по Android for Enterprise. [Интересный доклад про OEMConfig и демонстрация его работы.](https://www.youtube.com/watch?v=-YoQUtmdyxw)

# Новости облаков и автоматизации
- [Обновлённый курс по vRealize Automation 8](https://mylearn.vmware.com/mgrReg/courses.cfm?ui=www_edu&a=one&id_subject=91426) - теперь последние версии продуктов
- [Что эксперты говорят про HCI](https://www.vmware.com/radius/what-is-hyperconverged-infrastructure) - Гиперконвергентная инфраструктура
- [Вебинар IDC и VMware](https://onlinexperiences.com/scripts/Server.nxp?LASCmd=AI:4;F:QS!10100&ShowKey=80155) про автоматизацию, аналитику и жизнь компании при использовании мульти-облаков
- Terraform провайдер для vRealize Automation 8 - [как настроить](https://blogs.vmware.com/management/2020/01/getting-started-with-vra-terraform-provider.html) (пока только в облаке)
- [Поддержка OVA в качестве сервиса](https://blogs.vmware.com/management/2020/01/ova-content.html) (Добро пожаловать Bitnami)
- [WireGuard включён в апстрим ядра Linux](https://lists.zx2c4.com/pipermail/wireguard/2020-January/004906.html)
