Title: Эпизод тридцать четвёртый
Date: 2020-01-26 00:00
Tags: digital, solutions, nyansa, cloud, fusion, nautilus, workstation, airwatch, mdm, horizon 
Summary: Возвращаемся в рабочий ритм после праздников. Подборка новостей про VMware уже ждёт вас в облаке!

# Новости облаков и автоматизации
- Цифровая трансформация - современный тренд, [как VMware помогает своим заказчикам](https://www.vmware.com/company/news/releases/vmw-newsfeed.VMware-Helps-Retailers-Digitally-Transform-Brand-Experiences-to-Enable-Intelligent-Stores.12266803-80ed-4ff0-b885-f4e75d8a8524.html).
- [Как решения VMware применяют в различных индустриях](http://www.vmware-industry-solutions.com) и [отдельный блог для ритейла](https://blogs.vmware.com/industry-solutions/retail/)
- [VMware анонсировала намерение о приобретении компании Nyansa](https://blogs.vmware.com/velocloud/2020/01/21/announcement-jan-21/) (аналитика сети с применением AI)
- [Что такое виртуальная облачная сеть - видение VMware](https://www.vmware.com/solutions/virtual-cloud-network.html?src=WWW_US_NSXPage_VCN_2018Launch_LearnMore)
- Ещё один обучающий портал - [пути использования различных технологий VMware](https://pathfinder.vmware.com/path/explore_sd_wan_by_velocloud)
- [Видеоподкаст про современные приложения: Сначала подумайте о данных!](https://www.youtube.com/watch?v=JLWOzhu7DWk)
- [Безопасность, защита и приватность - чего ждать в 2020м](https://www.vmware.com/radius/security-predictions-2020/)
- Ещё раз напоминаем про [окончание жизненного цикла vSphere 6.0](https://blogs.vmware.com/vsphere/2019/10/vsphere-6-0-reaches-end-of-general-support-eogs-in-march-2020.html)
- Истекли сертификаты, подписывающие предыдущие сборки ESXi. [Что делать и как обновляться (статья БЗ)](https://kb.vmware.com/s/article/76555)
- [Проект Nautilus](https://vmwarefusion.github.io) и [техническое превью VMware Fusion](https://blogs.vmware.com/teamfusion/2020/01/fusion-tp20h1-introducing-nautilus.html) - теперь с контейнерами!

# Новости EUC
- [Техническое превью VMware Workstation](https://blogs.vmware.com/workstation/2020/01/vmware-workstation-tech-preview-20h1.html)
- VMware vSphere & Microsoft LDAP Channel Binding & Signing patch ([статья по интеграции AirWatch и AD](https://digital-work.space/display/AIRWATCH/AirWatch+and+Microsoft+Active+Directory)
- [BYOD и использование MDM - важность использования](https://www.brianmadden.com/opinion/BYOD-MDM-still-important)
- [Установка ПО NVIDIA на ESXi и настройка использования vGPU](http://www.yellow-bricks.com/2020/01/22/installing-the-nvidia-software-on-an-esxi-host-and-configuring-for-vgpu-usage/)
- [Horizon Universal Broker](https://docs.vmware.com/en/VMware-Horizon-Cloud-Service/services/hzncloudmsazure.admin15/GUID-B11D8E3C-2CC1-4D01-B679-F899D40437E6.htm)
