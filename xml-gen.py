#!/usr/bin/env python
# -*- coding: utf-8 -*- #

from datetime import datetime
from pytz import timezone
from os import listdir, EX_NOINPUT, EX_CONFIG, EX_UNAVAILABLE, EX_SOFTWARE
from os.path import splitext, basename, getsize
from sys import exit
from markdown import markdown
from lxml import etree
from PIL import Image
# from tinytag import TinyTag
from mutagen.mp4 import MP4

PODCAST = {
    'title': 'Лёха & Лёха о новостях VMware и ИТ',
    'author': 'Алексей (Малашин | Рыбалко)',
    'owner': {'email': 'alexey@malash.in', 'name': 'Алексей Малашин & Алексей Рыбалко'},
    'subtitle': 'Наш особый взгляд на происходящее в мире облаков',
    'language': 'ru',
    'copyright': f'© {datetime.now().year} Алексей Малашин & Алексей Рыбалко',
    'categories': ['Technology', 'Tech News'],
    'summary': 'Мы работаем в московском офисе VMware и решили рассказать вам про всё новое и интересное, что происходит в компании (естественно, на наш личный взгляд). Хотя не только про это, в своих рассказах мы затронем вниманием разнообразные технологии, начиная с виртуализации и облаков, End-User Computing’а и управления устройствами, Cloud-Native Applications и Kubernetes, Интернета вещей и машинного обучения, блокчейн и больших данных. Интересно же? Тогда скорее подключайтесь к нашим уютным аудиозаметкам, будем доставлять каждую неделю!\n\nИ да, здесь обязательно должна быть эта фраза: our postings are our own and don’t necessarily represent VMware’s positions, strategies or opinions.',
    'explicit': False,
    'pub_date': 'Tue, 28 May 2019 12:00:00 +0300',
    'www': 'https://podcast.digital-work.space',
    'root': 'https://alex-and-alex-podcast.s3.eu-central-1.amazonaws.com',
}

NOTES_DIR = 'notes'              # directory with podcast notes in markdown (*.md) format
ARTIFACTS_DIR = 'artifacts'      # directory with artefacts (audio and covers)
FEED_FILENAME = 'podcast.xml'    # xml feed filename
LOGO_FILENAME = 'logo.jpg'       # podcast logo filename

COVER_EXT = 'jpg'                # episode cover extension
AUDIO_EXT = 'm4a'                # episode audio extension
AUDIO_MIME_TYPE = 'audio/x-m4a'  # episode audio mime type
TIMEZONE = 'Europe/Moscow'       # timezone to calculate timestamp offsets

##############################################################################################

# nsmap types
ns_wfw = 'http://wellformedweb.org/CommentAPI/'
ns_itunes = 'http://www.itunes.com/dtds/podcast-1.0.dtd'
ns_google = 'http://www.google.com/schemas/play-podcasts/1.0'
ns_spotify = 'http://www.spotify.com/ns/rss'
ns_mediarss = 'http://www.rssboard.org/media-rss'
ns_content = 'http://purl.org/rss/1.0/modules/content/'
ns_atom = 'http://www.w3.org/2005/Atom'

NSMAP = {
    'wfw': ns_wfw,
    'itunes': ns_itunes,
    'googleplay': ns_google,
    'spotify': ns_spotify,
    'media': ns_mediarss,
    'content': ns_content,
    'atom': ns_atom
}


def read_note(note):
    """Read a note and convert it to html
    
    note - file path to note
    
    returns - dict with metadata and string with note converted to html
    """
    metadata = {}
    md_content = ''

    # reading the header
    is_metadata = True  # trigger for metadata (reading file header till first empty line)
    with open(note, 'r') as md_file:
        for line in md_file:
            if not is_metadata or line.isspace():  # if found empty line -> finished reading metadata and set the flag
                is_metadata = False
            # raise RuntimeError('specific message')
            if is_metadata:  # if reading metadata
                key, value = line.split(':', 1)  # split line by colon, TODO: check for colon
                metadata[key.strip().lower()] = value.strip()  # write metadata to dict
            else:
                md_content += line
    
    return metadata, markdown(md_content)


def sec_to_time(seconds):
    """Convert seconds to h:mm:ss duration
    
    returns - string
    """
    m, s = divmod(round(seconds), 60)  # get minutes and seconds
    h, m = divmod(m, 60)               # get hours and minutes
    
    fmt = f'{m:02d}:{s:02d}'
    if h > 0:
        fmt = f'{h:d}:{fmt}'
    return fmt


def rfc822_timestamp(dtime=None):
    """Return the date in RFC 822 format.
    
    dtime - datetime to format or none to use now()
    return - string
    """
    dt = dtime if dtime else datetime.now()
        
    tz = timezone("Europe/Moscow")
    tz_offset = tz.utcoffset(dt).seconds
    offset_h, offset_m = divmod((tz_offset // 60), 60)
    tz_offset_str = f'+{offset_h:02d}{offset_m:02d}'

    return dt.strftime(f'%a, %d %b %Y %H:%M:%S {tz_offset_str}')


def main():
    print('Starting to build XML...')
    
    # xml root element
    rss = etree.Element('rss', nsmap=NSMAP)
    rss.set('version', '2.0')
    print('-> XML Root created succesfully')

    # channel element
    channel = etree.SubElement(rss, 'channel')

    # general feed settings
    etree.SubElement(channel, 'title').text = PODCAST['title']
    etree.SubElement(channel, 'link').text = PODCAST['www']
    etree.SubElement(channel, 'language').text = PODCAST['language']
    etree.SubElement(channel, 'copyright').text = PODCAST['copyright']
    etree.SubElement(channel, 'description').text = PODCAST['summary']

    etree.SubElement(channel, 'pubDate').text = PODCAST['pub_date']
    etree.SubElement(channel, 'lastBuildDate').text = rfc822_timestamp()

    # atom link
    etree.SubElement(channel, f'{{{ns_atom}}}link', href=f'{PODCAST["root"]}/{FEED_FILENAME}', rel='self', type='application/rss+xml')

    # itunes settings
    etree.SubElement(channel, f'{{{ns_itunes}}}subtitle').text = PODCAST['subtitle']
    etree.SubElement(channel, f'{{{ns_itunes}}}author').text = PODCAST['author']

    owner = etree.SubElement(channel, f'{{{ns_itunes}}}owner')
    etree.SubElement(owner, f'{{{ns_itunes}}}email').text = PODCAST['owner']['email']
    etree.SubElement(owner, f'{{{ns_itunes}}}name').text = PODCAST['owner']['name']

    etree.SubElement(channel, f'{{{ns_itunes}}}new-feed-url').text = f'{PODCAST["root"]}/{FEED_FILENAME}'

    etree.SubElement(channel, f'{{{ns_itunes}}}image', href=f'{PODCAST["root"]}/{LOGO_FILENAME}')
    for category in PODCAST['categories']:
        etree.SubElement(channel, f'{{{ns_itunes}}}category', text=category)
    etree.SubElement(channel, f'{{{ns_itunes}}}explicit').text = 'yes' if PODCAST['explicit'] else 'no'
    etree.SubElement(channel, f'{{{ns_itunes}}}summary').text = PODCAST['summary']
    print('-> XML Settings created sucessfully')
    
    # process items
    print('-> Processing items...')
    md_files = [fn for fn in listdir(NOTES_DIR) if fn.endswith('.md')] # find all the *.md files in the notes directory
    md_files.sort()
    print(f'-> Found {len(md_files)} notes')

    for note in md_files:
        guid = splitext(basename(note))[0]      # guid = filename of the note
        print(f'-> Working with <{guid}>')
        
        # read note
        try:                                    # try to parse...
            metadata, html = read_note(f'{NOTES_DIR}/{note}')    # ...and get metadata and html from markdown
            if 'title' and 'date' and 'tags' and 'summary' in metadata:
                print('-> ... note file parsed')
            else:
                print('ERROR: metadata doesn`t contain required attributes')
                exit(EX_SOFTWARE)
        except:
            print('ERROR: Unable to parse note (check metadata first)')
            exit(EX_SOFTWARE)
    
        # parse note date/time
        try:
            pd = datetime.strptime(metadata['date'], '%Y-%m-%d')
            pd = pd.replace(hour=12)
            print('-> ... date is without time, using default')
        except:
            try:
                pd = datetime.strptime(metadata['date'], '%Y-%m-%d %H:%M')
                print('-> ... date is with time')
            except:
                print('ERORR: Unable to parse time in note')
                exit(EX_SOFTWARE)

        # read image info
        try:                                    # try to open and get info from the cover image
            cover_img = Image.open(f'{ARTIFACTS_DIR}/{guid}.{COVER_EXT}') # img file is relative to the current dir
            cover_w, cover_h = cover_img.size   # cover width and height
            cover_img.close()
            print(f'-> ... image file found, width = {cover_w}, height = {cover_h}')
        except:
            print('ERROR: Unable to open cover image')
            exit(EX_NOINPUT)
        
        # read audio info
        try:
            audio_file = f'{ARTIFACTS_DIR}/{guid}.{AUDIO_EXT}'
            # tag = TinyTag.get(f'{ARTIFACTS_DIR}/{guid}.{AUDIO_EXT}')
            mp4 = MP4(audio_file)
            # audio_sz = tag.filesize
            audio_sz = getsize(audio_file)
            # audio_len = sec_to_time(tag.duration)
            audio_len = sec_to_time(mp4.info.length)
            print(f'-> ... media file found, size = {audio_sz}, len = {audio_len}')
        except:
            print('ERROR: Unable to open audio file')
            exit(EX_NOINPUT)

        item = etree.SubElement(channel, 'item')
        etree.SubElement(item, 'title').text = metadata['title']
        etree.SubElement(item, 'guid', isPermaLink='false').text = guid
        etree.SubElement(item, 'pubDate').text = rfc822_timestamp(pd)
        # etree.SubElement(item, 'link').text = f'{PODCAST["www"]}/{guid}.html'
        print('-> ... title, guid, pubDate and link are set')
        
        image = etree.SubElement(item, 'image')
        etree.SubElement(image, 'url').text = f'{PODCAST["root"]}/{guid}.{COVER_EXT}'
        etree.SubElement(image, 'width').text = str(cover_w)
        etree.SubElement(image, 'height').text = str(cover_h)
    
        etree.SubElement(item, 'enclosure', length=str(audio_sz), type=f'{AUDIO_MIME_TYPE}', url=f'{PODCAST["root"]}/{guid}.{AUDIO_EXT}')
        
        print('-> ... generic parameters are set')
        
        etree.SubElement(item, f'{{{ns_itunes}}}title').text = metadata['title']
        if 'subtitle' in metadata and metadata['subtitle']:
            etree.SubElement(item, f'{{{ns_itunes}}}subtitle').text = metadata['subtitle']
        etree.SubElement(item, f'{{{ns_itunes}}}duration').text = audio_len
        etree.SubElement(item, f'{{{ns_itunes}}}image', href=f'{PODCAST["root"]}/{guid}.jpg')
        etree.SubElement(item, f'{{{ns_itunes}}}explicit').text = 'yes' if PODCAST['explicit'] else 'no'
        
        print('-> ... iTunes parameters are set')
        
        description = etree.SubElement(item, 'description')
        etree.SubElement(description, f'{{{ns_content}}}encoded').text = etree.CDATA(f'{metadata["summary"]}<br><br>{html}')
        
        print('-> ... description are set')

    print('-> XML built succesfully, now saving')

    # save to file
    rss_str = etree.tostring(rss, encoding='UTF-8', xml_declaration=True, pretty_print=True)
    with open(f'{FEED_FILENAME}', 'wb') as xml:
        xml.write(rss_str)
    
    print(f'XML written to {FEED_FILENAME}')


if __name__ == '__main__':
    main()
