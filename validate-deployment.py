#!/usr/bin/env python
# -*- coding: utf-8 -*- #

import ssl
from os import environ, EX_NOINPUT, EX_CONFIG, EX_UNAVAILABLE, EX_PROTOCOL
from os.path import isfile
from sys import exit
from urllib.request import Request, urlopen
from urllib.error import URLError, HTTPError


PODCAST = {
    'root': 'https://alex-and-alex-podcast.s3.eu-central-1.amazonaws.com'
}

FEED_FILENAME = 'podcast.xml'   # xml feed filename
LOGO_FILENAME = 'logo.jpg'      # podcast logo filename

COVER_EXT = 'jpg'               # episode cover extension
AUDIO_EXT = 'm4a'               # episode audio extension

ssl._create_default_https_context = ssl._create_unverified_context


def main():
    print('Starting validate task...')
    
    if not isfile(FEED_FILENAME):
        print(f'ERROR: Feed file {FEED_FILENAME} doesn`t exist')
        exit(EX_NOINPUT)

    if 'CI_COMMIT_TAG' not in environ or not environ['CI_COMMIT_TAG']:
        print('ERROR: There no commit tag in env')
        exit(EX_CONFIG)
    
    req_feed = Request(f'{PODCAST["root"]}/{FEED_FILENAME}')
    req_logo = Request(f'{PODCAST["root"]}/{LOGO_FILENAME}')
    req_cover = Request(f'{PODCAST["root"]}/{environ["CI_COMMIT_TAG"]}.{COVER_EXT}')
    req_audio = Request(f'{PODCAST["root"]}/{environ["CI_COMMIT_TAG"]}.{AUDIO_EXT}')
    print(f'-> Checking {environ["CI_COMMIT_TAG"]}...')
    try:
        print('-> ... feed xml...', end='')
        urlopen(req_feed)
        print('accessible')

        print('-> ... logo file...', end='')
        urlopen(req_logo)
        print('accessible')

        print('-> ... cover file...', end='')
        urlopen(req_cover)
        print('accessible')

        print('-> ... audio file...', end='')
        urlopen(req_audio)
        print('accessible')
    except HTTPError as e:
        print(f'http-{e.code}')
        print(f'ERROR: {e.reason}: {e.read()}')
        exit(EX_UNAVAILABLE)
    except URLError as e:
        print(f'ERROR: Failed to reach a server, reason: {e.reason}')
        exit(EX_PROTOCOL)
    
    print('-> Validation completed succesfully')


if __name__ == '__main__':
    main()
