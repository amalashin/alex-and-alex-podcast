[![pipeline status](https://gitlab.com/amalashin/alex-and-alex-podcast/badges/master/pipeline.svg)](https://gitlab.com/amalashin/alex-and-alex-podcast/-/commits/master)

podcast


# Prepare environment
- Install Python3
- python3 -m venv venv
- source venv/bin/activate
- pip3 install -r requirements_xxx.txt
- for libjpeg:
  - xcode-select --install
  - download https://www.ijg.org/files/jpegsrc.v9d.tar.gz
  - ./configure
  - make
  - sudo make install