#!/bin/zsh

source .env
docker login -u $GROUP_NAME -p $GL_API_TOKEN $REGISTRY
docker run --rm -it $IMAGE_NAME
