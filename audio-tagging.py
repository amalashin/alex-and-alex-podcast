#!/usr/bin/env python
# -*- coding: utf-8 -*- #

from io import BytesIO
from os import EX_OK, EX_NOINPUT
from os.path import isfile
from sys import argv, exit
from datetime import date
from mutagen.mp4 import MP4, MP4Cover
from PIL import Image

PODCAST = {
    'title': 'Лёха & Лёха о новостях VMware и ИТ',
    'author': 'Алексей (Малашин | Рыбалко)',
    'owner': {'email': 'alexey@malash.in', 'name': 'Алексей Малашин & Алексей Рыбалко'},
    'subtitle': 'Наш особый взгляд на происходящее в мире облаков',
    'language': 'ru',
    'copyright': f'© {date.today().year} Алексей Малашин & Алексей Рыбалко',
    'categories': ['Technology', 'Tech News'],
    'summary': 'Мы с Лёхой работаем в российском ИТ. Лёха занимается рабочими местами пользователей, а Лёха - доводит до кондиции OpenStack и всё с ним связанное. А ещё мы периодически встречаемся и обсуждаем всё новое и интересное, что происходит кругом (естественно, на наш личный взгляд). В своих рассказах мы затрагиваем виртуализацию и облака, виртуальные рабочие места и управление устройствами, микросервисы и Kubernetes, Интернет вещей и машинное обучение, блокчейн и большие данные. Интересно же? Тогда скорее подключайтесь к нашим уютным аудио и видео встречам, будем доставлять!',
    'explicit': False,
    'pub_date': 'Tue, 28 May 2019 12:00:00 +0300',
    'www': 'http://podcast.digital-work.space',
    'root': 'https://alex-and-alex-podcast.s3.eu-central-1.amazonaws.com',
}


FEED_FILENAME = 'podcast.xml'    # xml feed filename

NOTES_DIR     = 'notes'         # directory with podcast notes in markdown (*.md) format
ARTIFACTS_DIR = 'artifacts'     # directory with artifacts (audio and covers)
AUDIO_EXT     = 'm4a'           # episode audio extension
COVER_EXT     = 'jpg'           # episode cover extension


def read_note(note):
    """Read a note and convert it to html

    note - file path to note

    returns - dict with metadata and string with note converted to html
    """
    metadata = {}
    md_content = ''

    # reading the header
    is_metadata = True  #  trigger for metadata (reading file header till first empty line)
    with open(note, 'r') as md_file:
        for line in md_file:
            if not is_metadata or line.isspace():  #  if found empty line -> finished reading metadata and set the flag
                is_metadata = False
            # raise RuntimeError('specific message')
            if is_metadata:  # if reading metadata
                key, value = line.split(':', 1)  #  split line by colon, TODO: check for colon
                metadata[key.strip().lower()] = value.strip()  # write metadata to dict
            else:
                md_content += line

    return metadata


def main():
    print('Starting audio tagging...')

    audio_file = ''
    cover_file = ''
    note_file = ''

    if len(argv) < 2:
        print(f'Usage: {argv[0]} podcast-xxx')
        print(f'Where podcast-xxx - episode name')
        exit(EX_NOINPUT)
    else:
        audio_file = f'{ARTIFACTS_DIR}/{argv[1]}.{AUDIO_EXT}'
        cover_file = f'{ARTIFACTS_DIR}/{argv[1]}.{COVER_EXT}'
        note_file = f'{NOTES_DIR}/{argv[1]}.md'

    if not isfile(audio_file) or not isfile(cover_file) or not isfile(note_file):
        print('ERROR: audio, cover or note file doesn`t exist')
        exit(EX_NOINPUT)

    mp4 = MP4(audio_file)
    # print(f'Bitrate = {mp4.info.bitrate}')
    # print(f'Length = {mp4.info.length}')
    # print(f'Mime = {mp4.mime}')

    print(f'-> Audio file found, checking tags...', end='')
    if mp4.tags is not None and '----:space.digital-work:podcast' in mp4.tags:
        print(f'set already!')
        exit(EX_OK)
    else:
        print('not set')

        metadata = read_note(note_file)
        print('-> note metadata read')

        _, trkn = argv[1].split('-', 1)
        trkn = int(trkn)

        cover_img = Image.open(cover_file)
        cover_img = cover_img.resize((1000, 1000), Image.ANTIALIAS, reducing_gap=3.0)
        img = BytesIO()
        cover_img.save(img, 'jpeg', optimize=True, quality=85)
        cover_img.close()
        img.seek(0)

        print('-> clearing tags if any...', end='')
        if mp4.tags:
          mp4.delete()  # clearing the tags if any
        else:
          mp4.add_tags()
        print('done')

        print('-> writing new tags...', end='')
        mp4.tags['\xa9nam'] = metadata['title']         # track title
        mp4.tags['\xa9alb'] = PODCAST['title']          # album, podcast name
        mp4.tags['\xa9ART'] = PODCAST['author']         # track artist
        mp4.tags['aART'] = PODCAST['author']            # album artist
        mp4.tags['\xa9day'] = str(date.today().year)    # year
        mp4.tags['\xa9cmt'] = PODCAST['subtitle']       # comment, less than 100
        mp4.tags['\xa9gen'] = 'Podcast'                 # genre = 'Podcast' for podcasts
        # mp4.tags['\xa9wrt'] = 'composer'              # composer - podcast host or a production company
        mp4.tags['desc'] = metadata['summary']          # description (usually used in podcasts)
        mp4.tags['ldes'] = PODCAST['summary']           # long description
        mp4.tags['purl'] = PODCAST['www']               # podcast URL
        # mp4.tags['egid'] = 'podcast-044'              # podcast episode GUID
        mp4.tags['catg'] = 'Technology'                 # podcast category
        mp4.tags['keyw'] = metadata['tags']             # podcast keywords
        mp4.tags['cprt'] = PODCAST['copyright']         # copyright
        mp4.tags['pcst'] = True                         # podcast (iTunes reads this only on import)
        mp4.tags['trkn'] = [(trkn, 0)]                  # track number, total tracks
        mp4.tags['rtng'] = [2]                          # content rating: none:0, explicit:1, clean:2
        mp4.tags['----:space.digital-work:podcast'] = b'processed'  # special flag to check if tags are set
        print('done')

        print('-> set cover...', end='')
        # with open(cover_file_embed, 'rb') as img:             # cover artwork, list of MP4Cover objects
        mp4.tags['covr'] = [MP4Cover(img.read(), imageformat=MP4Cover.FORMAT_JPEG)]
        print('done')
        img.close()

        print('-> Saving...', end='')
        mp4.save()
        print('done')

        print('-> Tags written successfully!')

    # try:
    #     print('done!')
    # except FileNotFoundError:
    #     print('file not found')
    # except NoCredentialsError:
    #     print('err: no credentials')
    #     exit(EX_CONFIG)


if __name__ == '__main__':
    main()
