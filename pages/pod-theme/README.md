Podcaster Pelican Theme
=======================

Podaster [Pelican](https://getpelican.com/) theme is based on MinimalXY.

I made some improvements and polishing especially for podcastsers. The main purpose of this fork is to keep everything simple and focus on episodes. I removed all unnecessary parts. You can see this theme live here: [digital-work.space/podcast](https://digital-work.space/podcast).


Design focus
------------
- Minimal flat design
- Good usability, simple & intuitive navigation
- Podcasts episodes showing with tags
- Presented in a clean and straightforward way


Features
--------
- Fully responsive and mobile-first
- Podcasts catalogs links
- W3.CSS CSS framework & HTML5 semantics

How to use
----------

```python
DEFAULT_LANG = 'ru'
TIMEZONE = 'Europe/Moscow'
USE_FOLDER_AS_CATEGORY = False
SLUGIFY_SOURCE = 'basename'
DEFAULT_PAGINATION = 10
TYPOGRIFY = True
DISPLAY_PAGES_ON_MENU = False
THEME = 'pod-theme'
GOOGLE_ANALYTICS = ''

DIRECT_TEMPLATES = ['index', 'archives', 'tags']

ARCTICLE_PATHS = ['episodes', 'images']
# ARTICLE_SAVE_AS = '{date:%Y}/{slug}.html'
# ARTICLE_URL = '{date:%Y}/{slug}.html'
PLUGIN_PATHS = ['./plugins']
PLUGINS = ['glossary', 'sitemap']
STATIC_PATHS = ['files', 'images']

PODCAST_PLATFORMS = (
    ('Apple Podcasts', 'https://podcasts.apple.com/ru/podcast/id1468195415'),
    ('Google Podcasts', 'https://www.google.com/podcasts?feed=aHR0cHM6Ly9hbGV4LWFuZC1hbGV4LXBvZGNhc3QuczMuZXUtY2VudHJhbC0xLmFtYXpvbmF3cy5jb20vcG9kY2FzdC54bWw%3D'),
    ('Spotify', 'https://open.spotify.com/show/6ILTcimwMqUpEC7ErWsZmE'),
    ('Yandex.Music', 'https://music.yandex.ru/album/9348886'),
)

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

# Customizations
PODCAST_NAME = 'Лёха & Лёха о новостях VMware и ИТ'
PODCAST_FEED_XML = 'https://alex-and-alex-podcast.s3.eu-central-1.amazonaws.com/podcast.xml'
PODCAST_DESCRIPTION = 'Мы работаем в московском офисе VMware и решили рассказать вам про всё новое и интересное, что происходит в компании (естественно, на наш личный взгляд). Хотя не только про это, в своих рассказах мы затронем вниманием разнообразные технологии, начиная с виртуализации и облаков, End-User Computing-а и управления устройствами, Cloud-Native Applications и Kubernetes, Интернета вещей и машинного обучения, блокчейн и больших данных. Интересно же? Тогда скорее подключайтесь к нашим уютным аудиозаметкам, будем доставлять каждую неделю!<br><br>И да, здесь обязательно должна быть эта фраза: our postings are our own and don’t necessarily represent VMware’s positions, strategies or opinions.'

YEAR_START = 2019
YEAR_NOW = 2020 # date.today().year
```

License
-------
[MIT](LICENSE)
