#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Алексей (Малашин | Рыбалко)'
SITENAME = 'Заметки к подкасту Лёхи и Лёхи'
SITEURL = 'https://podcast.digital-work.space'

PATH = 'content'
OUTPUT_PATH = 'public'

DEFAULT_LANG = 'ru'
TIMEZONE = 'Europe/Moscow'
USE_FOLDER_AS_CATEGORY = False
SLUGIFY_SOURCE = 'basename'
DEFAULT_PAGINATION = 10
TYPOGRIFY = True
DISPLAY_PAGES_ON_MENU = False
THEME = 'pod-theme'
GOOGLE_ANALYTICS = ''

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DIRECT_TEMPLATES = ['index', 'archives', 'tags']

ARCTICLE_PATHS = ['episodes', 'images']
# ARTICLE_SAVE_AS = '{date:%Y}/{slug}.html'
# ARTICLE_URL = '{date:%Y}/{slug}.html'
PLUGIN_PATHS = ['./plugins']
PLUGINS = ['glossary', 'sitemap']
STATIC_PATHS = ['files', 'images']

# links to the podcast platforms
PODCAST_PLATFORMS = (
    ('Apple Podcasts', 'https://podcasts.apple.com/ru/podcast/id1468195415'),
    ('Google Podcasts', 'https://www.google.com/podcasts?feed=aHR0cHM6Ly9hbGV4LWFuZC1hbGV4LXBvZGNhc3QuczMuZXUtY2VudHJhbC0xLmFtYXpvbmF3cy5jb20vcG9kY2FzdC54bWw%3D'),
    ('Spotify', 'https://open.spotify.com/show/6ILTcimwMqUpEC7ErWsZmE'),
    ('Yandex.Music', 'https://music.yandex.ru/album/9348886'),
    ('Youtube', 'http://youtube.com/c/КРИПы'),
)

# links to the telegram channels
TG_HEADER = 'Общение'
TG_NOTE = 'Мы всегда рады ответить на вопросы в группах Телеграм'
TG_LINKS = (
    ('vRealize User Group', 'https://t.me/vrealize'),
    ('VMware Workspace One', 'https://t.me/WorkspaceOne'),
    ('NSX & SD-WAN User Group', 'https://t.me/nsx_russia_cis'),
    ('VMware vSAN', 'https://t.me/VMwarevSAN'),
    ('Tanzu VMware Kubernetes', 'https://t.me/vmwarek8s'),
    ('VMware Cloud Director', 'https://t.me/VMware_vCloud'),
    ('VMware User Group Rus', 'https://t.me/vmugru'),
    ('VMware User Group Central Asia', 'https://t.me/vmugca')
)

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

# Customizations
PODCAST_NAME = 'Лёха & Лёха о новостях в мире опенсорса и ИТ'
PODCAST_FEED_XML = 'https://alex-and-alex-podcast.s3.eu-central-1.amazonaws.com/podcast.xml'
PODCAST_DESCRIPTION = 'Мы с Лёхой работаем в российском ИТ. Лёха занимается рабочими местами пользователей, а Лёха - доводит до кондиции OpenStack и всё с ним связанное. А ещё мы периодически встречаемся и обсуждаем всё новое и интересное, что происходит кругом (естественно, на наш личный взгляд). В своих рассказах мы затрагиваем виртуализацию и облака, виртуальные рабочие места и управление устройствами, микросервисы и Kubernetes, Интернет вещей и машинное обучение, блокчейн и большие данные. Интересно же? Тогда скорее подключайтесь к нашим уютным аудио и видео встречам, будем доставлять!'

YEAR_START = 2019
YEAR_NOW = 2023  # date.today().year
