Alex & Alex Podcast Notes
---

Start: make clean && make html && make devserver

Automated for GitLab CI
1. Rename your project to namespace.gitlab.io, where namespace is
your username or groupname. This can be done by navigating to your
project's Settings > General (Advanced).


2. Adjust Pelican's SITEURL configuration setting in publishconf.py to
the new URL (e.g. https://namespace.gitlab.io)

Installation
---
```
python3 -m venv venv
source ./venv/bin/activate
pip install -r requirements.txt
cd notes
pelican-quickstart
```

Usage
---
```
cd .../podcast/notes
source ../venv/bin/activate
pelican content
pelican --listen
```

Themes
---
```
git clone --recursive https://github.com/getpelican/pelican-themes ./pelican-themes
```

edit Pelican settings
```
THEME = "/home/user/pelican-themes/theme-name"
```

Themes can also be specified directly via the -t ~/pelican-themes/theme-name parameter to the pelican command.


Plugins
---

```
git clone --recursive https://github.com/getpelican/pelican-plugins ./pelican-plugins
```

settings file
```
PLUGIN_PATHS = ['path/to/pelican-plugins']
PLUGINS = ['assets', 'sitemap', 'gravatar']
```
