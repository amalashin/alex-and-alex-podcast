#!/usr/bin/env python
# -*- coding: utf-8 -*- #

from os import environ, EX_NOINPUT, EX_CONFIG
from os.path import basename
from sys import exit
from mimetypes import guess_type
from boto3 import client
from botocore.exceptions import NoCredentialsError

AWS_REGION = 'eu-central-1'
AWS_S3_BUCKET = 'alex-and-alex-podcast'

ARTIFACTS_DIR = 'artifacts'     # directory with artefacts (audio and covers)
FEED_FILENAME = 'podcast.xml'   # xml feed filename
LOGO_FILENAME = 'logo.jpg'      # podcast logo filename

COVER_EXT = 'jpg'               # episode cover extension
AUDIO_EXT = 'm4a'               # episode audio extension

COVER_MIME_TYPE = 'image/jpeg'
AUDIO_MIME_TYPE = 'audio/x-m4a'

def main():
    print('Starting upload task...')

    if 'CHANGED_FILES' not in environ or not environ['CHANGED_FILES']:
        print('ERROR: There no changed files in env')
        exit(EX_NOINPUT)

    if 'AWS_ACCESS_KEY' not in environ or 'AWS_SECRET_KEY' not in environ:
        print('ERROR: AWS credentials are not set in the environment')
        exit(EX_CONFIG)

    changed_files = environ['CHANGED_FILES'].split('\n')
    changed_files = [file for file in changed_files if file.endswith(AUDIO_EXT) or file.endswith(COVER_EXT)]

    print(f'-> Found env settings:')
    print(f'-> ... Changed files ({len(changed_files)}) = {environ["CHANGED_FILES"]}')
    print(f'-> ... AWS Access Key = {environ["AWS_ACCESS_KEY"]}')
    print(f'-> ... AWS Secret Key = {environ["AWS_SECRET_KEY"][:10]}...')

    s3 = client('s3', aws_access_key_id=environ['AWS_ACCESS_KEY'], aws_secret_access_key=environ['AWS_SECRET_KEY'], region_name=AWS_REGION)
    settings = {'ACL': 'public-read', 'StorageClass': 'STANDARD'}
    
    print('-> Got AWS S3 Client, uploading...')
    for file in changed_files:
        print(f'-> ... {file}...', end='')
        try:
            if file.endswith(COVER_EXT):
                settings['ContentType'] = COVER_MIME_TYPE
            elif file.endswith(AUDIO_EXT):
                settings['ContentType'] = AUDIO_MIME_TYPE
            else:
                mimetype, _ = guess_type(file)
                if mimetype:
                    settings['ContentType'] = mimetype
                else:
                    settings['ContentType'] = 'application/octet-stream'
            
            s3.upload_file(file, AWS_S3_BUCKET, basename(file), ExtraArgs=settings)
            print('done!')
        except FileNotFoundError:
            print('file not found')
        except NoCredentialsError:
            print('err: no credentials')
            exit(EX_CONFIG)
    print('-> All files were uploaded successfully')


if __name__ == '__main__':
    main()
