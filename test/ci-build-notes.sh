#!/bin/sh

# prepare
source ci-pre.sh

# ci script
pip install -r requirements_pages.txt
mkdir -p pages/content/episodes
mkdir -p pages/content/images
cp notes/*.md pages/content/episodes/
cp artifacts/*.jpg pages/content/images/
ls -la artifacts/ pages/content/images/ pages/content/episodes/
cd pages/
pelican -s publishconf.py
ls -la public/

# save artifacts
cp -r public/ $BUILD/
