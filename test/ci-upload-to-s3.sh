#!/bin/sh

# prepare
source ci-pre.sh

# ci script
apk add --update git
pip install -r requirements_upload.txt
export CHANGED_FILES=$(git diff-tree --no-commit-id --name-only -r $CI_COMMIT_SHA)
echo $CHANGED_FILES
ls -la artifacts/
python upload-to-s3.py
