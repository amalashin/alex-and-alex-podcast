#!/bin/sh

# prepare
source ci-pre.sh

# ci script
apk add --update py3-lxml py3-markdown py3-pillow
pip install -r requirements_xml-gen.txt
ls -la artifacts/ notes/
python xml-gen.py

# save artifacts
cp podcast.xml $BUILD/
